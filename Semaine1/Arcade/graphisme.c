#include "graphisme.h"
#include "toile.h"
#include "recherche.h"


void endSDL(sdlMedia_t *media){

	free(media->stringListe);
	free(media->stringPos);
	TTF_Quit();
	if(media->window != NULL) SDL_DestroyWindow(media->window);
	//SDL_Quit();
	//free(media);

}

SDL_Texture* loadStringTexture(sdlMedia_t *media, char *str){
	SDL_Color color = {255,255,255,255};
	
	
	SDL_Surface* textSurf = NULL;
	textSurf = TTF_RenderText_Blended(media->font, str, color);
	if(textSurf == NULL) endSDL(media);
	
	SDL_Texture* textText = NULL;
	textText = SDL_CreateTextureFromSurface(media->renderer, textSurf);
	if(textText==NULL) endSDL(media);
	SDL_FreeSurface(textSurf);
	
	return textText;
}

SDL_Texture* loadTextureFromFile(char *fileName, sdlMedia_t *media){
	SDL_Surface *surface = NULL;
	SDL_Texture *texture = NULL;
	
	surface = IMG_Load(fileName);					// Charge l'image dans un objet Surface
	
	if(surface == NULL) endSDL(media);				// Si l'image n'est pas chargée dans la surface
	
	texture = SDL_CreateTextureFromSurface(media->renderer, surface);	// on transforme la surface en texture
	SDL_FreeSurface(surface);					// On libère la surface
	if(texture == NULL) endSDL(media);
	
	return texture;
}

void setSpriteShowed(sdlMedia_t *media, int id, int b){
	media->spriteListe[id]->isShowed = b;
}

void setSpritePos(sdlMedia_t *media, int id, int x, int y){
	media->spriteListe[id]->x = x;
	media->spriteListe[id]->y = y;
}

sprite_t* loadSprite(int *textId, int nSprite, int x, int y, int w, int h, int angle){


	sprite_t* sprite = (sprite_t*) malloc(sizeof(sprite_t));
	sprite->textId = textId;
	sprite->nSprite = nSprite;
	sprite->spriteState = 0;
	sprite->x = x;
	sprite->y = y;
	sprite->w = w;
	sprite->h = h;
	sprite->angle = angle;
	sprite->isShowed = 0;
	
	return sprite;

}

void loadListeString(sdlMedia_t *media, int nString, char **stringListe, int **stringPosListe){
	
	if(media->stringListe != NULL) {
	
		int k;
		for(k = 0; k<nString; k++){
			//free(media->stringListe[k]);
		}
		media->stringListe =  (SDL_Texture **) malloc(sizeof(SDL_Texture**)*nString);

	} else {
		media->stringListe =  (SDL_Texture **) malloc(sizeof(SDL_Texture**)*nString);
	}
	
	if(media->stringPos != NULL){
			//free(media->stringPos);
		media->stringPos = (SDL_Rect *) malloc(nString*sizeof(SDL_Rect));
	} else 
	
	media->stringPos = (SDL_Rect *) malloc(nString*sizeof(SDL_Rect));
	int i;
	for(i = 0; i<nString; i++){
		media->stringListe[i] = loadStringTexture(media, stringListe[i]);
		media->stringPos[i] = (SDL_Rect){stringPosListe[i][0], stringPosListe[i][1], 0, 0};
		SDL_QueryTexture(media->stringListe[i], NULL, NULL, &(media->stringPos[i].w), &(media->stringPos[i].h));
	}

}

sdlMedia_t * initSDL(int windowW, int windowH, int nString, char ** stringListe, int **stringPosListe, int nTexture, char **fileNameListe, int animSpeed, int nSprite, int **textId, int *nSpriteListe, int *x, int *y, int *w, int *h, int *angle){
	
	// Initialisation de l'aleatoire
	srand(time(NULL));
	
	sdlMedia_t *media = (sdlMedia_t*) malloc(sizeof(sdlMedia_t*));

	
	/* Initialisation de la SDL  + gestion de l'échec possible */
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		SDL_Log("Erreur : SDL initialisation - %s\n", SDL_GetError());                // l'initialisation de la SDL a échoué 
		exit(EXIT_FAILURE);
	}
	
	/* Création de la fenetre SDL */
	
	SDL_Window *win = SDL_CreateWindow("Fenetre", 0, 0, windowW+200, windowH+100, SDL_WINDOW_RESIZABLE);
	
	if(win == NULL){
		SDL_Log("Error : SDL window creation - %s\n", 
		SDL_GetError());                 	// échec de la création de la fenêtre
		SDL_Quit();                              // On referme la SDL       
		exit(EXIT_FAILURE);
	}
	
	media->window = win;		// Chargement de la fenetre dans la structure sdlMedia
	media->renderer = SDL_CreateRenderer(media->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);		// Chargement du renderer dans la structure sdlMedia
	if(media->renderer == NULL) printf("Erreur creation renderer\n");
	
	/* Police d'ecriture */
	if(TTF_Init()<0) endSDL(media);
	media->font = TTF_OpenFont("./font.ttf", 24);
	media->nString = nString;
	loadListeString(media, nString, stringListe, stringPosListe);
	
	
	/* Chargement des textures */
	media->textureListe = (SDL_Texture **) malloc(sizeof(SDL_Texture*)*nTexture);
	int i;
	for(i = 0; i<nTexture; i++){
		media->textureListe[i] = loadTextureFromFile(fileNameListe[i], media);
	}
	/* Chargement des sprites */
	media->nSprite = nSprite;
	media->spriteListe = (sprite_t**) malloc(nSprite*sizeof(sprite_t*));
	for(i = 0; i<nSprite; i++){
		media->spriteListe[i] = loadSprite(textId[i], nSpriteListe[i], x[i], y[i], w[i], h[i], angle[i]);
	}
	
	media->animSpeed = animSpeed;
	media->windowW = windowW;
	media->windowH = windowH;
	media->state = 0;
	
	return media;
}


void drawCircle(struct sdlMedia *media, int centerX, int centerY, int radius){
	for (int angle = 0; angle < 360; angle++) 
	    {
		float radian = angle * (M_PI / 180.0);
		int x = centerX + radius * cos(radian);
		int y = centerY + radius * sin(radian);
		SDL_RenderDrawPoint(media->renderer, x, y);
	    }
}

void drawLine(sdlMedia_t *media, int xSrc, int ySrc, int xDst, int yDst){
	SDL_RenderDrawLine(media->renderer, xSrc, ySrc, xDst, yDst);
}

void draw(sdlMedia_t *media){
	SDL_SetRenderDrawColor(media->renderer, 27, 79, 8, 255);
	SDL_RenderClear(media->renderer);
}

void playSprite(sdlMedia_t *media, int idSprite){
	sprite_t *sprite = media->spriteListe[idSprite];
	SDL_Rect dest = {sprite->x, sprite->y, sprite->w, sprite->h};
	SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, sprite->angle, NULL, SDL_FLIP_NONE);

}


int contain(int mx, int my, int x, int y, int w, int h){
	return(mx>=x && my>=y && mx<=x+w && my<=y+h);
}

int isMouseOnLine(int mx, int my, int xSrc, int ySrc,int xDst, int yDst){
	int pas = 16;
	int demiPas = 6;
	
	int norme = sqrt((yDst-ySrc)*(yDst-ySrc)+(xDst-xSrc)*(xDst-xSrc));
	int n = norme/pas;
	
	float dx = (xDst-xSrc)/n;
	float dy = (yDst-ySrc)/n;
	
	int i;
	int r = 0;
	for(i = 0; i<n; i++){
		if(contain(mx,my,xSrc+i*dx-demiPas, ySrc+i*dy-demiPas, pas, pas)) r = 1;

	}
	return r;
}

void animer(sdlMedia_t *media, level_t *level, int endState){
	spider_t *spider = level->spider;
	sprite_t *spiderSprite = media->spriteListe[spider->currentSpriteId];
	noeud_t noeud = level->listeNoeuds[spider->iNoeud];

	
	if(spiderSprite->x >= noeud.x-20 && spiderSprite->x <= noeud.x+20){
		media->state = endState;
		spider->currentSpriteId = spider->spriteIdRepos;
		setSpriteShowed(media, spider->spriteIdMvt, 0);
		media->spriteListe[level->spider->spriteIdRepos]->x = noeud.x;
		media->spriteListe[level->spider->spriteIdRepos]->y = noeud.y;		
		setSpriteShowed(media, spider->spriteIdRepos, 1);
	}
	else{
		noeud_t noeudPrec = level->listeNoeuds[spider->iNoeudPrec];
		
		int norme = sqrt((noeud.y-noeudPrec.y)*(noeud.y-noeudPrec.y) + (noeud.x-noeudPrec.x)*(noeud.x-noeudPrec.x));
		int n = norme/level->spider->pasAnim;
		int dx = (noeudPrec.x-noeud.x)/n;
		int dy = (noeudPrec.y-noeud.y)/n;
		
		spiderSprite->x = spiderSprite->x-dx;
		spiderSprite->y = spiderSprite->y-dy;
		int angle = (int) -(atan((noeudPrec.x-noeud.x)/(noeudPrec.y-noeud.y))*(180/M_PI));
		spiderSprite->angle = angle;
	}
}

void update(sdlMedia_t *media, level_t *level){
	//printf("state %d\n",media->state);
	
	spider_t *spider = level->spider;
	sprite_t *spiderSprite = media->spriteListe[spider->currentSpriteId];
	noeud_t noeud = level->listeNoeuds[spider->iNoeud];
	//printf("currentSpriteId %d\n", spider->currentSpriteId);
	//printf("sX %d\n", media->spriteListe[0]->x);
	//printf("spriteX : %d\n",spiderSprite->w);

	if(media->state == 0){
		spiderSprite->x = noeud.x-spiderSprite->w/2;
		spiderSprite->y = noeud.y-spiderSprite->h/1.5;
	}
	else if(media->state == 1){
		animer(media, level, 0);
	}
	else if(media->state == 2){
		animer(media, level, 3);
	}
	
	int i;
	for(i = 0; i<media->nSprite; i++){
		sprite_t *sprite = media->spriteListe[i];
		sprite->spriteState = (sprite->spriteState == sprite->nSprite)?0:sprite->spriteState+1; // l'etat du sprite est remis a 0 si il est a l'indice max, sinon il avance de 1
	}
	
	
}

int loop(sdlMedia_t *media, level_t *level){
	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	
	/*
	Code de retour : 
	1: fin en cour de route
	2: victoire
	3: defaite
	*/
	int retour = 1;		
	
	int **matriceConnexion = level->matriceConnexion;
	
	int compteur = 0;
	update(media, level);


	sprite_t *papillonSprite = media->spriteListe[level->papillon->currentSpriteId];
	noeud_t noeud = level->listeNoeuds[level->papillon->iNoeud];
	papillonSprite->x = noeud.x-papillonSprite->w/2;
	papillonSprite->y = noeud.y-papillonSprite->h/1.5;

	while(program_on){
	
		if(media->state == 3) program_on = SDL_FALSE;
	
		int mx, my;
		if(SDL_PollEvent(&event)){
			switch(event.type){
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
					case SDLK_ESCAPE:
						program_on = SDL_FALSE;
						break;
				}
				break;
			case SDL_MOUSEBUTTONUP:
				mx = event.motion.x;
				my = event.motion.y;
				if(media->state==1) break;
				
				int i, j;
				int fait = 0;
				for(i = 0; i<level->taille; i++){
					for(j = 0; j<level->taille; j++){
						
				if(fait==0 && matriceConnexion[i][j]){
					noeud_t src = level->listeNoeuds[i];
					noeud_t dst = level->listeNoeuds[j];
					
					if(isMouseOnLine(mx, my, src.x, src.y, dst.x, dst.y)){
						matriceConnexion[i][j] = 0;
						matriceConnexion[j][i] = 0;
						fait = 1;
					}
				}		
			}
		}
				if(fait==1){
					media->state = 1;
					level->spider->currentSpriteId = level->spider->spriteIdMvt;
					setSpriteShowed(media, level->spider->spriteIdRepos, 0);
					media->spriteListe[level->spider->spriteIdMvt]->x = media->spriteListe[level->spider->spriteIdRepos]->x;
					media->spriteListe[level->spider->spriteIdMvt]->y = media->spriteListe[level->spider->spriteIdRepos]->y;
					setSpriteShowed(media, level->spider->spriteIdMvt, 1);
					
					//int Prochaine_etape(int ** MATRICE, int taille, int pos_ar, int pos_pap);
					
					int nouvNoeud = Prochaine_etape(level->matriceConnexion, level->taille, level->spider->iNoeud, level->papillon->iNoeud);
					if(nouvNoeud!=-1) {
						level->spider->iNoeudPrec = level->spider->iNoeud; 
						level->spider->iNoeud = nouvNoeud; 
						
						update(media, level);
						if(level->spider->iNoeud == level->papillon->iNoeud){
							retour = 3;
							media->state = 2;
						}
					}
					else{
						retour = 2;
						program_on = SDL_FALSE;
					}
	}
				break;
			case SDL_QUIT:
				program_on = SDL_FALSE;
				break;
			case SDL_MOUSEMOTION:
				mx = event.motion.x;
				my = event.motion.y;
				
				break;
			}
		}
		// AFFICHAGE
		draw(media);
		SDL_SetRenderDrawColor(media->renderer, 255, 255, 255, 255);
		int i, j;
		
		for(i = 0; i<level->taille; i++){
			noeud_t noeud = level->listeNoeuds[i];
			drawCircle(media, noeud.x, noeud.y, 4);
		}
		
		for(i = 0; i<level->taille; i++){
			for(j = 0; j<level->taille; j++){
				if(matriceConnexion[i][j]){
				
					noeud_t src = level->listeNoeuds[i];
					noeud_t dst = level->listeNoeuds[j];
					
					SDL_SetRenderDrawColor(media->renderer, 255, 255, 255, 255);
					drawLine(media, src.x, src.y, dst.x, dst.y);
				}
			}
		}
		
		for(i = 0; i<media->nSprite; i++){
			if(media->spriteListe[i]->isShowed) playSprite(media, i);
		}
		
		for(i = 0; i<media->nString; i++) SDL_RenderCopy(media->renderer, media->stringListe[i], NULL, &(media->stringPos[i]));
		
		SDL_RenderPresent(media->renderer);
		SDL_Delay(5);
		compteur = (compteur >= media->animSpeed)?0:compteur+1;
		if(compteur == 0){
			update(media, level);
		}
		
	}
	setSpriteShowed(media, level->spider->spriteIdMvt, 0);
	return retour;

}

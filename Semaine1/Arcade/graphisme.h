#ifndef __GRAPHISME_H__
#define __GRAPHISME_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>
#include "toile.h"

typedef struct sprite{
	int *textId;
	int nSprite;
	int spriteState;
	int x, y, w, h;
	int speed, angle;
	int isShowed;
} sprite_t;

typedef struct sdlMedia{
	SDL_Window *window;
	SDL_Renderer *renderer;
	TTF_Font *font;
	int nString;
	SDL_Texture **stringListe;
	SDL_Rect *stringPos;
	int nTexture;
	SDL_Texture **textureListe;
	int nSprite;
	sprite_t **spriteListe;
	
	int windowW, windowH;
	int animSpeed;
	int state;
	
} sdlMedia_t;

void loadListeString(sdlMedia_t *media, int nString, char **stringListe, int **stringPosListe);

int loop(sdlMedia_t *media, level_t *level);
sdlMedia_t * initSDL(int windowW, int windowH, int nString, char ** stringListe, int **stringPosListe, int nTexture, char **fileNameListe, int animSpeed, int nSprite, int **textId, int *nSpriteListe, int *x, int *y, int *w, int *h, int *angle);
void endSDL(sdlMedia_t *media);

#endif

#include "graphisme.h"
#include "toile.h"
#include "recherche.h"

int main(){


	/* Variables importantes */
	
	int animSpeed = 5;
	sdlMedia_t *media;
	level_t *level;
	
	int score = 0, nLevel = 0;
	
	/* Chargement des textes */
	
	// Liste des textes
	char **str = (char**) malloc(sizeof(char*));
	str[0] = "Sauvez le papillon. Cliquez sur un fil pour le couper";
	
	// Position des textes
	int **pos = (int**) malloc(sizeof(int*));
	pos[0] = (int*) malloc(2*sizeof(int));
	pos[0][0]= 10;
	pos[0][1]= 10;

	/* Chargement des textures */
	int nTexture = 9;
	char **fileNameListe = (char**) malloc(nTexture*sizeof(char*));
	fileNameListe[0] = "./res/araignee0.png";
	fileNameListe[1] = "./res/araignee1.png";
	fileNameListe[2] = "./res/araignee2.xcf";
	fileNameListe[3] = "./res/araignee4.xcf";
	fileNameListe[4] = "./res/araignee5.xcf";
	fileNameListe[5] = "./res/araignee6.xcf";
	fileNameListe[6] = "./res/papillon0.png";
	fileNameListe[7] = "./res/papillon1.png";
	fileNameListe[8] = "./res/papillon2.png";
	
	/* Chargement des sprites */
	int nSprite = 3;
	
	// Nombre de textures pour chaque sprite (-1 pour une raison inconnue)
	int *nSpriteListe = (int*) malloc(nSprite*sizeof(int));
	nSpriteListe[0] = 0;
	nSpriteListe[1] = 5;
	nSpriteListe[2] = 4;
	
	// Liste des indices des textures pour chaque sprite
	int **textId = (int**) malloc(nSprite*sizeof(int*));
	int i;
	for(i = 0; i<nSprite; i++) textId[i] = (int*) malloc(nSpriteListe[i]*sizeof(int));
	textId[0][0] = 0;
	textId[1][0] = 0;
	textId[1][1] = 1;
	textId[1][2] = 2;
	textId[1][3] = 3;
	textId[1][4] = 4;
	textId[1][5] = 5;
	textId[2][0] = 6;
	textId[2][1] = 7;
	textId[2][2] = 8;
	textId[2][3] = 7;
	textId[2][4] = 8;
	
	// Position, taille et angle de chaque sprite
	int *x = (int*) malloc(nSprite*sizeof(int));
	int *y = (int*) malloc(nSprite*sizeof(int));
	int *w = (int*) malloc(nSprite*sizeof(int));
	int *h = (int*) malloc(nSprite*sizeof(int));
	int *angle = (int*) malloc(nSprite*sizeof(int));
	x[0] = 1;
	y[0] = 1;
	w[0] = 50;
	h[0] = 80;
	angle[0] = 0;
	
	x[1] = 1;
	y[1] = 1;
	w[1] = 50;
	h[1] = 80;
	angle[1] = 0;
	
	x[2] = 100;
	y[2] = 1;
	w[2] = 90;
	h[2] = 40;
	angle[2] = 0;
	
	
	/* Chargement de la SDL */
	//sdlMedia_t * initSDL(int nString, char ** stringListe, int **stringPosListe, int nTexture, char **fileNameListe, int nSprite, int **textId, int *nSpriteListe, int *x, int *y, int *w, int *h, int *angle)
	media = initSDL(1200, 800, 1, str, pos, nTexture, fileNameListe, animSpeed, nSprite, textId, nSpriteListe, x, y, w, h, angle);
	
	
	level = initLevel(26);	
	int running = 1;
	while(running){
	
	/* Creation de niveau */
		reloadLevel(level, nLevel);
	
	
	/* /!\ Lancement du jeu ICI /!\ */
	
		media->spriteListe[level->spider->spriteIdRepos]->isShowed = 1;
		media->spriteListe[level->papillon->spriteRepos]->isShowed = 1;
		
		int v = loop(media, level);
		media->state = 0;
		if(v == 1) running = 0;
		else{
			if(v == 2) score++;
			nLevel ++;
			char s[200] ;
			sprintf(s, "%d / %d ", score, nLevel);
			str[0] = s;
			
			loadListeString(media, 1, str, pos);
		}
		//if(nLevel==3) running = 0;
	} 
	
	
	endSDL(media);


	free(str);
	free(pos);
	
	return 0;
}

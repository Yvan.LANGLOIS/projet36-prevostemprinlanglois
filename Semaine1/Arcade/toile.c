#include "toile.h"

noeud_t* getNoeudsCharge(int nSommet, int nLevel){

	
	noeud_t *liste = (noeud_t*) malloc(nSommet*sizeof(noeud_t));

	if(nLevel == 0){
		liste[0].x = 80;
		liste[0].y = 100;
		liste[1].x = 170;
		liste[1].y = 100;
		liste[2].x = 260;
		liste[2].y = 130;
		liste[3].x = 410;
		liste[3].y = 90;
		liste[4].x = 330;
		liste[4].y = 200;
	}

	if (nLevel == 1)
	{
	liste[0].x = 10;
	liste[0].y = 300;
	
	liste[1].x = 50;
	liste[1].y = 310;
	
	liste[2].x = 190;
	liste[2].y = 230;
	
	liste[3].x = 180;
	liste[3].y = 180;
	
	liste[4].x = 230;
	liste[4].y = 290;
	
	liste[5].x = 200;
	liste[5].y = 400;
	
	liste[6].x = 230;
	liste[6].y = 370;
	liste[7].x = 250;
	liste[7].y = 350;
	liste[8].x = 380;
	liste[8].y = 320;
	liste[9].x = 470;
	liste[9].y = 250;
	liste[10].x = 480;
	liste[10].y = 350;
	liste[11].x = 500;
	liste[11].y = 230;

	liste[12].x = 510;
	liste[12].y = 380;
	liste[13].x = 530;
	liste[13].y = 200;
	}

	if (nLevel == 2)
	{
	liste[0].x = 10;
	liste[0].y = 300;
	
	liste[1].x = 100;
	liste[1].y = 310;
	
	liste[2].x = 300;
	liste[2].y = 150;
	
	liste[3].x = 305;
	liste[3].y = 110;
	
	liste[4].x = 302;
	liste[4].y = 200;
	
	liste[5].x = 200;
	liste[5].y = 300;
		
	liste[6].x = 230;
	liste[6].y = 400;

	liste[7].x = 210;
	liste[7].y = 500;

	liste[8].x = 200;
	liste[8].y = 550;

	liste[9].x = 300;
	liste[9].y = 310;

	liste[10].x = 380;
	liste[10].y = 410;

	liste[11].x = 430;
	liste[11].y = 300;

	liste[12].x = 500;
	liste[12].y = 250;

	liste[13].x = 550;
	liste[13].y = 230;

	liste[14].x = 430;
	liste[14].y = 480;

	liste[15].x = 530;
	liste[15].y = 550;
	}
	return liste;
}

int** getMatriceConnectionCharge(int taille, int nLevel){
	
	int** matrice = (int**)malloc(sizeof(int*) * taille);
	
	for (int i = 0; i < taille; i ++)
	{
	matrice[i] = (int*)malloc(sizeof(int) * taille);
	}
	
	for (int i = 0; i < taille; i ++)
	{
	for (int j = 0; j <= i; j ++)
	{
	    matrice[i][j] = 0;
	    matrice[j][i] = 0;
	}
	}
	if(nLevel == 0){
		matrice[0][1] = 1;
		matrice[1][0] = 1;
		matrice[1][2] = 1;
		matrice[2][1] = 1;
		matrice[2][3] = 1;
		matrice[3][2] = 1;
		matrice[2][4] = 1;
		matrice[4][2] = 1;
		matrice[4][3] = 1;
		matrice[3][4] = 1;
		
	}
	if(nLevel==1){
	matrice[0][1] = 1;
	matrice[1][0] = 1;
	matrice[1][2] = 1;
	matrice[1][4] = 1;
	matrice[1][6] = 1;
	matrice[1][7] = 1;
	matrice[2][1] = 1;
	matrice[2][3] = 1;
	matrice[2][4] = 1;
	matrice[3][2] = 1;
	matrice[4][1] = 1;
	matrice[4][2] = 1;
	matrice[4][7] = 1;
	matrice[4][8] = 1;
	matrice[5][6] = 1;
	matrice[6][1] = 1;
	matrice[6][5] = 1;
	matrice[6][7] = 1;
	matrice[7][1] = 1;
	matrice[7][4] = 1;
	matrice[7][6] = 1;
	matrice[7][8] = 1;
	matrice[8][4] = 1;
	matrice[8][7] = 1;
	matrice[8][9] = 1;
	matrice[8][10] = 1;
	matrice[9][8] = 1;
	matrice[9][10] = 1;
	matrice[9][11] = 1;
	matrice[10][8] = 1;
	matrice[10][9] = 1;
	matrice[10][11] = 1;
	matrice[10][12] = 1;
	matrice[11][9] = 1;
	matrice[11][10] = 1;
	matrice[11][13] = 1;
	matrice[12][10] = 1;
	matrice[13][11] = 1;
	}

	if(nLevel==2){
	matrice[0][1] = 1;
	matrice[1][0] = 1;
	matrice[1][2] = 1;
	matrice[1][5] = 1;
	matrice[1][6] = 1;
	matrice[1][7] = 1;
	matrice[2][1] = 1;
	matrice[2][3] = 1;
	matrice[2][4] = 1;
	matrice[2][12] = 1;
	matrice[3][2] = 1;
	matrice[4][2] = 1;
	matrice[4][5] = 1;
	matrice[4][9] = 1;
	matrice[4][11] = 1;
	matrice[4][12] = 1;
	matrice[5][1] = 1;
	matrice[5][4] = 1;
	matrice[5][6] = 1;
	matrice[5][9] = 1;
	matrice[6][1] = 1;
	matrice[6][5] = 1;
	matrice[6][7] = 1;
	matrice[6][9] = 1;
	matrice[6][10] = 1;
	matrice[7][6] = 1;
	matrice[7][8] = 1;
	matrice[7][14] = 1;
	matrice[8][7] = 1;
	matrice[9][4] = 1;
	matrice[9][5] = 1;
	matrice[9][6] = 1;
	matrice[9][10] = 1;
	matrice[9][11] = 1;
	matrice[10][6] = 1;
	matrice[10][9] = 1;
	matrice[10][11] = 1;
	matrice[10][14] = 1;
	matrice[11][4] = 1;
	matrice[11][9] = 1;
	matrice[11][10] = 1;
	matrice[11][12] = 1;
	matrice[12][2] = 1;
	matrice[12][4] = 1;
	matrice[12][11] = 1;
	matrice[12][13] = 1;
	matrice[12][14] = 1;
	matrice[13][12] = 1;
	matrice[14][7] = 1;
	matrice[14][10] = 1;
	matrice[14][12] = 1;
	matrice[14][15] = 1;
	matrice[15][14] = 1;
	}
	return matrice;
	
}

void reloadLevel(level_t *level, int nLevel){
	int taille = 5;
	if(nLevel==0) taille = 5;
	if(nLevel==1) taille = 14;
	if(nLevel==2) taille = 16;
	if(nLevel>=3) taille = 3+random()%9;
	
	level->taille = taille;
	printf("level %d taille%d\n", nLevel, taille);
	
	if(nLevel<3){
	level->listeNoeuds = getNoeudsCharge(taille, nLevel);
	level->matriceConnexion = getMatriceConnectionCharge(taille, nLevel);
	}
	else{
	level->listeNoeuds = getNoeuds(taille, 900, 500);
	level->matriceConnexion = getMatriceConnexion(taille, 80);
	}

	
	level->spider->iNoeud = 1;
	level->spider->iNoeudPrec = 0;
	level->spider->state = 0;
	level->spider->spriteIdRepos = 0;
	level->spider->spriteIdMvt = 1;
	level->spider->currentSpriteId = level->spider->spriteIdRepos;
	
	int papillonNoeud = random()%level->taille;
	if(nLevel==0) papillonNoeud = 3;
	if(nLevel==1) papillonNoeud = 8;
	if(nLevel==2) papillonNoeud = 9;
	if(nLevel>=3) while(papillonNoeud == level->spider->iNoeud)papillonNoeud = random()%level->taille;
	level->papillon->iNoeud = papillonNoeud;
	level->papillon->spriteRepos = 2;
	level->papillon->currentSpriteId = level->papillon->spriteRepos;
	
	
}

level_t* initLevel(int pasAnimSpider){
	int taille = 5;

	level_t* level = (level_t*) malloc(sizeof(level_t));
	level->taille = taille;
	level->spider = (spider_t*) malloc(sizeof(spider_t));
	level->papillon = (papillon_t*) malloc(sizeof(papillon_t));
	level->spider->pasAnim = pasAnimSpider;						// Pas de mouvement de l'araignee
	
	reloadLevel(level, 0);
	
	return level;

}

noeud_t* getNoeuds(int nSommet, int width, int height){
	noeud_t *liste = (noeud_t*) malloc(nSommet*sizeof(noeud_t));
	/**/
	int i;
	
	for(i=0; i<nSommet; i++){
		liste[i].x = 80+rand()%900;
		liste[i].y = 80+rand()%500;
	}
	//*/
	/*
	if(liste==NULL) printf("Erreur\n");
	int taux = 9;	
	int i, j;
	int **tab = (int**)malloc(sizeof(int*)*taux);
	for(i = 0; i<taux; i++) {
		tab[i] = (int*)malloc(nSommet*sizeof(int));	
		if(tab[i] == NULL) printf("erreur\n");
	}
	
	for(i = 0; i<taux; i++){
		for(j = 0; j<nSommet; j++){
			tab[i][j] = 0;
		}
	}
	
	int libre = (nSommet*taux);
	int caseW = width/nSommet, caseH = height/taux;
	for(i=0; i<nSommet-1; i++){
		int r = rand()%libre;
		int u  = 0, v = 0, k = 0;
		int code = 0;
		while(u<taux-1 && code == 0){
			v = 0;
			while(v<nSommet && code == 0){
				if(tab[u][v]==0){
					if(k==r) code = 1;					
					k++;
				}
				v++;
			}
			u++;
		}
		u--; v--;
		
		tab[u][v] = 1;
		if(u>0) tab[u-1][v] = 1;
		if(v>0) tab[u][v-1] = 1;
		if(u>0 && v>0) tab[u-1][v-1] = 1;
		if(u<nSommet) tab[u+1][v] = 1;
		if(v<9) tab[u][v+1] = 1;
		if(u<nSommet && v<9) tab[u+1][v+1] = 1;
		if(u>0 && v<9) tab[u-1][v+1] = 1;
		if(u<nSommet && v>0) tab[u+1][u-1] = 1;
		libre-=9;
		printf("%d / %d\n",i,nSommet);
		liste[i].x = caseW*u+rand()%(caseW/2);
		liste[i].y = caseH*v+rand()%(caseH/2);
	}
	
	free(tab);
	
	*/
	
	
	
	return liste;
}

int** getMatriceConnexion(int taille, int proba_ajout_liaison){

    int** matrice = (int**)malloc(sizeof(int*) * taille);

    int tab[taille];
    for (int i = 0; i < taille; i ++)
        tab[i] = i;

    for (int i = 0; i < taille; i ++)
    {
        matrice[i] = (int*)malloc(sizeof(int) * taille);
    }

    //Creation de l'arbre binaire

    for (int i = 0; i < taille; i ++)
    {
        for (int j = 0; j <= i; j ++)
        {
            matrice[i][j] = 0;
            matrice[j][i] = 0;
        }
    }

    int indice_des_feuilles = 1;
    int indice_des_parents = 0;

    if (taille % 2 != 0)
    {
        while (indice_des_feuilles < taille)
        {
            for (int i = 0; i < 2; i++)
            {
                matrice[indice_des_parents][indice_des_feuilles] = 1;
                matrice[indice_des_feuilles][indice_des_parents] = 1;
                indice_des_feuilles += 1;
            }
            indice_des_parents += 1;
        }
    }
    else
    {
        while (indice_des_feuilles < taille-1)
        {
            for (int i = 0; i < 2; i++)
            {
                matrice[tab[indice_des_parents]][tab[indice_des_feuilles]] = 1;
                matrice[tab[indice_des_feuilles]][tab[indice_des_parents]] = 1;
                indice_des_feuilles += 1;
            }
            indice_des_parents += 1;
        }
        matrice[indice_des_parents][indice_des_feuilles] = 1;
        matrice[indice_des_feuilles][indice_des_parents] = 1;
    }

    // Proba d'ajouter une liaison entre 2 sommets

    srand(time(NULL));
    for (int i = 0 ; i < taille ; i ++)
    {
        for (int j = 0 ; j < taille ; j ++)
        {
            if (matrice[i][j] == 0)
            {
                int nb_aleat = rand()%101;

                if( nb_aleat <= proba_ajout_liaison)
                {
                    int nb_aleat2 = rand()%2;
                    matrice[i][j] = nb_aleat2;
                    matrice[j][i] = nb_aleat2;
                }
            }
        }
    }

    for (int i = 0; i < taille ; i ++)
        matrice[i][i] = 0;
    return matrice;
}

#ifndef __TOILE_H__
#define __TOILE_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>

typedef struct spider{
	int iNoeud;
	int iNoeudPrec;
	int state;
	
	int currentSpriteId;
	int pasAnim;
	
	int spriteIdRepos;
	int spriteIdMvt;
} spider_t;

typedef struct papillon{
	int iNoeud;
	
	int currentSpriteId;
	
	int spriteRepos;
	
} papillon_t;

typedef struct noeud{
	int x;
	int y;
} noeud_t;

typedef struct level{
	int taille;
	int **matriceConnexion;
	noeud_t *listeNoeuds;
	spider_t *spider;
	papillon_t *papillon;
	
} level_t;


int** getMatriceConnexion(int taille, int proba_ajout_liaison);
noeud_t* getNoeuds(int taille, int width, int height);
level_t* initLevel(int pasAnimSpider);
void reloadLevel(level_t *level, int currentLvl);



#endif

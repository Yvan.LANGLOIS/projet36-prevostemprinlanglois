#include "SDL.h"

void drawCircle(SDL_Renderer* renderer, int centerX, int centerY, int radius) 
{
    for (int angle = 0; angle < 360; angle++) 
    {
        float radian = angle * (M_PI / 180.0);
        int x = centerX + radius * cos(radian);
        int y = centerY + radius * sin(radian);
        SDL_RenderDrawPoint(renderer, x, y);
    }
}
void creation_fenetre_noire(void)
{
    // taille de l'écran
    SDL_Init(SDL_INIT_VIDEO);

    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);

    int screenWidth = displayMode.w;
    int screenHeight = displayMode.h;

    //création de la fenêtre
    SDL_Window* window = SDL_CreateWindow("Fenêtre noire", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        printf("Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        return ;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL) {
        printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
        return ;
    }

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    SDL_Delay(3000); // Attendre pendant 3 secondes

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

}



void afficher_X_en_couleur(void)
{
    SDL_Window *window = NULL;

    // Initialisation de la SDL  + gestion de l'échec possible 
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", 
    SDL_GetError());                // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
    }

    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);

    int screenWidth = displayMode.w;
    int screenHeight = displayMode.h;

    int nombre_de_fenetre_par_diagonale = 10;

    int long_Width = screenWidth / nombre_de_fenetre_par_diagonale;
    int long_Height = screenHeight / nombre_de_fenetre_par_diagonale;

    for (int i = 0; i<nombre_de_fenetre_par_diagonale - 1; i++) // -1 à cause de la barre en bas
    {
        // Création de la fenêtre pour la premiere diagonale
        window = SDL_CreateWindow(
        "Fenêtre ",                    // codage en utf8, donc accents possibles
        long_Width * i, long_Height * i,                                  // coin haut gauche en haut gauche de l'écran
        long_Height, long_Width,                              // largeur = 400, hauteur = 300
        SDL_WINDOW_RESIZABLE);                 // redimensionnable

        if (window == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", 
        SDL_GetError());                 // échec de la création de la fenêtre
        SDL_Quit();                              // On referme la SDL       
        exit(EXIT_FAILURE);
        }

        SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
        if (renderer == NULL) {
            printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
            return ;
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
        SDL_RenderClear(renderer);
        SDL_RenderPresent(renderer);

        
        // Création de la fenêtre pour la deuxieme diagonale
        
        window = SDL_CreateWindow(
        "Fenêtre ",                    // codage en utf8, donc accents possibles
        screenWidth - long_Width * (i+1), long_Height * i,                                  // coin haut gauche en haut gauche de l'écran
        long_Height, long_Width,                              // largeur = 400, hauteur = 300
        SDL_WINDOW_RESIZABLE);                 // redimensionnable

        if (window == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", 
        SDL_GetError());                 // échec de la création de la fenêtre
        SDL_Quit();                              // On referme la SDL       
        exit(EXIT_FAILURE);
        }

        SDL_Renderer* renderer2 = SDL_CreateRenderer(window, -1, 0);
        if (renderer2 == NULL) {
            printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
            return ;
        }

        SDL_SetRenderDrawColor(renderer2, 0, 255, 0, 0);
        SDL_RenderClear(renderer2);
        SDL_RenderPresent(renderer2);
    }
    
    // Normalement, on devrait ici remplir les fenêtres... 
    SDL_Delay(10000);                           // Pause exprimée  en ms

    // et on referme tout ce qu'on a ouvert en ordre inverse de la création 

    SDL_DestroyWindow(window);               // la fenêtre 1     
    SDL_Quit();                                // la SDL
}



void creation_drapeau_fr(void)
{    SDL_Window *window = NULL;

    // Initialisation de la SDL  + gestion de l'échec possible 
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", 
    SDL_GetError());                // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
    }

    window = SDL_CreateWindow(
        "Fenêtre",                    // codage en utf8, donc accents possibles
    0, 0,                                  // coin haut gauche en haut gauche de l'écran
    400, 400,                              // largeur = 400, hauteur = 400
    SDL_WINDOW_RESIZABLE);                 // redimensionnable

    if (window == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", 
        SDL_GetError());                 // échec de la création de la fenêtre
        SDL_Quit();                              // On referme la SDL       
        exit(EXIT_FAILURE);
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL) {
        printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
        return ;
    }

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Fond vert
    SDL_RenderClear(renderer);

    SDL_Rect rectangle;                                                
                                            
    SDL_SetRenderDrawColor(renderer,255, 0, 0,255);
       rectangle.x = 50;                                             // x haut gauche du rectangle
       rectangle.y = 50;                                                  // y haut gauche du rectangle
       rectangle.w = 100;                                                // sa largeur (w = width)
       rectangle.h = 300;                                                // sa hauteur (h = height)

    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); // Couleur du rectangle bleu
    SDL_RenderFillRect(renderer, &rectangle); // Tracer le rectangle

    SDL_SetRenderDrawColor(renderer,255, 0, 0,255);
    rectangle.x = 150;                                             // x haut gauche du rectangle
    rectangle.y = 50;                                                  // y haut gauche du rectangle
    rectangle.w = 100;                                                // sa largeur (w = width)
    rectangle.h = 300;                                                // sa hauteur (h = height)

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Couleur du rectangle blanc
    SDL_RenderFillRect(renderer, &rectangle); // Tracer le rectangle

    SDL_SetRenderDrawColor(renderer,255, 0, 0,255);
    rectangle.x = 250;                                             // x haut gauche du rectangle
    rectangle.y = 50;                                                  // y haut gauche du rectangle
    rectangle.w = 100;                                                // sa largeur (w = width)
    rectangle.h = 300;                                                // sa hauteur (h = height)
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Couleur du rectangle rouge
    SDL_RenderFillRect(renderer, &rectangle); // Tracer le rectangle

    SDL_RenderPresent(renderer);

    SDL_Delay(4000);                           // Pause exprimée  en ms

    // et on referme tout ce qu'on a ouvert en ordre inverse de la création 
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);               // la fenêtre 1     
    SDL_Quit();

    return ;
}



void affichage_gradient_de_couleur(void)
{
        SDL_Window *window = NULL;

     // Initialisation de la SDL  + gestion de l'échec possible 
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", 
    SDL_GetError());                // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
    }

    window = SDL_CreateWindow(
        "Fenêtre",                    // codage en utf8, donc accents possibles
    0, 0,                                  // coin haut gauche en haut gauche de l'écran
    450, 400,                              // largeur = 400, hauteur = 400
    SDL_WINDOW_RESIZABLE);                 // redimensionnable

    if (window == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", 
        SDL_GetError());                 // échec de la création de la fenêtre
        SDL_Quit();                              // On referme la SDL       
        exit(EXIT_FAILURE);
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL) {
        printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
        return ;
    }

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Fond vert
    SDL_RenderClear(renderer);

    SDL_Rect rectangle;

    int differentes_couleurs = 255;

    for (int i = 0; i < differentes_couleurs; i++)
    {
        SDL_SetRenderDrawColor(renderer,255, 0, 0,255);
        rectangle.x = 50+i;                                             // x haut gauche du rectangle
        rectangle.y = 50;                                                  // y haut gauche du rectangle
        rectangle.w = 100;                                                // sa largeur (w = width)
        rectangle.h = 300;                                                // sa hauteur (h = height)
        SDL_SetRenderDrawColor(renderer, i, 0, 0, 255); // Couleur du rectangle rouge
        SDL_RenderFillRect(renderer, &rectangle); // Tracer le rectangle
        SDL_Delay(5);
        SDL_RenderPresent(renderer);
    }

    SDL_Delay(4000);                           // Pause exprimée  en ms

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);               // la fenêtre 1     
    SDL_Quit();

    return ;
}

void affichage_gradient_de_couleur_v2(void)
{

    SDL_Window *window = NULL;

     // Initialisation de la SDL  + gestion de l'échec possible 
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", 
    SDL_GetError());                // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
    }

    window = SDL_CreateWindow(
        "Fenêtre",                    // codage en utf8, donc accents possibles
    0, 0,                                  // coin haut gauche en haut gauche de l'écran
    400, 400,                              // largeur = 400, hauteur = 400
    SDL_WINDOW_RESIZABLE);                 // redimensionnable

    if (window == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", 
        SDL_GetError());                 // échec de la création de la fenêtre
        SDL_Quit();                              // On referme la SDL       
        exit(EXIT_FAILURE);
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL) {
        printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
        return ;
    }


    int differentes_couleurs = 255;
    for (int j= 0 ; j < 3 ; j++)
    {
        SDL_Delay(5);
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Fond blanc
        SDL_RenderClear(renderer);
        SDL_Delay(5);

        SDL_Rect rectangle;

        for (int i = 0; i < differentes_couleurs ; i++) 
        {
            SDL_SetRenderDrawColor(renderer,255, 0, 0,255);
            rectangle.x = 50+i;                                             // x haut gauche du rectangle
            rectangle.y = 50;                                                  // y haut gauche du rectangle
            rectangle.w = 100;                                                // sa largeur (w = width)
            rectangle.h = 300;                                                // sa hauteur (h = height)
            SDL_SetRenderDrawColor(renderer, i, 0, 0, 255); // Couleur du rectangle rouge
            SDL_RenderFillRect(renderer, &rectangle); // Tracer le rectangle
            SDL_Delay(15);
            SDL_RenderPresent(renderer);
        }
        for (int i = 0; i < differentes_couleurs - 50; i++) // on remplace en blanc
        {
            SDL_SetRenderDrawColor(renderer,255, 255, 255,255);
            rectangle.x = 355-i;                                             // x haut gauche du rectangle
            rectangle.y = 50;                                                  // y haut gauche du rectangle
            rectangle.w = 100;                                                // sa largeur (w = width)
            rectangle.h = 300;                                                // sa hauteur (h = height)
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Couleur du rectangle rouge
            SDL_RenderFillRect(renderer, &rectangle); // Tracer le rectangle
            SDL_Delay(15);
            SDL_RenderPresent(renderer);
        }
    }

    SDL_Delay(2000);                           // Pause exprimée  en ms

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);               // la fenêtre 1     
    SDL_Quit();

    return ;
}

void fusee_tourne_autour_terre(void)
{
        SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("Rotation d'une image JPEG", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        printf("Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        return;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL) {
        printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
        return;
    }

    // Chargement de l'image
    SDL_Surface* imageSurfaceTerre = IMG_Load("./Terre.xcf");
    if (imageSurfaceTerre == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return;
    }
    SDL_Surface* imageSurfaceFusee = IMG_Load("./Tintin.xcf");
    if (imageSurfaceFusee == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return;
    }

    // Conversion de l'image en texture
    SDL_Texture* imageTextureTerre = SDL_CreateTextureFromSurface(renderer, imageSurfaceTerre);
    if (imageTextureTerre == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return;
    }
    SDL_Texture* imageTextureFusee = SDL_CreateTextureFromSurface(renderer, imageSurfaceFusee);
    if (imageTextureFusee == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return;
    }

    // Libération de la surface car nous n'en avons plus besoin
    SDL_FreeSurface(imageSurfaceTerre);
    SDL_FreeSurface(imageSurfaceFusee);

    // Obtenir les dimensions de l'image
    int imageWidthTerre, imageHeightTerre;
    SDL_QueryTexture(imageTextureTerre, NULL, NULL, &imageWidthTerre, &imageHeightTerre);
    int imageWidthFusee, imageHeightFusee;
    SDL_QueryTexture(imageTextureFusee, NULL, NULL, &imageWidthFusee, &imageHeightFusee);

    // Affichage de l'image de la Terre au centre de la fenêtre
    SDL_Rect destRect;
    destRect.x = (800 - imageWidthTerre) / 2;
    destRect.y = (600 - imageHeightTerre) / 2;
    destRect.w = imageWidthTerre;
    destRect.h = imageHeightTerre;

    // Position initiale de la fusee
    int imageFuseeX = (800 - imageWidthFusee) / 2;
    int imageFuseeY = (600 - imageHeightFusee) / 2;

    double angle = 0; // Angle de rotation
    int IMAGE_SPEED = 5;

    SDL_bool program_on = SDL_TRUE;                         // Booléen pour dire que le programme doit continuer
    SDL_Event event;

    while (program_on) {
        while (SDL_PollEvent(&event)) 
        {
            if (event.type == SDL_QUIT) 
            {
                program_on = SDL_FALSE;
                break;
            }

            if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    program_on = SDL_FALSE;
                    break;
                }
                else if (event.key.keysym.sym == SDLK_LEFT) {
                    imageFuseeX -= IMAGE_SPEED;
                }
                else if (event.key.keysym.sym == SDLK_RIGHT) {
                    imageFuseeX += IMAGE_SPEED;
                }
                else if (event.key.keysym.sym == SDLK_UP) {
                    imageFuseeY -= IMAGE_SPEED;
                }
                else if (event.key.keysym.sym == SDLK_DOWN) {
                    imageFuseeY += IMAGE_SPEED;
                }
            }
        }

        // Rotation de l'image
        angle += 0.01; // Augmenter l'angle de rotation

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Effacer le rendu
        SDL_RenderClear(renderer);

        // Affichage de la Terre avec rotation
        SDL_RenderCopyEx(renderer, imageTextureTerre, NULL, &destRect, angle, NULL, SDL_FLIP_NONE);

        // Affichage de la fusee à sa nouvelle position
        SDL_Rect destRect1 = { imageFuseeX, imageFuseeY, imageWidthFusee, imageHeightFusee };
        SDL_RenderCopy(renderer, imageTextureFusee, NULL, &destRect1);

        SDL_RenderPresent(renderer);
    }

    // Libération de la texture et destruction des autres ressources SDL
    SDL_DestroyTexture(imageTextureTerre);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}


void fusee_guidee_tourne_autour_terre(void)
{
        SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("Rotation d'une image JPEG", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        printf("Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        return;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL) {
        printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
        return;
    }

    // Chargement de l'image
    SDL_Surface* imageSurfaceTerre = IMG_Load("./Terre.xcf");
    if (imageSurfaceTerre == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return;
    }
    SDL_Surface* imageSurfaceFusee = IMG_Load("./Tintin.xcf");
    if (imageSurfaceFusee == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return;
    }
    SDL_Surface* imageSurfaceFlamme1 = IMG_Load("./flamme1.xcf");
    if (imageSurfaceFlamme1 == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return;
    }
    SDL_Surface* imageSurfaceFlamme2 = IMG_Load("./flamme2.xcf");
    if (imageSurfaceFlamme2 == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return;
    }

    // Conversion de l'image en texture
    SDL_Texture* imageTextureTerre = SDL_CreateTextureFromSurface(renderer, imageSurfaceTerre);
    if (imageTextureTerre == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return;
    }
    SDL_Texture* imageTextureFusee = SDL_CreateTextureFromSurface(renderer, imageSurfaceFusee);
    if (imageTextureFusee == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return;
    }
    SDL_Texture* imageTextureFlamme1 = SDL_CreateTextureFromSurface(renderer, imageSurfaceFlamme1);
    if (imageTextureFlamme1 == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return;
    }
    SDL_Texture* imageTextureFlamme2 = SDL_CreateTextureFromSurface(renderer, imageSurfaceFlamme2);
    if (imageTextureFlamme2 == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return;
    }

    // Libération de la surface car nous n'en avons plus besoin
    SDL_FreeSurface(imageSurfaceTerre);
    SDL_FreeSurface(imageSurfaceFusee);
    SDL_FreeSurface(imageSurfaceFlamme1);
    SDL_FreeSurface(imageSurfaceFlamme2);

    // Obtenir les dimensions de l'image
    int imageWidthTerre, imageHeightTerre;
    SDL_QueryTexture(imageTextureTerre, NULL, NULL, &imageWidthTerre, &imageHeightTerre);
    int imageWidthFusee, imageHeightFusee;
    SDL_QueryTexture(imageTextureFusee, NULL, NULL, &imageWidthFusee, &imageHeightFusee);
    int imageWidthFlamme1, imageHeightFlamme1;
    SDL_QueryTexture(imageTextureFlamme1, NULL, NULL, &imageWidthFlamme1, &imageHeightFlamme1);
    int imageWidthFlamme2, imageHeightFlamme2;
    SDL_QueryTexture(imageTextureFlamme2, NULL, NULL, &imageWidthFlamme2, &imageHeightFlamme2);
    // Affichage de l'image de la Terre au centre de la fenêtre
    SDL_Rect destRect;
    destRect.x = (800 - imageWidthTerre) / 2;
    destRect.y = (600 - imageHeightTerre) / 2;
    destRect.w = imageWidthTerre;
    destRect.h = imageHeightTerre;

    // Position initiale de la fusee
    int imageFuseeX = (800 - imageWidthFusee) / 2;
    int imageFuseeY = (600 - imageHeightFusee) / 2;

    // Position des flammes sous la fusee
    int imageFlamme1X = imageFuseeX;
    int imageFlamme1Y = imageFuseeY + imageHeightFusee;
    int imageFlamme2X = imageFuseeX;
    int imageFlamme2Y = imageFuseeY + imageHeightFusee;

    double angle = 0; // Angle de rotation

    // gestion de l'alternance de l'affichage des flammes
    int dureeFlamme1 = 0;
    int dureeFlamme2 = 0;
    Bool alternanceFlamme = true;

    SDL_bool program_on = SDL_TRUE;                         // Booléen pour dire que le programme doit continuer
    SDL_Event event;

    while (program_on) {
        while (SDL_PollEvent(&event)) 
        {
            if (event.type == SDL_QUIT) 
            {
                program_on = SDL_FALSE;
                break;
            }

            if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    program_on = SDL_FALSE;
                    break;
                }
            }
        }
        // Deplacement aleatoire 
        int d1 = rand()%3;
        int d2 = rand()%3;
        int d3 = rand()%3;
        int d4 = rand()%3;

        // Deplacement fusee
        imageFuseeX -= d1;
        imageFuseeX += d2;
        imageFuseeY -= d3;
        imageFuseeY += d4;

        //deplacement flamme1
        imageFlamme1X -= d1;
        imageFlamme1X += d2;
        imageFlamme1Y -= d3;
        imageFlamme1Y += d4;

        //deplacement flamme2
        imageFlamme2X -= d1;
        imageFlamme2X += d2;
        imageFlamme2Y -= d3;
        imageFlamme2Y += d4;

        // Rotation de l'image
        angle += 0.05; // Augmenter l'angle de rotation

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Effacer le rendu
        SDL_RenderClear(renderer);

        // Affichage de la Terre avec rotation
        SDL_RenderCopyEx(renderer, imageTextureTerre, NULL, &destRect, angle, NULL, SDL_FLIP_NONE);

        // Affichage de la fusee à sa nouvelle position
        SDL_Rect destRect1 = { imageFuseeX, imageFuseeY, imageWidthFusee, imageHeightFusee };
        SDL_RenderCopy(renderer, imageTextureFusee, NULL, &destRect1);

        // Affichage alternance de flamme
        SDL_Rect destRect2 = { imageFlamme1X, imageFlamme1Y, imageWidthFlamme1, imageHeightFlamme1 };
        

        while (alternanceFlamme && (dureeFlamme1 < 100))
        {
            SDL_RenderCopyEx(renderer, imageTextureFlamme1, NULL, &destRect2, 180, NULL, SDL_FLIP_NONE);
            dureeFlamme1 += 1;
        }
        while (!alternanceFlamme && (dureeFlamme2 < 100))
        {
            SDL_RenderCopyEx(renderer, imageTextureFlamme2, NULL, &destRect2, 180, NULL, SDL_FLIP_NONE);
            dureeFlamme2 += 1;
        }

        if (dureeFlamme1 == 100)
        {
            dureeFlamme1 = 0;
            alternanceFlamme = false;
        }
        if (dureeFlamme2 == 100)
        {
            dureeFlamme2 = 0;
            alternanceFlamme = true;
        }

        SDL_RenderPresent(renderer);
    }

    // Libération de la texture et destruction des autres ressources SDL
    SDL_DestroyTexture(imageTextureTerre);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
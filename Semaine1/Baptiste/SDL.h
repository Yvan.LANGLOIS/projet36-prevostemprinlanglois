#ifndef __SDL_H__
#define __SDL_H__

#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

typedef enum
	{
		false,
		true
	}Bool;

    
void drawCircle(SDL_Renderer* renderer, int centerX, int centerY, int radius);
void creation_fenetre_noire(void);
void afficher_X_en_couleur(void);
void creation_drapeau_fr(void);
void affichage_gradient_de_couleur(void);
void affichage_gradient_de_couleur_v2(void);
void fusee_tourne_autour_terre(void);
void fusee_guidee_tourne_autour_terre(void);
#endif
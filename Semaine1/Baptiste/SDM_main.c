#include "SDL.h"



int main(int argc, char **argv) 
{
    (void)argc;
    (void)argv;
     SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("Rotation d'une image JPEG", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        printf("Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        return 0;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL) {
        printf("Erreur lors de la création du rendu : %s\n", SDL_GetError());
        return 0;
    }

    // Chargement de l'image
    SDL_Surface* imageSurfaceTerre = IMG_Load("./Terre.xcf");
    if (imageSurfaceTerre == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return 0;
    }
    SDL_Surface* imageSurfaceFusee = IMG_Load("./Tintin.xcf");
    if (imageSurfaceFusee == NULL) {
        printf("Erreur lors du chargement de l'image : %s\n", IMG_GetError());
        return 0;
    }

    // Conversion de l'image en texture
    SDL_Texture* imageTextureTerre = SDL_CreateTextureFromSurface(renderer, imageSurfaceTerre);
    if (imageTextureTerre == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return 0;
    }
    SDL_Texture* imageTextureFusee = SDL_CreateTextureFromSurface(renderer, imageSurfaceFusee);
    if (imageTextureFusee == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        return 0;
    }

    // Libération de la surface car nous n'en avons plus besoin
    SDL_FreeSurface(imageSurfaceTerre);
    SDL_FreeSurface(imageSurfaceFusee);

    // Obtenir les dimensions de l'image
    int imageWidthTerre, imageHeightTerre;
    SDL_QueryTexture(imageTextureTerre, NULL, NULL, &imageWidthTerre, &imageHeightTerre);
    int imageWidthFusee, imageHeightFusee;
    SDL_QueryTexture(imageTextureFusee, NULL, NULL, &imageWidthFusee, &imageHeightFusee);

    // Affichage de l'image de la Terre au centre de la fenêtre
    SDL_Rect destRect;
    destRect.x = (800 - imageWidthTerre) / 2;
    destRect.y = (600 - imageHeightTerre) / 2;
    destRect.w = imageWidthTerre;
    destRect.h = imageHeightTerre;

    // Position initiale de la fusee
    int imageFuseeX = (800 - imageWidthFusee) / 2;
    int imageFuseeY = (600 - imageHeightFusee) / 2;

    double angle = 0; // Angle de rotation
    int IMAGE_SPEED = 5;

    SDL_bool program_on = SDL_TRUE;                         // Booléen pour dire que le programme doit continuer
    SDL_Event event;

    while (program_on) 
    {
        while (SDL_PollEvent(&event)) 
        {
            if (event.type == SDL_QUIT) 
            {
                program_on = SDL_FALSE;
                break;
            }

            if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    program_on = SDL_FALSE;
                    break;
                }
                else if (event.key.keysym.sym == SDLK_LEFT) {
                    imageFuseeX -= IMAGE_SPEED;
                }
                else if (event.key.keysym.sym == SDLK_RIGHT) {
                    imageFuseeX += IMAGE_SPEED;
                }
                else if (event.key.keysym.sym == SDLK_UP) {
                    imageFuseeY -= IMAGE_SPEED;
                }
                else if (event.key.keysym.sym == SDLK_DOWN) {
                    imageFuseeY += IMAGE_SPEED;
                }
            }
        }

        // Rotation de l'image
        angle += 0.01; // Augmenter l'angle de rotation

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Effacer le rendu
        SDL_RenderClear(renderer);

        // Affichage de la Terre avec rotation
        SDL_RenderCopyEx(renderer, imageTextureTerre, NULL, &destRect, angle, NULL, SDL_FLIP_NONE);

        // Affichage de la fusee à sa nouvelle position
        SDL_Rect destRect1 = { imageFuseeX, imageFuseeY, imageWidthFusee, imageHeightFusee };
        SDL_RenderCopy(renderer, imageTextureFusee, NULL, &destRect1);

        SDL_RenderPresent(renderer);
    }
    return 0;
}


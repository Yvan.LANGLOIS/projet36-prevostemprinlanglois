#include <SDL2/SDL.h>
#include <stdio.h>

void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
        char const* msg,                                       // message à afficher
        SDL_Window* window,                                    // fenêtre à fermer
        SDL_Renderer* renderer) {                              // renderer à fermer
    char msg_formated[255];                                            
    int l;                                                     
                                        
    if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                         
    l = strlen(msg_formated);                                            
    strcpy(msg_formated + l, " : %s\n");                                     
                                        
    SDL_Log(msg_formated, SDL_GetError());                                   
    }                                                          
                                        
    if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
    }
    if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
    }
                                        
    SDL_Quit();                                                    
                                        
    if (!ok) {                                       // On quitte si cela ne va pas            
    exit(EXIT_FAILURE);                                                  
    }                                                          
}    

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;



    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", 
        SDL_GetError());                // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }

    /*Recuperation de la taille de l'ecran*/

    SDL_DisplayMode dm;

    if (SDL_GetDesktopDisplayMode(0, &dm) != 0){

    SDL_Log("SDL_GetNumDisplayModes failed: %s", SDL_GetError());
    return 1;

    }

    int displayW = dm.w, displayH = dm.h;

    int nb_fenetre = 9;
    int window_W = displayW / nb_fenetre;
    int window_H = displayH / nb_fenetre;
    int i;
    SDL_Window *window = NULL; 


    for (i=1;i<nb_fenetre-1;i++){

        SDL_Window *window = NULL;

        /* Création de la fenêtre*/
        window = SDL_CreateWindow(
        "Fenêtre",                                         
        0 + i*window_W, 0 + i*window_H,                   
        window_W, window_H,                              
        0);

        if (window == NULL) {
        /* L'init de la SDL : OK
            fenêtre 1 :OK
            fenêtre 2 : échec */
                SDL_Log("Error : SDL window creation - %s\n", 
                SDL_GetError());                        // échec de la création de la fenêtre 
                SDL_DestroyWindow(window);             // la première fenétre (qui elle a été créée) doit être détruite
                SDL_Quit();
                exit(EXIT_FAILURE);
        }

        SDL_Renderer *renderer2;

        renderer2 = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
        if (renderer2 == 0) {
            fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
            /* faire ce qu'il faut pour quitter proprement */
        }
        
        /* on prépare/efface le renderer */
        SDL_SetRenderDrawColor(renderer2, 0, 0, 0, 255);
        SDL_Delay(10);
        SDL_RenderClear(renderer2);

        /* afficher le renderer dans la fenetre */
        SDL_RenderPresent(renderer2);
        SDL_Delay(10);
        

    
        /* Création de la fenêtre*/
        window = SDL_CreateWindow(
        "Fenêtre",                                         // codage en utf8, donc accents possibles
        displayW +(-i-1)*window_W, 0 + i*window_H,                   //
        window_W, window_H,                              // 
        0);

        if (window == NULL) {
        /* L'init de la SDL : OK
            fenêtre 1 :OK
            fenêtre 2 : échec */
                SDL_Log("Error : SDL window creation - %s\n", 
                SDL_GetError());                        // échec de la création de la fenêtre 
                SDL_DestroyWindow(window);             // la première fenétre (qui elle a été créée) doit être détruite
                SDL_Quit();
                exit(EXIT_FAILURE);
        }

        SDL_Renderer *renderer;

        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
        if (renderer == 0) {
            fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
            end_sdl(0, "Echec du chargement du renderer", window, renderer);
        }

        /* on prépare/efface le renderer */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        /* afficher le renderer dans la fenetre */
        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    
    }


    SDL_Delay(5000);                           // Pause exprimée  en ms

    /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
    SDL_DestroyWindow(window);               // la fenêtre    

    SDL_Quit();                                // la SDL

    return 0;

    
}
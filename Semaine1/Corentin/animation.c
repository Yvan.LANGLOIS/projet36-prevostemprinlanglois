#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>   // pour rand

#define PI 3.14159

void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
        char const* msg,                                       // message à afficher
        SDL_Window* window,                                    // fenêtre à fermer
        SDL_Renderer* renderer) {                              // renderer à fermer
    char msg_formated[255];                                            
    int l;                                                     
                                        
    if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                         
    l = strlen(msg_formated);                                            
    strcpy(msg_formated + l, " : %s\n");                                     
                                        
    SDL_Log(msg_formated, SDL_GetError());                                   
    }                                                          
                                        
    if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
    }
    if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
    }
                                        
    SDL_Quit();                                                    
                                        
    if (!ok) {                                       // On quitte si cela ne va pas            
    exit(EXIT_FAILURE);                                                  
    }                                                          
}    

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", 
        SDL_GetError());                // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }


    SDL_Window *window = NULL;

    /* Création de la fenêtre*/
    window = SDL_CreateWindow(
    "Pluie d'etoiles filantes",                                         // codage en utf8, donc accents possibles
    100, 100,                               //position
    600, 600,                              // taille
    0);

    if (window == NULL) {
    /* L'init de la SDL : OK
        fenêtre 1 :OK
        fenêtre 2 : échec */
            SDL_Log("Error : SDL window creation - %s\n", 
            SDL_GetError());                        // échec de la création de la fenêtre 

            SDL_Quit();
            exit(EXIT_FAILURE);
    }

    SDL_Renderer * renderer;

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */

    if (renderer == NULL) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        end_sdl(0, "Echec du chargement du renderer", window, renderer);
    }

    //initialisation des etoiles
    int running = 1;
    int nb_etoile = 100;
    int x[nb_etoile]; 
    int y[nb_etoile];


    int red[nb_etoile];
    int green[nb_etoile];
    int blue[nb_etoile];

    int speed[nb_etoile];
    int distance[nb_etoile];

    srand(time(NULL));
    
    for (int k=0; k<nb_etoile; k++)
    {
        red[k]=150 + rand()%100;
        green[k]=150 + rand()%100;
        blue[k]=150 + rand()%100;

        x[k] = -100 + rand()%600;
        y[k] = -50 + rand()%500;

        speed[k] = 1 + rand()%2;
        distance[k] = 1 + rand()%1;
    }


    SDL_Event event;


    while(running)
    {
        if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête
                                                        // de file dans 'event'
            switch(event.type){                       // En fonction de la valeur du type de cet évènement
                case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
                    running = 0;  
                    break ;
                default:
                    break;
            }
        }

                        /* on prépare/efface le renderer */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        for (int l = 0; l<nb_etoile; l++){

            SDL_SetRenderDrawColor(renderer, red[l], green[l], blue[l], 255);                   
            SDL_RenderDrawLine(renderer,                             
                x[l], y[l],                                          // x,y du point de la première extrémité
                x[l] + distance[l]*20, y[l] + distance[l]*30);                                // x,y seconde extrémité
            
            x[l] = x[l] + 2*speed[l];
            y[l] = y[l] + 3*speed[l];

            if (x[l]>605 || y[l]>605)
            {
                x[l] = -100 + rand()%200;
                if (x[l] < -10)
                    y[l] = rand()%500;
                else{
                    y[l] = -50 + rand()%40;
                    x[l] = rand()%550;
                }
            }
        
        }

                    /* afficher le renderer dans la fenetre */
        SDL_RenderPresent(renderer);

        SDL_Delay(20);

    }

    
    /* Normalement, on devrait ici remplir les fenêtres... */
    //SDL_Delay(3000);                           // Pause exprimée  en ms

                                                    
    SDL_DestroyRenderer(renderer);                                
    renderer = NULL;


    /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
    SDL_DestroyWindow(window);               // la fenêtre    

    SDL_Quit();                                // la SDL

    return 0;


}
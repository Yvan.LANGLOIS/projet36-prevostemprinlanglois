#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>   // pour rand
#include <SDL2/SDL_image.h>

int nb_frame = 3;
char frames_Baba[3][20] =  {"BABA/A1.png","BABA/A2.png","BABA/A3.png"};


void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
        char const* msg,                                       // message à afficher
        SDL_Window* window,                                    // fenêtre à fermer
        SDL_Renderer* renderer) {                              // renderer à fermer
    char msg_formated[255];                                            
    int l;                                                     
                                        
    if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                         
    l = strlen(msg_formated);                                            
    strcpy(msg_formated + l, " : %s\n");                                     
                                        
    SDL_Log(msg_formated, SDL_GetError());                                   
    }                                                          
                                        
    if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
    }
    if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
    }
                                        
    SDL_Quit();                                                    
                                        
    if (!ok) {                                       // On quitte si cela ne va pas            
    exit(EXIT_FAILURE);                                                  
    }                                                          
}    

void afficher_fond(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer){
    SDL_Rect 
    source = {0},                         // Rectangle définissant la zone de la texture à récupérer
    window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
    destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_GetWindowSize(
    window, &window_dimensions.w,
    &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
    SDL_QueryTexture(my_texture, NULL, NULL,
            &source.w, &source.h);             // Récupération des dimensions de l'image

    float zoom = 1;                        // Facteur de zoom à appliquer    
       destination.w = source.w * zoom;         // La destination est un zoom de la source
       destination.h = source.h * zoom;         // La destination est un zoom de la source
       destination.x =
     (window_dimensions.w - destination.w) /2;     // La destination est au milieu de la largeur de la fenêtre
       destination.y =
       (window_dimensions.h - destination.h) / 2;  // La destination est au milieu de la hauteur de la fenêtre


    /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */
    
    SDL_RenderCopy(renderer, my_texture,
        &source,
        &destination);                 // Création de l'élément à afficher
    SDL_RenderPresent(renderer);                  // Affichage
}

void afficher_sprite(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer, int pos_x, int pos_y, SDL_RendererFlip flip){
    SDL_Rect 
    source = {0},                         // Rectangle définissant la zone de la texture à récupérer
    window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
    destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_QueryTexture(my_texture, NULL, NULL,
            &source.w, &source.h);             // Récupération des dimensions de l'image

    destination.w = 55;
    destination.h = 55;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

    destination.x = pos_x;
    destination.y = pos_y;

    SDL_RenderCopyEx(renderer, my_texture,
    &source,
    &destination, 0, NULL, flip);                 // Création de l'élément à afficher
    SDL_RenderPresent(renderer); 
    
}

void afficher_rectangle(SDL_Renderer *renderer) 
{

   SDL_Rect rect;

   /* on prépare/efface le renderer */
   /*
   SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
   SDL_RenderClear(renderer);
    */

   /* dessiner en blanc */
   SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
   rect.x = 219;
   rect.y = 90;
   rect.w = rect.h = 15;
   SDL_RenderFillRect(renderer, &rect );

   /* afficher le renderer dans la fenetre */
   SDL_RenderPresent(renderer);
}



int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", 
        SDL_GetError());                // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }

    SDL_Window *window = NULL;

    /* Création de la fenêtre*/
    window = SDL_CreateWindow(
    "Fenêtre",                                         // codage en utf8, donc accents possibles
    100, 100,                               //position
    594, 196,                              // taille
    0);

    if (window == NULL) {
    /* L'init de la SDL : OK*/
 
            SDL_Log("Error : SDL window creation - %s\n", 
            SDL_GetError());                        // échec de la création de la fenêtre 

            SDL_Quit();
            exit(EXIT_FAILURE);
    }

    /* Création du renderer */

    SDL_Renderer * renderer;

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */

    if (renderer == NULL) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        end_sdl(0, "Echec du chargement du renderer", window, renderer);
    }

    /* Texture du fond */

    SDL_Texture *fond; 
    fond = IMG_LoadTexture(renderer,"../level00.jpg");
    if (fond == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

    /* Texture du Sprite */

    
    SDL_Texture * Baba[nb_frame];

    for (int i = 0; i<nb_frame; i++)
    {
        Baba[i] = IMG_LoadTexture(renderer, frames_Baba[i]);
        if (Baba[i] == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    }


    /* Boucle d'évènement */
    int speed =  18;
    int x = 197, y=68;
    char Or = 'd';
    SDL_RendererFlip flip1 = SDL_FLIP_NONE;


    int compteur = 0;
    int num_frame = 0;
    int running = 1;
    SDL_Event event;

    while(running)
    {
        if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête
                                                    // de file dans 'event'
            switch(event.type){                     // En fonction de la valeur du type de cet évènement
                case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
                    running = 0;  
                    break ;
                default:
                    break;
                
                case SDL_KEYDOWN:                             // Le type de event est : une touche appuyée
                              // comme la valeur du type est SDL_Keydown, dans la partie 'union' de
                              // l'event, plusieurs champs deviennent pertinents   
                    switch (event.key.keysym.sym) {             // la touche appuyée est ...
                        case SDLK_d:
                            x = x + speed;
                            if (Or == 'g'){
                                flip1 = SDL_FLIP_NONE;
                                Or = 'd';
                            }
                            break;
                        case SDLK_q:
                            x = x - speed;
                            if (Or == 'd'){
                                flip1 = SDL_FLIP_HORIZONTAL;
                                Or = 'g';
                            }
                            break;
                        case SDLK_z:
                            y = y - speed;
                            break;
                        case SDLK_s:
                            y = y + speed;
                            break;

                        case SDLK_ESCAPE:                           // 'ESCAPE'                                
                            running = 0;                           // 'escape'  d'autres façons de quitter le programme                     
                            break;
                        default:                                    // Une touche appuyée qu'on ne traite pas
                            break;
                        }
            }
        }

        //Un monde en tore
        if (x>=575)
            x=-19;
        if (x<=-37)
            x=557;
        if (y>=176)
            y=-22;
        if (y<=-40)
            y=158;

        SDL_RenderClear(renderer);

        afficher_fond(fond, window, renderer);
        afficher_rectangle(renderer);
        afficher_sprite(Baba[num_frame], window, renderer, x , y, flip1);
        compteur = compteur+1;

        if (compteur == 10){
            num_frame = num_frame +1;
            compteur = 0;
        }
        if (num_frame > 2)
            num_frame = 0; 

        SDL_Delay(20);
    }

    //SDL_RenderClear(renderer);

    /* on referme le renderer*/
    SDL_DestroyRenderer(renderer);                                
    renderer = NULL;

    /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
    SDL_DestroyWindow(window);               // la fenêtre    

    SDL_Quit();                                // la SDL

    return 0;


}


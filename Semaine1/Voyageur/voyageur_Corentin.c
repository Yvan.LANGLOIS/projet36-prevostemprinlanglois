#include "voyageur_Corentin.h"

void init_list_voisins(int *list, int taille) // Principe : liste du sommet k, list [i] =
                                              //  -1 : i n'est pas voisin (de k), 0 : i voisin deja visité, 1 : i voisin pas visité
{
    int i;
    for (i = 0; i<taille; i++)
        list[i] = -1;
}

void Tous_les_voisins(int A, int N_sommet, int **MATRICE, int *list_voisin)
{
    int i;

    for (i = 0; i < N_sommet; i++)
    {
        // printf("%d\n",i);
        if (MATRICE[A][i] != 0)
            list_voisin[i] = 1;
    }
}

void Voisins_non_visites(int *deja_visite, int *list_voisin, int N_sommet, int nb_deja_vis)
{
    int i, j;

    for (i = 0; i < N_sommet; i++)
    {
        if (list_voisin[i] == 1)
        {
            for (j = 0; j < nb_deja_vis; j++)
            {
                if (deja_visite[j] == i)
                    list_voisin[i] = 0;
            }
        }
    }
}

void liste_voisin_explicite(int A, int N_sommet, int **MATRICE, int *vois_explicite, int *nb_vois)
{
    int i;
    int k = 0;

    for (i = 0; i < N_sommet; i++)
    {
        if (MATRICE[A][i] != 0)
        {
            vois_explicite[k] = i;
            k = k + 1;
            *nb_vois = *nb_vois + 1;
        }
    }
}

int Meilleur_voisin(int A, int N_sommet, int **MATRICE, int *deja_visite, int nb_deja_vu)
{

    int meilleur = -1;
    int score = 1000000; // valeur qu'on ne pourra deplacer, mettre Define?

    int * list_voisin_A = (int*)malloc((N_sommet+1)*sizeof(int));
    init_list_voisins(list_voisin_A, N_sommet);
    Tous_les_voisins(A, N_sommet, MATRICE, list_voisin_A);
    Voisins_non_visites(deja_visite, list_voisin_A, N_sommet, nb_deja_vu);

    for (int i = 0; i < N_sommet; i++)
    {
        if (list_voisin_A[i] == 1)
        {
            if (MATRICE[A][i] <= score)
            {
                meilleur = i;
                score = MATRICE[A][i];
            }
        }
    }

    
    if (meilleur == -1) //on entre jamais dans ce cas là avec une matrice complète
    {
        int nb_de_voisin = 0;
        int * voisin_explicite = (int*)malloc((N_sommet)*sizeof(int));
        init_list_voisins(voisin_explicite, N_sommet);
        liste_voisin_explicite(A, N_sommet, MATRICE, voisin_explicite, &nb_de_voisin);
        //srand(time(NULL));
        meilleur = voisin_explicite[rand() % nb_de_voisin];
        free(voisin_explicite);
    }

    free(list_voisin_A);
    return meilleur;
}

int Recherche_sommet(int A, int *list, int taille)
{
    int presence = 0;
    for (int i = 0; i < taille; i++)
    {
        if (list[i] == A)
        {
            presence = 1;
            break;
        }
    }

    return presence;
}

int Chemin_avec_meilleur_voisin(int **MATRICE, int N_sommet, int *chemin_final, int *nb_sommet_cumule, int *distance_cumule, int point_depart, int limit_ite) // int * point de depart?
                                                                                                                                                              /// renvoie 1 si il a fini la boucle (code err)
{
    int code = 1;
    int ite = 0;

    int sommet_courant = point_depart;

    // Creation de la liste voisin deja visite + le nombre
    int nb_deja_vu = 1;
    int *deja_visit;
    deja_visit = (int *)malloc(N_sommet * sizeof(int)); // peut-on faire mieux? (dynamique)
    init_list_voisins(deja_visit, N_sommet);            // nécéssaire?
    deja_visit[0] = point_depart;

    while (((nb_deja_vu < N_sommet) || (sommet_courant != point_depart)) && (ite < limit_ite))
    {
        // Recherche meilleur voisin
        int suivant = Meilleur_voisin(sommet_courant, N_sommet, MATRICE, deja_visit, nb_deja_vu);

        *distance_cumule = *distance_cumule + MATRICE[sommet_courant][suivant];
        // printf("%d\n",MATRICE[sommet_courant][suivant]);
        *nb_sommet_cumule = *nb_sommet_cumule + 1;
        chemin_final[ite] = sommet_courant;

        if (nb_deja_vu >= N_sommet) // Cas tous les sommets sont parcourus
        {
            deja_visit[0] = -1; // retourner à la case départ est prioritaire
        }
        else
        {
            // Ajout du voisin à deja_visit
            if (Recherche_sommet(suivant, deja_visit, N_sommet) == 0) // Si le sommet est rencontré pour la premiere fois;
            {
                deja_visit[nb_deja_vu] = suivant;
                nb_deja_vu++;
            }
            if (nb_deja_vu >= N_sommet) // Cas tous les sommets sont parcourus
            {
                deja_visit[0] = -1; // retourner à la case départ est prioritaire
            }
        }

        if (suivant >= N_sommet)
        {
            printf("curr: %d\n", sommet_courant);
            printf("suiv: %d\n", suivant);
        }

        sommet_courant = suivant;
        ite++;
    }

    chemin_final[ite] = point_depart;
    // liberation
    free(deja_visit);

    if (ite >= limit_ite)
        code = 0;

    return code;
}

chemin_t initialisation_chemin(int taille)
{
    chemin_t ch;
    int * liste = (int*)malloc(taille*sizeof(int));
    init_list_voisins(liste,taille);
    ch.nb_sommet = 0;
    ch.distance = 0; 
    ch.suite = liste;

    return ch;
}


void copier_chemin(chemin_t * source, chemin_t * dest)
{
    //chemin_t ch = initialisation_chemin(taille);
    for (int i =0; i<source->nb_sommet;i++)
    {
        dest->suite[i] = source->suite[i];
    }
    dest->distance = source->distance;
    dest->nb_sommet = source->nb_sommet;

}



void Ajouter_sommet(int A, int num_chemin, chemin_t * ch, int ** MATRICE)
{
    int z = ch[num_chemin].nb_sommet;
    
    ch[num_chemin].suite[z] = A;
    ch[num_chemin].nb_sommet = z + 1;

    //printf("distance avant: %d\n",ch[num_chemin].distance);
    ch[num_chemin].distance = ch[num_chemin].distance + MATRICE[A][ch[num_chemin].suite[z-1]];
    //printf("distance entre %d et %d =",A,ch[num_chemin].suite[z-1]);

    //printf(" %d\n",MATRICE[A][ch[num_chemin].suite[z-1]]);
    //printf("distance totale: %d\n",ch[num_chemin].distance);
}

void creer_chemin_par_voisin(int ** MATRICE, chemin_t * liste_chemin, chemin_t * chemin_suivant, int num_chemin, int taille, int * nb_chemin)
{
    int * voisin_expli = (int*)malloc(sizeof(int)*taille);
    int nb_voisin = 0;
    int sommet = liste_chemin[num_chemin].suite[liste_chemin[num_chemin].nb_sommet-1];
    //printf("sommet concerné = %d\n",sommet);

    init_list_voisins(voisin_expli, taille);
    liste_voisin_explicite(sommet,taille,MATRICE,voisin_expli,&nb_voisin);

    for (int k=0; k<nb_voisin; k++)
    {
        {
            copier_chemin(&(liste_chemin[num_chemin]), &(chemin_suivant[*nb_chemin + k]));
            Ajouter_sommet(voisin_expli[k],*nb_chemin + k,chemin_suivant,MATRICE);

            //afficher_chemin(chemin_suivant[*nb_chemin + k]);
        }
    }
    
    *nb_chemin = *nb_chemin + nb_voisin;

    free(voisin_expli);
}

void liberer_chemin(chemin_t * ch, int taille)
{
    for(int i=0;i<taille;i++)
    {
        free(ch[i].suite);
    }
    free(ch);
}


void Plus_court_chemin(int ** MATRICE, int N_sommet, int A, int B, chemin_t * chemin_le_plus_court, int * code_err)
{
    
    //contient les proposition de chemin
    
    chemin_t * liste_chemin = (chemin_t*)malloc(sizeof(chemin_t)*N_sommet);
    for (int i =0; i<N_sommet;i++)
        liste_chemin[i] = initialisation_chemin(N_sommet);
    

    int compte_chemin = 1; 
    int code = -1; 

    //liste_chemin[0] = initialisation_chemin(N_sommet);
    liste_chemin[0].distance = 0;
    liste_chemin[0].nb_sommet = 1;
    liste_chemin[0].suite[0] = A;

    //liste de sommet deja visite
    int deja_visite[N_sommet];
    init_list_voisins(deja_visite, N_sommet);
    deja_visite[0] = A;

    while(code == -1)
    {
        int max_nouv_chemin = 10000;
        chemin_t * chemin_suivant = (chemin_t*)malloc(sizeof(chemin_t)*max_nouv_chemin);

        int compte_voisin = 0;
    
        for (int i =0; i<max_nouv_chemin;i++)
        {
            chemin_suivant[i] = initialisation_chemin(N_sommet);
        }

        /*Creation d'une liste contenant toutes les listes avec les voisins suivant*/
        for (int k=0;k<compte_chemin;k++)
        {
                creer_chemin_par_voisin(MATRICE, liste_chemin, chemin_suivant, k, N_sommet, &compte_voisin);
        }

        int dist_min = 100000000;
        int chemin_retenue = -1;

        for (int k=0;k<compte_voisin;k++)
        {

            int dernier_voisin = chemin_suivant[k].suite[chemin_suivant[k].nb_sommet-1];
            //printf("voisin recherché = %d\n",dernier_voisin);
            if (chemin_suivant[k].distance < dist_min && Recherche_sommet(dernier_voisin,deja_visite,N_sommet)==0)
            {
                dist_min = chemin_suivant[k].distance;
                chemin_retenue = k;
            }
        }

        if(chemin_retenue!=-1)
        {
            copier_chemin(&(chemin_suivant[chemin_retenue]), &(liste_chemin[compte_chemin]));
            //printf("chemin choisi pour être rajouté:\n");
            //afficher_chemin(liste_chemin[compte_chemin]);

            int K = liste_chemin[compte_chemin].suite[liste_chemin[compte_chemin].nb_sommet-1];
            deja_visite[compte_chemin] = K;
            //printf("                                    deja vu %d\n",K);
            
            compte_chemin++;

            //printf("nombre de chemin = %d\n", compte_chemin);

            liberer_chemin(chemin_suivant,compte_voisin);

            for (int k=0;k<compte_chemin;k++)
            {
                int G = liste_chemin[k].suite[liste_chemin[k].nb_sommet-1];
                //printf("dernier sommet d'un chemin: %d\n",G);
                if(G == B)
                {
                    code = k;
                }
            }
            //printf("code = %d\n\n\n", code);
        }
        else
        {
            *code_err = 1;
            printf("L'arrivée et le départ ne sont pas joignables\n");
            break;
        }

    }


    if (*code_err != 1)
    {
        *chemin_le_plus_court = initialisation_chemin(N_sommet);
        
        copier_chemin(&(liste_chemin[code]),chemin_le_plus_court);
    }

    liberer_chemin(liste_chemin, compte_chemin);

}

void afficher_chemin(chemin_t PCC)
{
   	printf("nb sommet: %d et distance %d\n chemin = ", PCC.nb_sommet, PCC.distance);
    int i =0;
    for (i=0;i<PCC.nb_sommet;i++)
		printf("%d ",PCC.suite[i]);
        
	
	printf("\n\n"); 
}

int **  Matrice_complete(int ** MATRICE, int N_sommet)
{
    int ** M =(int**)malloc(N_sommet*sizeof(int*));
    chemin_t PCC;
    int code_ERR = 0;

    for (int i =0; i<N_sommet;i++)
    {
        M[i] = (int*)malloc(N_sommet*sizeof(int));
        for(int j = 0; j<=i ; j++)
        {
            if (j!=i && MATRICE[i][j]==0)
            {
                PCC = initialisation_chemin(N_sommet);
                Plus_court_chemin(MATRICE, N_sommet,i,j, &PCC, &code_ERR);
                M[i][j] = PCC.distance;
                M[j][i] = M[i][j];
            }
            else
            {
                M[i][j] = MATRICE[i][j];
                M[j][i] = MATRICE[j][i];
            }
        }
    }
    return M;
}

void LibererMatrice (int ** MATRICE, int taille)
{
    for (int i = 0; i<taille; i++)
    {
        free(MATRICE[i]);
    }
    free(MATRICE);
}

void randomizer (int taille, int * liste_random, int depart)
{
    
    int liste_sommet[taille];

    for (int i = 0; i<taille; i++)
    {
        liste_sommet[i] = i;
    }
    decale_gauche(liste_sommet, taille, depart);


    for (int i = 1; i<taille; i++)
    {
        int c = rand()%(taille-i);
        liste_random[i] = liste_sommet[c];

        decale_gauche(liste_sommet, taille, c);
    }
    liste_random[taille] = depart;
    liste_random[0] = depart;
    
}

void decale_gauche(int * list, int taille, int indice)
{
    for (int k = indice;k<taille-1;k++)
    {
        list[k] = list[k+1];
    }
}

int distance_from_chemin(int* chemin,int taille,int ** matrice_comp)
{
    int dist = 0;
    for (int k = 0;k<taille;k++)
    {
        dist = dist + matrice_comp[chemin[k]][chemin[k+1]];
    }

    return dist;
}

void minor_swap(int * chemin_a_changer, int taille)
{
    int indice1 = 1+rand()%(taille-1);
    int indice2 = indice1;
    
    while(indice1 == indice2 && taille > 3)
    {
        indice2 = 1+rand()%(taille-1);
    }

    int tmp = chemin_a_changer[indice1];
    chemin_a_changer[indice1] = chemin_a_changer[indice2];
    chemin_a_changer[indice2] = tmp;
}


/***********************************LES FOURMIES***********************************/


void algo_pour_trouver_bon_chemin_fourmie(int taille, int **matriceConnexion, int **matriceProba, int *parcours_fourmie, int nb_iteration)
{/*
    Bool sortie = true;

    while (sortie)
    {
        printf("coucou\n");
        algo_fourmies(taille, matriceConnexion, matriceProba, parcours_fourmie);
        int length = taille_liste(parcours_fourmie, 5 * taille * taille);
        int nb_100p = 0; //nombre de 100% où est passé la fourmie
        for (int i = 0; i < length-1; i ++)
        {
            if ((matriceProba[parcours_fourmie[i + 1]][parcours_fourmie[i]] == 100) && (matriceProba[parcours_fourmie[i + 1]][parcours_fourmie[i]] == 100))
                nb_100p += 1;
        }
        compteur += 1;
        if (length == nb_100p)
            sortie = false;
    }*/
    for(int i = 0 ; i < nb_iteration ; i++)
    {
        algo_fourmies(taille, matriceConnexion, matriceProba, parcours_fourmie);
    	afficher_Matrice(taille, matriceProba);
    }
}


//l'algo renvoie le nombre de sommet minimum avant de tous les parcourrir

void algo_fourmies(int taille, int **matriceConnexion, int **matriceProba, int *parcours_fourmie1)
{
    // 3 fourmies parcours le graph la premiere arrivee fait +6 en pheromones la 2eme +2 et la derniere -2, il peut y avoir des egalites et dans ce cas on prend le plus avantageux

    int pheromones_ajoutes_1er = 2;//6
    int pheromones_ajoutes_2eme = 0;//2
    int pheromones_ajoutes_3eme = -1;//-2

    //initialisation des listes
    //int parcours_fourmie1[5 * taille * taille];
    int parcours_fourmie2[5 * taille * taille];
    int parcours_fourmie3[5 * taille * taille];
    //init_list_voisins(parcours_fourmie1, 5 * taille * taille);
    init_list_voisins(parcours_fourmie2, 5 * taille * taille);
    init_list_voisins(parcours_fourmie3, 5 * taille * taille);
    //on cherche le parcours des 3 fourmies
    algo_parcours_d_une_fourmie(taille, matriceConnexion, matriceProba, parcours_fourmie1);
    algo_parcours_d_une_fourmie(taille, matriceConnexion, matriceProba, parcours_fourmie2);
    algo_parcours_d_une_fourmie(taille, matriceConnexion, matriceProba, parcours_fourmie3);
    // on regarde la longueur du chemin parcouru par les fourmies
    int taille_l1 = taille_liste(parcours_fourmie1, 5 * taille * taille);
    int taille_l2 = taille_liste(parcours_fourmie2, 5 * taille * taille);
    int taille_l3 = taille_liste(parcours_fourmie3, 5 * taille * taille);
    // on trie les listes de la taille la plus petite à la plus grande
    int list_tri1[] = {taille_l1, taille_l2, taille_l3};

    triInsertion(list_tri1, 3);

    //on remet dans l'ordre croissant
    int list_tri[] = {list_tri1[2], list_tri1[1], list_tri1[0]};

    // ajout des pheromones dans la matrice de proba
    if ((list_tri[0] == list_tri[1]) && (list_tri[1] == list_tri[2])) // les 3 fourmies ont prit une même distance de chemin
    {
        ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_1er, taille);
        ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_1er, taille);
        ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_1er, taille);
        return;
    }
    //gestion pour egalitee entre 2 fourmies
    if (list_tri[0] == list_tri[1])
    {
        if(taille_l1 == taille_l2)
        {
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_1er, taille);
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_1er, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_2eme, taille);
            return;
        }
        if(taille_l1 == taille_l3)
        {
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_1er, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_1er, taille);
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_2eme, taille);
            return;
        }
        if(taille_l2 == taille_l3)
        {
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_1er, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_1er, taille);
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_2eme, taille);
            return;
        }
    }
    if (list_tri[1] == list_tri[2])
    {
        if(taille_l1 == taille_l2)
        {
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_1er, taille);
            return;
        }
        if(taille_l1 == taille_l3)
        {
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_1er, taille);
            return;
        }
        if(taille_l2 == taille_l3)
        {
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_1er, taille);
            return;
        }
    }

    // cas trois longeurs de parcours differentes

    if (list_tri[0] == taille_l1)
    {
        ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_1er, taille);
        if(list_tri[1] == taille_l2)
        {
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_3eme, taille);
            return;
        }
        else
        {
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_3eme, taille);
            return;
        }
    }
    if (list_tri[0] == taille_l2)
    {
        ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_1er, taille);
        if(list_tri[1] == taille_l1)
        {
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_3eme, taille);
            return;
        }
        else
        {
            ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_3eme, taille);
            return;
        }
    }
    if (list_tri[0] == taille_l3)
    {
        ajout_pheromones(matriceProba, parcours_fourmie3, pheromones_ajoutes_1er, taille);
        if(list_tri[1] == taille_l2)
        {
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_3eme, taille);
            return;
        }
        else
        {
            ajout_pheromones(matriceProba, parcours_fourmie1, pheromones_ajoutes_2eme, taille);
            ajout_pheromones(matriceProba, parcours_fourmie2, pheromones_ajoutes_3eme, taille);
            return;
        }
    }
}
//pour une liste ayant au moins un -1
int taille_liste(int *liste, int taille)// calcul de la taille d'une liste rapide
{
    int somme = 0;
    for (int i = 0; i < taille; i ++)
    {
        if (liste[i] != -1)
            somme += 1;
    }
    return somme;
}
// parcours_fourmie doit finir par un -1
void ajout_pheromones(int** matriceProba, int *parcours_fourmie, int pheromones_ajoute, int taille)
{
    int n = taille_liste(parcours_fourmie, 5 * taille * taille);

    //boucle pour ajout d'un cote de la matrice
    for (int i = 0; i < n-1 ; i++) // n - 1 car on prend l'elt courant et son suivant à chaque itération
    {
        int valeur1 = matriceProba[parcours_fourmie[i]] [parcours_fourmie[i + 1]] + pheromones_ajoute;

        if (valeur1 >= 100)
        {
            matriceProba[parcours_fourmie[i]][parcours_fourmie[i + 1]] = 100;
        }
        if (valeur1 <= 0)
        {
            matriceProba[parcours_fourmie[i]][parcours_fourmie[i + 1]] = 0;
        }
        if ((valeur1 > 0) && (valeur1 < 100))
        {
            matriceProba[parcours_fourmie[i]][parcours_fourmie[i + 1]] = valeur1;
        }
    }
    //boucle pour ajout de l'autre cote
    for (int i = 0; i < n-1 ; i++) // n - 1 car on prend l'elt courant et son suivant à chaque itération
    {
        int valeur1 = matriceProba[parcours_fourmie[i + 1]][parcours_fourmie[i]] + pheromones_ajoute;

        if (valeur1 >= 100)
        {
            matriceProba[parcours_fourmie[i + 1]][parcours_fourmie[i]] = 100;
        }
        if (valeur1 <= 0)
        {
            matriceProba[parcours_fourmie[i + 1]][parcours_fourmie[i]] = 0;
        }
        if ((valeur1 > 0) && (valeur1 < 100))
        {
            matriceProba[parcours_fourmie[i + 1]][parcours_fourmie[i]] = valeur1;
        }
    }
}

//retourne un tableau avec les sommets parcours dans l'ordre
void algo_parcours_d_une_fourmie(int taille, int **matriceConnexion, int **matriceProba, int *tab_des_sommets_parcourru)
{
    
    tab_des_sommets_parcourru[0] = 0;
    int indice_tab_des_sommets_parcourru = 1;

    int sommet_actuel = 0;

    int list_voisin_visite_differents[taille];

    init_list_voisins(list_voisin_visite_differents, taille);

    list_voisin_visite_differents[0] = 0;
    int indice_list_voisin_visite_differents = 1;

    
    while (indice_tab_des_sommets_parcourru < taille + 10)
    //while (indice_list_voisin_visite_differents < taille )
    {
        int list_voisin[taille];
        int list_voisin_poids[taille];
        init_list_voisins(list_voisin, taille);
        init_list_voisins(list_voisin_poids, taille);

        Tous_les_voisins(tab_des_sommets_parcourru[indice_tab_des_sommets_parcourru -1], taille, matriceConnexion, list_voisin);
        
        //int nb_voisin = taille_liste(list_voisin, taille);
        //printf("liste des voisins :");
        //affiche_liste(list_voisin, taille);

        for (int i = 0; i < taille; i ++) // creation liste avec les poids des voisins concernes
        {
            if(list_voisin[i] == 1)
            {
                list_voisin_poids[i] = matriceProba[i][sommet_actuel] + matriceProba[sommet_actuel][i];
            }
        }
        //srand(time(NULL));
        int nb_aleat = rand()%101;
        //printf("voisin poids :");
        //affiche_liste(list_voisin_poids, taille);
        //printf("nb aleat : %d\n",nb_aleat);

        sommet_actuel = donne_le_sommet_suiv_aleat(nb_aleat, list_voisin_poids, taille);
        //printf("sommet actuel :%d\n",sommet_actuel);

        //ajout du sommet dans la liste de parcours de la fourmie
        //printf("indice sommet parcourru : %d\n",indice_tab_des_sommets_parcourru);
        tab_des_sommets_parcourru[indice_tab_des_sommets_parcourru] = sommet_actuel;
        indice_tab_des_sommets_parcourru += 1;

        // on ajoute le sommet à list_voisin_visite_differents si il n'est pas dedans
        int code = 0; // il est pas dedans

        for (int i = 0; i < indice_list_voisin_visite_differents ; i++)
        {
            if(list_voisin_visite_differents[i] == sommet_actuel)
                code = 1;
        }

        if (code == 0)
        {
            list_voisin_visite_differents[indice_list_voisin_visite_differents] = sommet_actuel;
            indice_list_voisin_visite_differents += 1;
        }
        //printf("long sommets parcourus : %d\n", indice_tab_des_sommets_parcourru);
        //printf("indice vvd %d\n", indice_list_voisin_visite_differents);
        //printf("affichage des sommets diff :");
        //affiche_liste(list_voisin_visite_differents, taille);
    }
    //printf("je suis sorti de la 1ere boucle");
    // il faut maintenant revenir à 0, le sommet initial
    int sommet_de_retour = tab_des_sommets_parcourru[indice_tab_des_sommets_parcourru - 1];
    while (sommet_de_retour != 0)
    {
        int list_voisin[taille];
        int list_voisin_poids[taille];
        init_list_voisins(list_voisin, taille);
        init_list_voisins(list_voisin_poids, taille);

        Tous_les_voisins(tab_des_sommets_parcourru[indice_tab_des_sommets_parcourru - 1], taille, matriceConnexion, list_voisin);
        
        int nb_voisin = taille_liste(list_voisin, taille);

        for (int i = 0; i < nb_voisin; i ++) // creation liste avec les poids des voisins concernes
        {
            if(list_voisin[i] == 1)
            {
                list_voisin_poids[i] = matriceProba[i][sommet_de_retour] + matriceProba[sommet_de_retour][i];
            }
        }

        int nb_aleat = rand()%101;

        sommet_de_retour = donne_le_sommet_suiv_aleat(nb_aleat, list_voisin_poids, taille);

        tab_des_sommets_parcourru[indice_tab_des_sommets_parcourru] = sommet_de_retour;
        indice_tab_des_sommets_parcourru += 1;
    }
    return;
}

//renvoie le sommet choisi par la fourmie par exemple nb_aleat = 26 et list_voisin_poids = [33, -1, 25, 50] la fourmie va en 0 car 26 > 25 et 26 < 33
int donne_le_sommet_suiv_aleat(int nb_aleat, int *list_voisin_poids, int taille)
{//si pb remplacer les tailles pas le n en //
    //srand(time(NULL));
    int copie_list_voisin_poids[taille];
    init_list_voisins(copie_list_voisin_poids, taille);
    //int n = taille_liste(list_voisin_poids, taille);

    for (int i  = 0; i < taille; i++)// on retire les proba nulle
    {
        copie_list_voisin_poids[i] = list_voisin_poids[i];
        if(list_voisin_poids[i] == 0)
        {
            copie_list_voisin_poids[i] = -1;
        }
    }

    triInsertion(copie_list_voisin_poids, taille);

    //on prend en compte le cas où on a plusieurs probas à 100%
    if(copie_list_voisin_poids[0] >= 100) // >= à 100 car sommet dans les 2 sens 1->0 et 0->1
    {
        int nb_de_100 = 0;
        while (copie_list_voisin_poids[nb_de_100] >= 100)
            nb_de_100 +=1;
        int nb_aleat1 = rand()%(nb_de_100); // on prend le nb_aleat ieme 100 dans list_voisin_poids

        nb_aleat1 += 1; //car il faut au moins un 100 et le dernier doit aussi etre pris en compte
        int indice_du_sommet = 0;
        int indice_de_parcours = 0;
        while ((indice_du_sommet < nb_aleat1)  && (indice_de_parcours < taille))
        {
            if (list_voisin_poids[indice_de_parcours] >= 100)
                indice_du_sommet += 1;
            indice_de_parcours += 1;
        }
        return indice_de_parcours - 1;// car on veut la valeur avant
    }

    //on prend en compte les autres cas

    int indice = 0; // on cherche le sommet dans la liste triee


    while((indice < taille) && (copie_list_voisin_poids[indice] >= nb_aleat))
        indice += 1;

    if (copie_list_voisin_poids[indice] == -1)//on prend en compte le cas où copie_list_voisin_poids[indice] sera à -1
        indice -= 1;

    int nb_meme_valeur = 0; // il faut faire attention à ne pas toujours prendre la 1ere valeur sinon on tourne en boucle
    for (int i = 0; i < taille; i++)
    {
        if(copie_list_voisin_poids[indice] == list_voisin_poids[i])
            nb_meme_valeur +=1;
    }

    int nb_aleat1 = rand()%nb_meme_valeur; // on prend le nb_aleat ieme 100 dans list_voisin_poids
    nb_aleat1 += 1; //car il faut au moins un 100 et le dernier doit aussi etre pris en compte

    int indice_du_sommet = 0;
    int indice_de_parcours = 0;

    while ((indice_du_sommet < nb_aleat1)  && (indice_de_parcours < taille))
    {
        if (list_voisin_poids[indice_de_parcours] == copie_list_voisin_poids[indice])
            indice_du_sommet += 1;
        indice_de_parcours += 1;
    }
    return indice_de_parcours - 1;


    // au cas ou, on retourne la premiere valeur differente de -1

    int indice_au_cas_ou = 0;
    while (list_voisin_poids[indice_au_cas_ou] == -1)
        indice_au_cas_ou += 1;
    return indice_au_cas_ou;
}

// algo de trifusion du plus grand au plus petit:

// Fusionne deux sous-tableaux triés en un seul tableau trié
void fusion(int tableau[], int debut, int milieu, int fin) 
{
    int taille1 = milieu - debut + 1;
    int taille2 = fin - milieu;

    // Création de tableaux temporaires
    int tableauGauche[taille1], tableauDroit[taille2];

    // Copie des données dans les tableaux temporaires
    for (int i = 0; i < taille1; i++)
        tableauGauche[i] = tableau[debut + i];
    for (int j = 0; j < taille2; j++)
        tableauDroit[j] = tableau[milieu + 1 + j];

    // Fusion des tableaux temporaires dans le tableau final
    int i = 0;  // Index initial du sous-tableau gauche
    int j = 0;  // Index initial du sous-tableau droit
    int k = debut;  // Index initial du tableau fusionné

    while (i < taille1 && j < taille2) {
        if (tableauGauche[i] >= tableauDroit[j]) {
            tableau[k] = tableauGauche[i];
            i++;
        } else {
            tableau[k] = tableauDroit[j];
            j++;
        }
        k++;
    }

    // Copie des éléments restants du sous-tableau gauche
    while (i < taille1) {
        tableau[k] = tableauGauche[i];
        i++;
        k++;
    }

    // Copie des éléments restants du sous-tableau droit
    while (j < taille2) {
        tableau[k] = tableauDroit[j];
        j++;
        k++;
    }
}


// Fonction de tri fusion récursif
void triFusion(int tableau[], int debut, int fin) 
{
    if (debut < fin) {
        int milieu = debut + (fin - debut) / 2;

        // Tri récursif des deux moitiés
        triFusion(tableau, debut, milieu);
        triFusion(tableau, milieu + 1, fin);

        // Fusion des deux moitiés triées
        fusion(tableau, debut, milieu, fin);
    }
}

void affiche_liste(int *liste, int taille)
{
    for (int i = 0; i < taille ; i++)
    {
        printf("%d\t",liste[i]);
    }
    printf("\n");
}

void triInsertion(int tableau[], int taille) 
{
    int i, j, element;
    
    for (i = 1; i < taille; i++) {
        element = tableau[i];
        j = i - 1;

        // Déplacer les éléments du tableau qui sont plus petits que l'élément
        // vers la droite de leur position actuelle
        while (j >= 0 && tableau[j] < element) {
            tableau[j + 1] = tableau[j];
            j--;
        }

        tableau[j + 1] = element;
    }
}

//Optimisation avec méthode
void recuit_simule(int ** MATRICE, int taille, int depart, chemin_t * chemin_final)
{
    int max_ite = 10000000;
    float p = 0.1;

    int chemin_courant[taille +1];

    randomizer(taille, chemin_courant,depart);
    
    int ** matrice_complete = Matrice_complete(MATRICE,taille);

    int distance_min = 1000000;

    for(int i = 0;i<max_ite;i++)
    {
        int d = distance_from_chemin(chemin_courant, taille, matrice_complete);
    

        if (d < distance_min || rand() < p)
        {
            chemin_final ->distance = d;
            for(int j = 0;j<taille+1;j++)
            {
                chemin_final->suite[j] = chemin_courant[j];
            }
        }

        minor_swap(chemin_courant, taille);
        p = p*0.995;
        //printf("proba = %f\n",p);
    }

    chemin_final->nb_sommet = taille+1;

    LibererMatrice(matrice_complete, taille);
}

void methode_gloutonne(int ** MATRICE, int taille, int depart, chemin_t * chemin_final)
{
    int ** matrice_complete = Matrice_complete(MATRICE,taille);

    int nb_sommet_cumule = 1;
    int dist = 0;
    int limit = taille * 5;

    Chemin_avec_meilleur_voisin(matrice_complete, taille, chemin_final->suite, &nb_sommet_cumule, &dist, depart, limit);

    chemin_final->distance = dist;
    chemin_final->nb_sommet = taille +1;
    chemin_final->suite[taille] = depart;
    LibererMatrice(matrice_complete, taille);
}

void recuit_simule_sur_methode_gloutonne(int ** MATRICE, int taille, int depart, chemin_t * chemin_final)
{
    int ** matrice_complete = Matrice_complete(MATRICE,taille);

    //methode_gloutonne

    int nb_sommet_cumule = 1;
    int dist = 0;
    int limit = taille * 5;

    int chemin_depart_recuit[taille +1];

    Chemin_avec_meilleur_voisin(matrice_complete, taille, chemin_depart_recuit, &nb_sommet_cumule, &dist, depart, limit);

    chemin_depart_recuit[taille] = depart;
    
    //Recuit simulé
    int max_ite = 10000000;
    float p = 0.1;
    int distance_min = 1000000;

    for(int i = 0;i<max_ite;i++)
    {
        int d = distance_from_chemin(chemin_depart_recuit, taille, matrice_complete);
    

        if (d < distance_min || rand() < p)
        {
            chemin_final ->distance = d;
            for(int j = 0;j<taille+1;j++)
            {
                chemin_final->suite[j] = chemin_depart_recuit[j];
            }
        }

        minor_swap(chemin_depart_recuit, taille);
        p = p*0.995;
        //printf("proba = %f\n",p);
    }

    chemin_final->nb_sommet = taille+1;



    LibererMatrice(matrice_complete, taille);
}

/***********************************LES FOURMIES***********************************/

void main_fourmie(int **matrice, int **matriceProba, int* listSommetDiff, int taille, int sommetDep, int *list_plus_court_chemin)
{
    int sommet = sommetDep;
    listSommetDiff[0] = 0;
    int indice_listSommetDiff = 1;
    int indice_list_plus_court_chemin = 0;
    while(indice_listSommetDiff < taille)
    {
        int sommet_suiv = choix_chemin(taille, listSommetDiff);
        listSommetDiff[indice_listSommetDiff] = sommet_suiv;
        indice_listSommetDiff += 1;
        modif_mat_prob(matriceProba, sommet, sommet_suiv, taille);
        sommet = sommet_suiv;
    }

    listSommetDiff[indice_listSommetDiff] = sommetDep;//on retourne au depart

    for(int i = 0; i < taille - 1; i++)
    {
        chemin_t chemin_le_plus_cours ;
        //chemin_le_plus_cours = initialisation_chemin(taille);
        int code = 0;
        Plus_court_chemin(matrice, taille, listSommetDiff[i], listSommetDiff[i + 1],&chemin_le_plus_cours, &code);

        for (int j = 0; j < chemin_le_plus_cours.nb_sommet; j ++)
        {   
            if (list_plus_court_chemin[indice_list_plus_court_chemin -1] != chemin_le_plus_cours.suite[j])
            {
                list_plus_court_chemin[indice_list_plus_court_chemin] = chemin_le_plus_cours.suite[j];
                indice_list_plus_court_chemin += 1;
            }
        }
    }
    list_plus_court_chemin[indice_list_plus_court_chemin] = sommetDep;
}


void modif_mat_prob(int **matriceProba, int sommet, int sommet_suiv, int taille)
{
    matriceProba[sommet][sommet_suiv] = 0;
    matriceProba[sommet_suiv][sommet] = 0;

    int somme = 0;
    for (int j = 0; j < taille; j++)
    {
        if (matriceProba[sommet][j] != 0)
            somme += 1;
    }
    if (somme != 0)
    {
        for (int j = 0; j < taille; j++)
        {
            if (matriceProba[sommet][j] != 0)
                {
                    matriceProba[sommet][j] = (int)100/somme;
                }
        }
    }
    
}

Bool est_dans(int *listSommetDiff, int sommet_suiv, int taille)
{
    int length = taille_liste(listSommetDiff, taille);

    for (int i = 0; i < length; i++)
    {
        if ( listSommetDiff[i] == sommet_suiv)
            return true;
    }

    return false;
}


int choix_chemin(int taille, int *listSommetDiff)
{
    int sommet_suiv = -1;

    do
    {
        sommet_suiv = rand()%taille;
    }while(est_dans(listSommetDiff, sommet_suiv, taille));
    return sommet_suiv;
}
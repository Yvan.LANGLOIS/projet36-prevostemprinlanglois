#ifndef __VOYAGEUR_CORENTIN_H__
#define __VOYAGEUR_CORENTIN_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdlib.h>
#include <time.h>   // pour rand 
#include "Creation_de_matrice.h"

typedef enum
{
    false,
    true
}Bool;


void Tous_les_voisins(int A, int N_sommet, int ** MATRICE, int * list_voisin);

void init_list_voisins(int * list, int taille);

void Voisins_non_visites(int * deja_visite, int * list_voisin, int N_sommet, int nb_deja_vis);

int Meilleur_voisin(int A, int N_sommet, int ** MATRICE, int * deja_visite, int nb_deja_vu);

void liste_voisin_explicite(int A, int N_sommet, int ** MATRICE, int * vois_explicite, int * nb_vois);

int Recherche_sommet(int A, int * list, int taille);

int Chemin_avec_meilleur_voisin(int ** MATRICE, int N_sommet, int * chemin_final, int * nb_sommet_cumule, int * distance_cumule, int point_depart, int limit_ite);

void Opti_avec_meilleur_voisin(int ** MATRICE, int N_sommet, int ** meilleur_chemin, int * nb_sommet_meilleur, int * distance_cumule_meilleur, int point_depart, int nb_ite, int limite);

typedef struct chemin {
    int nb_sommet;
    int distance;
    int * suite;
} chemin_t;

chemin_t initialisation_chemin(int taille);

void copier_chemin(chemin_t* source, chemin_t* dest);

void Ajouter_sommet(int A, int num_chemin, chemin_t * ch, int ** MATRICE);

void creer_chemin_par_voisin(int ** MATRICE, chemin_t * liste_chemin, chemin_t * chemin_suivant, int num_chemin, int taille, int * nb_chemin);

void liberer_chemin(chemin_t * ch, int taille);

void Plus_court_chemin(int ** MATRICE, int N_sommet, int A, int B, chemin_t * chemin_le_plus_cours, int * code_err);

void afficher_chemin(chemin_t PCC);

int **  Matrice_complete(int ** MATRICE, int N_sommet);

void LibererMatrice (int ** MATRICE, int taille);

void recuit_simule(int ** MATRICE, int taille, int depart, chemin_t * chemin_final);

void randomizer (int taille, int * liste_random, int depart);

void decale_gauche(int * list, int taille, int indice);

int distance_from_chemin(int* chemin,int taille,int ** matrice_comp);

void minor_swap(int * chemin_a_changer, int taille);

void methode_gloutonne(int ** MATRICE, int taille, int depart, chemin_t * chemin_final);

void recuit_simule_sur_methode_gloutonne(int ** MATRICE, int taille, int depart, chemin_t * chemin_final);

/***********************************LES FOURMIES***********************************/

void algo_pour_trouver_bon_chemin_fourmie(int taille, int **matriceConnexion, int **matriceProba, int *parcours_fourmie, int nb_iteration);

//init_list_voisins(parcours_fourmie1, 5 * taille * taille); // à initialiser avant
void algo_fourmies(int taille, int **matriceConnexion, int **matriceProba, int *parcours_fourmie1);

int taille_liste(int *list, int taille);

void ajout_pheromones(int** matriceProba, int *parcours_fourmie, int pheromones_ajoute, int taille);

void algo_parcours_d_une_fourmie(int taille, int **matriceConnexion, int **matriceProba, int *tab_des_sommets_parcourru);

int donne_le_sommet_suiv_aleat(int nb_aleat, int *list_voisin_poids, int taille);

void fusion(int tableau[], int debut, int milieu, int taille);

void triFusion(int tableau[], int debut, int taille);

void affiche_liste(int *liste, int taille);

void triInsertion(int tableau[], int taille);




/***********************************LES FOURMIES***********************************/

//int listSommetDiff[taille + 1];
void main_fourmie(int **matrice, int **matriceProba, int* listSommetDiff, int taille, int sommetDep, int *list_plus_court_chemin);

Bool est_dans(int *listSommetDiff, int sommet_suiv, int taille);

void modif_mat_prob(int **matriceProba, int sommet, int sommet_suiv, int taille);

int choix_chemin(int taille, int *listSommetDiff);

#endif
#include "voyageur_Yvan.h"


void endSDL(sdlMedia_t *media){

	free(media->stringListe);
	free(media->stringPos);
	TTF_Quit();
	if(media->window != NULL) SDL_DestroyWindow(media->window);
	//SDL_Quit();
	//free(media);

}

SDL_Texture* loadStringTexture(sdlMedia_t *media, char *str, SDL_Color color){
	SDL_Surface* textSurf = NULL;
	textSurf = TTF_RenderText_Blended(media->font, str, color);
	if(textSurf == NULL) endSDL(media);
	
	SDL_Texture* textText = NULL;
	textText = SDL_CreateTextureFromSurface(media->renderer, textSurf);
	if(textText==NULL) endSDL(media);
	SDL_FreeSurface(textSurf);
	
	return textText;
}

void loadSommetLabel(sdlMedia_t *media, sommet_t *listeSommet, int nSommet){
	
	media->nSommet = nSommet;
	
	media->labelListe =  (SDL_Texture **) malloc(sizeof(SDL_Texture*)*nSommet);
	media->labelPos = (SDL_Rect *) malloc(nSommet*sizeof(SDL_Rect));

	SDL_Color color = {255,0,255,255};
	int i;
	for(i = 0; i<nSommet; i++){
	
		char s[21];
		snprintf(s, sizeof(s), "S%d", i);
		SDL_Texture* textText = loadStringTexture(media, s, color);
		media->labelListe[i] = textText;
		int x, y;
		x = listeSommet[i].x-10;
		y = listeSommet[i].y+20;
		media->labelPos[i] = (SDL_Rect){x,y,0,0};
		SDL_QueryTexture(media->labelListe[i], NULL, NULL, &(media->labelPos[i].w),  &(media->labelPos[i].h));
	}
}

void loadColoredString(char *str, SDL_Color color, int *pos, sdlMedia_t *media, int strId){

	SDL_Texture* textText = loadStringTexture(media, str, color);
	
	media->stringListe[strId] = textText;
	media->stringPos[strId] = (SDL_Rect){pos[0], pos[1], 0, 0};
	SDL_QueryTexture(media->stringListe[strId], NULL, NULL, &(media->stringPos[strId].w), &(media->stringPos[strId].h));

}

void loadString(char *str, int *pos, sdlMedia_t *media, int strId){
	SDL_Color color = {255,255,255,255};
	loadColoredString(str, color, pos, media, strId);
}

sdlMedia_t * initSDL(int nString){
	
	srand(time(NULL));
	
	sdlMedia_t *media = (sdlMedia_t*) malloc(sizeof(sdlMedia_t*));
	
	/* Initialisation de la SDL  + gestion de l'échec possible */
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		SDL_Log("Erreur : SDL initialisation - %s\n", SDL_GetError());                // l'initialisation de la SDL a échoué 
		exit(EXIT_FAILURE);
	}
	
	/* Création de la fenetre SDL */
	
	SDL_Window *win = SDL_CreateWindow("Fenetre", 0, 0, 1800, 1000, SDL_WINDOW_RESIZABLE);
	
	if(win == NULL){
		SDL_Log("Error : SDL window creation - %s\n", 
		SDL_GetError());                 	// échec de la création de la fenêtre
		SDL_Quit();                              // On referme la SDL       
		exit(EXIT_FAILURE);
	}
	
	media->window = win;		// Chargement de la fenetre dans la structure sdlMedia
	media->renderer = SDL_CreateRenderer(media->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);		// Chargement du renderer dans la structure sdlMedia
	
	/* Police d'ecriture */
	
	if(TTF_Init()<0) endSDL(media);
	media->font = TTF_OpenFont("./font.ttf", 24);
	media->nString = nString;
	media->stringListe =  (SDL_Texture **) malloc(sizeof(SDL_Texture**)*nString);
	media->stringPos = (SDL_Rect *) malloc(nString*sizeof(SDL_Rect));
	
	return media;

}

void draw(sdlMedia_t *media){
	SDL_SetRenderDrawColor(media->renderer, 0, 0, 0, 255);
	SDL_RenderClear(media->renderer);
	
}

sommet_t* getListeSommet(int nSommet, int width, int height){

	sommet_t *liste = (sommet_t *) malloc(sizeof(sommet_t)*nSommet);
	int taux = 9;	
	int i, j;
	int **tab = (int**)malloc(sizeof(int*)*taux);
	for(i = 0; i<taux; i++) {
		tab[i] = (int*)malloc(nSommet*sizeof(int));	
		if(tab[i] == NULL) printf("erreur\n");
	}
	
	for(i = 0; i<taux; i++){
		for(j = 0; j<nSommet; j++){
			tab[i][j] = 0;
		}
	}
	
	int libre = (nSommet*taux);
	int caseW = width/nSommet, caseH = height/taux;
	printf("DIMENSION %d, %d \n",caseW,caseH);
	for(i=0; i<nSommet; i++){
		int r = rand()%libre;
		int u  = 0, v = 0, k = 0;
		int code = 0;
		while(u<taux-1 && code == 0){
			v = 0;
			while(v<nSommet && code == 0){
				if(tab[u][v]==0){
					if(k==r) code = 1;					
					k++;
				}
				v++;
			}
			u++;
		}
		u--; v--;
		
		tab[u][v] = 1;
		if(u>0) tab[u-1][v] = 1;
		if(v>0) tab[u][v-1] = 1;
		if(u>0 && v>0) tab[u-1][v-1] = 1;
		if(u<nSommet) tab[u+1][v] = 1;
		if(v<9) tab[u][v+1] = 1;
		if(u<nSommet && v<9) tab[u+1][v+1] = 1;
		if(u>0 && v<9) tab[u-1][v+1] = 1;
		if(u<nSommet && v>0) tab[u+1][u-1] = 1;
		libre-=9;
		liste[i].x = 50+caseW*u+rand()%(caseW/2);
		liste[i].y = 50+caseH*v+rand()%(caseH/2);
	}
	
	free(tab);
	
	
	
	return liste;
}


void endSommet(sommet_t *listeSommet){
	free(listeSommet);
}

void drawCircle(struct sdlMedia *media, int centerX, int centerY, int radius){
	for (int angle = 0; angle < 360; angle++) 
	    {
		float radian = angle * (M_PI / 180.0);
		int x = centerX + radius * cos(radian);
		int y = centerY + radius * sin(radian);
		SDL_RenderDrawPoint(media->renderer, x, y);
	    }
}

void drawUsrPath(sdlMedia_t *media, usrSommet_t *racine, sommet_t *listeSommet){
	SDL_SetRenderDrawColor(media->renderer, 255, 200, 0, 255);
	usrSommet_t *cour = racine;
	while(cour->fils!=NULL){
		usrSommet_t *fils = cour->fils;
		sommet_t srcSommet = listeSommet[cour->sommetId];
		sommet_t dstSommet = listeSommet[fils->sommetId];
		
		SDL_RenderDrawLine(media->renderer, srcSommet.x, srcSommet.y, dstSommet.x, dstSommet.y);
		cour = fils;
	}
}


void drawLine(sdlMedia_t *media, int xSrc, int ySrc, int xDst, int yDst){
	SDL_RenderDrawLine(media->renderer, xSrc, ySrc, xDst, yDst);
}

void drawThickLine(sdlMedia_t *media, int xSrc, int ySrc, int xDst, int yDst, int thickness){
	int i;
	int limit = thickness/2;
	if( thickness==1) {
		drawLine(media, xSrc, ySrc, xDst, yDst);
		return;
	}
	for(i = -limit; i<limit; i++){
		drawLine(media, xSrc+i, ySrc+i, xDst+i, yDst+i);
	}
}


void drawPossibilities(sdlMedia_t *media, usrSommet_t *benjamin, sommet_t *listeSommet, int **matrice, int nSommet){
	int id = benjamin->sommetId;
	sommet_t ben = listeSommet[id];
	SDL_SetRenderDrawColor(media->renderer, 0, 100, 255, 10);
	
	int i;
	for(i = 0; i<nSommet; i++){
		if(matrice[id][i] != 0){
			sommet_t som = listeSommet[i];
			drawLine(media, ben.x, ben.y, som.x, som.y);
		}
	}
}



usrSommet_t* update(usrSommet_t *benjamin, int sommetId, sommet_t *listeSommet, int nSommet, usrSommet_t *racine, int *code){
	benjamin->fils = (usrSommet_t*) malloc(sizeof(usrSommet_t));
	usrSommet_t *new = benjamin->fils;
	new->sommetId = sommetId;
	new->fils = NULL;
	listeSommet[sommetId].visite = 1;
	
	if(sommetId==racine->sommetId){
		printf("fin de route\n");
		int i, visite = 1;
		for(i = 0; i<nSommet; i++){
			if(listeSommet[i].visite != 1){
				visite = 0;
				break;
			}
		}
		if(visite == 1){
			printf("Voyage termine !\n");
			calcLongueurVoyage(racine, listeSommet);
			*code = 1;
		}
	}
	return new;
}

int calcLongueurVoyage(usrSommet_t *racine, sommet_t *listeSommet){
	int norme = 0;
	usrSommet_t *cour = racine;
	while(cour->fils != NULL){
		sommet_t src = listeSommet[cour->sommetId];
		sommet_t dst = listeSommet[cour->fils->sommetId];
		
		int add = (int)pow(((dst.x-src.x)*(dst.x-src.x) + (dst.y-src.y)*(dst.y-src.y)),0.5);
		printf("%d\n", add);
		norme +=  add;
		cour = cour->fils;
	}

	printf("Norme: %d\n", norme);
	return norme;
}

void freeUsrSommet(usrSommet_t *racine){
	
	usrSommet_t *cour = racine;
	usrSommet_t *tmp = NULL;
	while(cour->fils!=NULL){
		tmp = cour;
		cour = tmp->fils;
		free(tmp);
	}
	free(cour);
	
}

void drawNextLine(sdlMedia_t *media, sommet_t benjamin, int mx, int my){
	SDL_SetRenderDrawColor(media->renderer, 255, 0, 0, 255);
	SDL_RenderDrawLine(media->renderer, benjamin.x, benjamin.y, mx, my);
}

void loop(sdlMedia_t *media, sommet_t *listeSommet, int nSommet, int** matriceConnexion, usrSommet_t *racine, int **matriceProba){
	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	usrSommet_t *benjamin = racine;
	
	int code = 0;
	
	while(program_on){
		int mx, my;
		if(SDL_PollEvent(&event)){		// il y a des elements dans la file des evenements
		switch(event.type){
		case SDL_KEYDOWN:
		
			switch(event.key.keysym.sym){
			case SDLK_ESCAPE:
				program_on = SDL_FALSE;
				break;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			mx = event.motion.x;
			my = event.motion.y;
			int i;
			for(i = 0; i<nSommet; i++){
				int cX = listeSommet[i].x, cY = listeSommet[i].y;	// cX coordonnee x du centre, cY coordonee y du centre du sommet
				if( ((mx-cX)*(mx-cX) + (my-cY)*(my-cY)) <=20*20 && matriceConnexion[i][benjamin->sommetId]!=0 && code==0) benjamin = update(benjamin, i, listeSommet, nSommet, racine, &code);
				
				
				
			}
			break;
		case SDL_MOUSEMOTION:
			mx = event.motion.x;
			my = event.motion.y;
			
			break;
		case SDL_QUIT :                           
			program_on = SDL_FALSE; 
			break;	
		default:
			break;
		}
		}
		
		// AFFICHAGE
		draw(media);
		
		
		SDL_SetRenderDrawColor(media->renderer, 255, 255, 0, 255);
		int i, j;
		for(i=0; i<nSommet; i++){
					drawCircle(media, listeSommet[i].x, listeSommet[i].y, 20);	
					SDL_SetRenderDrawColor(media->renderer, 0, 255, 0, 255);

		}
		
		for(i=0; i<nSommet; i++){
			for(j=0; j<nSommet; j++){
				if(matriceConnexion[i][j]) {			
					int intensite = 155+(matriceProba[i][j]+matriceProba[j][i])%100;
					SDL_SetRenderDrawColor(media->renderer, intensite, intensite, intensite, 255);
					drawThickLine(media,listeSommet[i].x, listeSommet[i].y, listeSommet[j].x, listeSommet[j].y,1); 
				}
			}
		}
		
		drawUsrPath(media, racine, listeSommet);
		if(code == 0) drawPossibilities(media, benjamin, listeSommet, matriceConnexion, nSommet);
		if(code == 0){
			drawNextLine(media, listeSommet[benjamin->sommetId],mx,my);
		}
		for(i = 0; i<media->nString; i++){
			SDL_RenderCopy(media->renderer, media->stringListe[i], NULL, &(media->stringPos[i]));
		}
		for(i = 0; i<media->nSommet; i++){
			SDL_RenderCopy(media->renderer, media->labelListe[i], NULL, &(media->labelPos[i]));
		}
		SDL_RenderPresent(media->renderer);
		SDL_Delay(2);
	}
}

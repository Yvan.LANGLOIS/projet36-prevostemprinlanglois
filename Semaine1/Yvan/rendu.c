#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct sommet sommet;
struct sommet{
	int x, y;
	struct sommet *fils;
};

void drawPath(SDL_Renderer* renderer, struct sommet *racine){
	int delta = 1;					// delta de deplacement (vitesse)
	SDL_SetRenderDrawColor(renderer, 0,255,0,255);
	struct sommet *cour = racine;
	while(cour->fils != NULL){
		sommet * fils = cour->fils;
				
		//int startX = cour->x, startY = cour->y, endX = fils->x, endY = fils->y;
		
		int random = rand()%100;	
		if(fils->fils!=NULL && random<50){		// probabilite que le point bouge
			int direction = rand()%4;		// direction que le point va prendre
			switch(direction){
				case 0:		// Direction nord
					cour->y-= delta;
					break;
				case 1:		// Direction est
					cour->x+=delta;
					break;
				case 2: 	// Direction sud
					cour->y+=delta;
					break;
				case 3:		//Direction ouest
					cour->x-=delta;				
					break;
			}
		}
		
		SDL_RenderDrawLine(renderer, cour->x, cour->y, fils->x, fils->y);
		cour = fils;
	}

}

void drawNextLine(SDL_Renderer* renderer, struct sommet* sommet, int mx, int my){
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	SDL_RenderDrawLine(renderer, sommet->x, sommet->y, mx, my);
}

void draw(SDL_Renderer* renderer){
	
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	
	SDL_Rect rect;
	SDL_SetRenderDrawColor(renderer, 0,255,0,255);
	rect.x = 0; rect.y = 0; rect.w = 100; rect.h = 30;
	SDL_RenderFillRect(renderer, &rect);
	
}

struct sommet* update(struct sommet *racine, int mx, int my){
	int k = 0;
	struct sommet *cour = racine;
	while(cour->fils!=NULL){
		k++;
		cour = cour->fils;
	}
	cour->fils = (struct sommet*) malloc(sizeof(struct sommet*));
	cour = cour->fils;
	cour->x = mx;
	cour->y = my;
	printf("Nombre de sommets: %d\n",k);
	return cour;
	
}

void end_sdl(struct sommet* racine, SDL_Window *window){
	struct sommet* courant = racine;
	struct sommet* temp = NULL;
	
	while(courant->fils != NULL){
		temp = courant;
		courant = courant->fils;
		free(temp);
	}
	//free(cour);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

int main(){
	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_DisplayMode dm;			// Objet SDL_DisplayMode
	
	
	/* Initialisation de la SDL  + gestion de l'échec possible */
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
	SDL_Log("Error : SDL initialisation - %s\n", 
		  SDL_GetError());                // l'initialisation de la SDL a échoué 
	exit(EXIT_FAILURE);
	}

	/* Recherche de la taille de l'écran*/
	if (SDL_GetDesktopDisplayMode(0, &dm) != 0){	// On associe SDL_DisplayMode à la SDL
     		SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());	// On verifie qu'il n'y a pas d'erreur
     		return 1;
	}
	int screenW = dm.w, screenH = dm.h;	// On recupere les parametres width et height de l'écran
	
	/* Creation de la fenetre*/
	window = SDL_CreateWindow("Fenetre", 0, 0, 600, 400, SDL_WINDOW_RESIZABLE);
	
	if(window == NULL){
		SDL_Log("Error : SDL window creation - %s\n", 
		SDL_GetError());                 // échec de la création de la fenêtre
		SDL_Quit();                              // On referme la SDL       
		exit(EXIT_FAILURE);
	}

	/* Renderer */
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	/* Sommets*/
	
	struct sommet *r2 = (struct sommet*) malloc(sizeof(struct sommet*));
	r2->x = 130;
	r2->y = 80;
	r2->fils = NULL;
	struct sommet *r1 = (struct sommet*) malloc(sizeof(struct sommet*));
	r1->x = 100;
	r1->y = 80;
	r1->fils = r2;
	struct sommet *benjamin = r2;		// element benjamin de la liste des sommets
	
	
	/* Boucle d'events*/
	int r =0;
	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	
	while(program_on){				// boucle du programme
		int mx, my;
		if(SDL_PollEvent(&event)){		// il y a des elements dans la file des evenements
		switch(event.type){
		case SDL_KEYDOWN:
			switch(event.key.keysym.sym){
			case SDLK_SPACE:
				r = rand()%100;
				printf("space ! %d \n", r);
				break;
			
			case SDLK_ESCAPE:
				program_on = SDL_FALSE;
				break;
		  	}
		case SDL_MOUSEBUTTONDOWN:
			mx = event.motion.x;
			my = event.motion.y;
			
			benjamin = update(r1, mx, my);
		        break;    
		case SDL_MOUSEMOTION:
			mx = event.motion.x; 
			my = event.motion.y;
			
			//printf("mx: %d my: %d\n",mx, my);
			break; 
		case SDL_QUIT :                           
		program_on = SDL_FALSE;                 // fin de programme
		break;

		default:
		break;
		}
		
		}
		// AFFICHAGE
		draw(renderer);
		drawPath(renderer, r1);
		drawNextLine(renderer, benjamin, mx, my);
		SDL_RenderPresent(renderer);
		SDL_Delay(2);
	}
	end_sdl(r1, window);
	
	return 0;
}

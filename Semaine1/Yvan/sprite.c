#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL_ttf.h>

typedef struct objet objet;
struct objet{
	SDL_Texture **textureListe;	// Liste des textures du programme
	int *textID;			// Tableau des indices des texture de l'objet
	int nSprite;			// Nombre d'image de l'animation
	int spriteState;		// Indice actuel de l'animation
	int critere;
	int x, y, w, h;			// Position et dimension de l'objet
	int speed, angle;		// Vitesse et angle de rotation (en degré) de l'image
};

void draw(SDL_Renderer* renderer){
	
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	
	SDL_Rect rect;
	SDL_SetRenderDrawColor(renderer, 0,255,0,255);
	rect.x = 0; rect.y = 0; rect.w = 100; rect.h = 30;
	SDL_RenderFillRect(renderer, &rect);
	
}


void end_sdl(SDL_Window *window, SDL_Texture **textureListe){
	
	TTF_Quit();
	free(textureListe);
	IMG_Quit();
	SDL_DestroyWindow(window);
	SDL_Quit();
	
}

SDL_Texture* loadString(char *str, TTF_Font *font, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture **textureListe){

	SDL_Color color = {255,255,255,255};
	SDL_Surface* textSurf = NULL;
	textSurf = TTF_RenderText_Blended(font, str, color);
	if(textSurf == NULL) end_sdl(window, textureListe);
	
	SDL_Texture* textText = NULL;
	textText = SDL_CreateTextureFromSurface(renderer, textSurf);
	if(textText==NULL) end_sdl(window, textureListe);
	SDL_FreeSurface(textSurf);
	return textText;

}

SDL_Texture* load_texture_from_file(char *file_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture **textureListe){
	SDL_Surface *surface = NULL;
	SDL_Texture *texture = NULL;
	
	surface = IMG_Load(file_name);					// Charge l'image dans un objet Surface
	
	if(surface == NULL) end_sdl(window, textureListe);				// Si l'image n'est pas chargée dans la surface
	
	texture = SDL_CreateTextureFromSurface(renderer, surface);	// on transforme la surface en texture
	SDL_FreeSurface(surface);					// On libère la surface
	if(texture == NULL) end_sdl(window, textureListe);
	
	return texture;
}

void play_texture(struct objet *objet, SDL_Window *window, SDL_Renderer *renderer){
	SDL_Rect destination = {objet->x, objet->y, objet->w, objet->h};
	SDL_RenderCopyEx(renderer, objet->textureListe[objet->textID[objet->spriteState]], NULL, &destination, objet->angle, NULL, SDL_FLIP_NONE);

	//git(renderer, objet->textureListe[objet->textID[objet->spriteState]], NULL, &destination, objet->angle, NULL, SDL_FLIP_NONE);
	
	int random = rand()%100;
	if(random<objet->critere) objet->spriteState = objet->spriteState+1;	// En fonction du critere de probabilite, on avance dans les textures de l'objet
	if(objet->spriteState == objet->nSprite) objet->spriteState = 0;
	//SDL_RenderPresent(renderer);
}

int main(){
	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_DisplayMode dm;			// Objet SDL_DisplayMode
	
	/* Initialisation de la SDL  + gestion de l'échec possible */
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
	SDL_Log("Error : SDL initialisation - %s\n", 
		  SDL_GetError());                // l'initialisation de la SDL a échoué 
	exit(EXIT_FAILURE);
	}

	/* Recherche de la taille de l'écran*/
	if (SDL_GetDesktopDisplayMode(0, &dm) != 0){	// On associe SDL_DisplayMode à la SDL
     		SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());	// On verifie qu'il n'y a pas d'erreur
     		return 1;
	}
	int screenW = dm.w, screenH = dm.h;	// On recupere les parametres width et height de l'écran
	
	/* Creation de la fenetre*/
	window = SDL_CreateWindow("Fenetre", 0, 0, screenW, screenH, SDL_WINDOW_RESIZABLE);
	
	if(window == NULL){
		SDL_Log("Error : SDL window creation - %s\n", 
		SDL_GetError());                 // échec de la création de la fenêtre
		SDL_Quit();                              // On referme la SDL       
		exit(EXIT_FAILURE);
	}

	/* Renderer */
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	/* Textures */
	int nTexture = 6;				// Nombre de textures à charger
	
	SDL_Texture **textureListe = (SDL_Texture **) malloc(sizeof(SDL_Texture*)*nTexture);		// Liste des textures du programme
	
	textureListe[0] = load_texture_from_file("./res/arrow1.png", window, renderer, textureListe);
	textureListe[1] = load_texture_from_file("./res/arrow2.png", window, renderer, textureListe);
	textureListe[2] = load_texture_from_file("./res/arrow3.png", window, renderer, textureListe);
	textureListe[3] = load_texture_from_file("./res/backImg1.png", window, renderer, textureListe);
	textureListe[4] = load_texture_from_file("./res/backImg2.png", window, renderer, textureListe);
	textureListe[5] = load_texture_from_file("./res/backImg3.png", window, renderer, textureListe);
	
	/* Police d'ecriture */
	int nString = 3;				// Nombre de textes à charger
	
	if(TTF_Init()<0) end_sdl(window, textureListe);
	TTF_Font *font = NULL;
	
	font = TTF_OpenFont("./res/Pacifico.ttf", 65);
	if(font == NULL) end_sdl(window, textureListe);
	TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);
	
	SDL_Texture **stringListe = (SDL_Texture **) malloc(sizeof(SDL_Texture**)*nString);
	
	stringListe[0] = loadString("Yvan", font, window, renderer, textureListe);
	stringListe[1] = loadString("Echap pour quitter", font, window, renderer, textureListe);
	stringListe[2] = loadString("ZQSD -> Deplacements", font, window, renderer, textureListe);
	

	SDL_Rect *stringPos = (SDL_Rect *) malloc(nString*sizeof(SDL_Rect));
	
	stringPos[0] = (SDL_Rect){300, 80, 0, 0};
	stringPos[1] = (SDL_Rect){80, 580, 0, 0};
	stringPos[2] = (SDL_Rect){80, 780, 0, 0};
	
	int i;
	for(i = 0; i<nString; i++){
		SDL_QueryTexture(stringListe[i], NULL, NULL, &stringPos[i].w, &stringPos[i].h);
	}
	
	/* Objets */
	struct objet fleche;
	fleche.x = 0;
	fleche.y = 0;
	fleche.w = 160;
	fleche.h = 220;
	fleche.nSprite = 3;
	fleche.textID = (int*) malloc(fleche.nSprite*(sizeof(int)));
	fleche.textID[0] = 0;
	fleche.textID[1] = 1;
	fleche.textID[2] = 2;
	fleche.spriteState = 0;
	fleche.textureListe = textureListe;
	fleche.speed = 3;
	fleche.angle = 90;
	fleche.critere = 60;
	
	struct objet back;
	back.x = 0; back.y = 0; back.w = screenW; back.h = screenH;
	back.nSprite = 8;
	back.textID = (int*) malloc(back.nSprite*(sizeof(int)));
	back.textID[0] = 3;
	back.textID[1] = 3;
	back.textID[2] = 4;
	back.textID[3] = 4;
	back.textID[4] = 5;
	back.textID[5] = 5;
	back.textID[6] = 4;
	back.textID[7] = 4;
	back.spriteState = 0;
	back.textureListe = textureListe;
	back.angle = 0;
	back.critere = 20;
	
	/* Boucle d'events*/

	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	int r = 0;
	
	while(program_on){				// boucle du programme
		int mx, my;
		if(SDL_PollEvent(&event)){		// il y a des elements dans la file des evenements
		switch(event.type){
		case SDL_KEYDOWN:
			switch(event.key.keysym.sym){
			case SDLK_s:
				fleche.y+=fleche.speed; 	// Deplacement de la fleche
				fleche.angle = 180;
				break;
			case SDLK_d:
				fleche.x+=fleche.speed;
				fleche.angle = 90;
				break;
			case SDLK_z:
				fleche.y-=fleche.speed;
				fleche.angle = 0;
				break;
			case SDLK_q:
				fleche.x-=fleche.speed;
				fleche.angle = 270;
				break;
			case SDLK_SPACE:
				r = rand()%100;
				printf("space ! %d \n", r);
				break;
			
			case SDLK_ESCAPE:
				program_on = SDL_FALSE;
				break;
		  	}
		case SDL_MOUSEBUTTONDOWN:
			mx = event.motion.x;
			my = event.motion.y;
			
		        break;    
		case SDL_MOUSEMOTION:
			mx = event.motion.x; 
			my = event.motion.y;
			
			break; 
		case SDL_QUIT :                           
		program_on = SDL_FALSE;                 // fin de programme
		break;

		default:
		break;
		}
		
		}
		// AFFICHAGE
		draw(renderer);
		play_texture(&back, window, renderer);
		play_texture(&fleche, window, renderer);
		
		for(i = 0; i<nString; i++){
			SDL_RenderCopy(renderer, stringListe[i], NULL, &stringPos[i]);
		}
		SDL_RenderPresent(renderer);
		SDL_Delay(20);
	}
	
	for(i = 0; i<nString; i++){
		SDL_DestroyTexture(stringListe[i]);
	}
	end_sdl(window, textureListe);
	
	return 0;
}


// Commande " gcc -o prg sprite.c -I/usr/include/SDL2 -lSDL2 -lSDL2_image -g " 

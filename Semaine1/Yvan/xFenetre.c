#include <SDL2/SDL.h>
#include <stdio.h>



/************************************/
/*  exemple de création de fenêtres */
/************************************/




int main(int argc, char **argv) {
	(void)argc;
	(void)argv;

	// Definition du X
	int tabLenght = 3;			// L'écran va être séparé en un tableau de tail tabLenght * tabLenght

	SDL_Window 
	*window_1 = NULL,              		// Future fenêtre de gauche
	*window_2 = NULL;             	        // Future fenêtre de droite

	/* Initialisation de la SDL  + gestion de l'échec possible */
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
	SDL_Log("Error : SDL initialisation - %s\n", 
		  SDL_GetError());                // l'initialisation de la SDL a échoué 
	exit(EXIT_FAILURE);
	}

	/* Recherche de la taille de l'écran*/
	SDL_DisplayMode dm;			// Objet SDL_DisplayMode
	if (SDL_GetDesktopDisplayMode(0, &dm) != 0){	// On associe SDL_DisplayMode à la SDL
     		SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());	// On verifie qu'il n'y a pas d'erreur
     		return 1;
	}
	int screenW = dm.w, screenH = dm.h;	// On recupere les parametres width et height de l'écran
	
	/* Creation des fenêtres*/

	SDL_Window **winListe;
	winListe = (SDL_Window**) malloc(sizeof(SDL_Window*)*(tabLenght*2-1));	// Tableau dynamique (ne fonctionne pas)
	SDL_Renderer **renderer = (SDL_Renderer**) malloc(sizeof(SDL_Renderer*)*tabLenght*2-1); 
	
	int i, j, k = 0;
	
	if(winListe != NULL){
		
		for(i = 0; i<tabLenght; i++){
			for(j = 0; j<tabLenght; j++){
			
			if(i==(tabLenght-j-1)){
				winListe[k] = SDL_CreateWindow("Fenetre", (int)screenW/tabLenght*i, (int)screenH/tabLenght*j, (int)screenW/tabLenght, (int)screenH/tabLenght, SDL_WINDOW_RESIZABLE);
				
				if(winListe[k] == NULL){
					SDL_Log("Error : SDL window %d creation - %s\n",i, 
					SDL_GetError());                 // échec de la création de la fenêtre
					SDL_Quit();                              // On referme la SDL       
					exit(EXIT_FAILURE);
					}
				renderer[i] = SDL_CreateRenderer(winListe[i], -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
				k++;
				}
				
			else if(i==j){
			winListe[k] = SDL_CreateWindow("Fenetre", (int)screenW/tabLenght*i, (int)screenH/tabLenght*j, (int)screenW/tabLenght, (int)screenH/tabLenght, SDL_WINDOW_RESIZABLE);
				if(winListe[k] == NULL){
					SDL_Log("Error : SDL window %d creation - %s\n",i, 
					SDL_GetError());                 // échec de la création de la fenêtre
					SDL_Quit();                              // On referme la SDL       
					exit(EXIT_FAILURE);
					}
					k++;
				}
			}
			
			
		}
	}
	else{
		SDL_Log("Erreur creation des fenetres");
	}
		
////////

	/* Normalement, on devrait ici remplir les fenêtres... */
	//SDL_Delay(2000);                           // Pause exprimée  en ms
	SDL_bool
	program_on = SDL_TRUE,                          // Booléen pour dire que le programme doit continuer
	paused = SDL_FALSE,                             // Booléen pour dire que le programme est en pause
	event_utile = SDL_FALSE;                        // Booléen pour savoir si on a trouvé un event traité 
	SDL_Event event;                                  // Evènement à traiter

	while (program_on){                           // Voilà la boucle des évènements 

	int i;
	for(i = 0; i<tabLenght*2-1; i++){
		SDL_SetRenderDrawColor(renderer[i], 0, 0, 0, 255);
		SDL_RenderClear(renderer[i]);
	}

	if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête
		                          // de file dans 'event'
	switch(event.type){                       // En fonction de la valeur du type de cet évènement
	case SDLK_ESCAPE:
	
	case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
	program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
	break;

	default:                                  // L'évènement défilé ne nous intéresse pas
	break;
	}
	}
	// Affichages et calculs souvent ici
	}  

///////

	/* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
	
	for(i = 0; i<tabLenght*2-1; i++){
		SDL_DestroyWindow(winListe[i]);
	}
	free(winListe);			// pour tableau de fenetres 
	SDL_Quit();                             // la SDL

	return 0;
}

#include "Creation_de_matrice.h"



int** Creation_Matrice(int taille)
{
    int** matrice = (int**)malloc(sizeof(int*) * taille);

    srand(time(NULL));

    for (int i = 0; i < taille; i ++)
        matrice[i] = (int*)malloc(sizeof(int) * taille);

    for (int i = 0; i < taille; i ++)
    {
        for (int j = 0; j <= i; j ++)
        {
            int nb_aleat = rand()%2;

            matrice[i][j] = nb_aleat;
            matrice[j][i] = nb_aleat;

            if (i == j)
                matrice[i][i] = 0;//on ne peut pas rester sur un sommet
        }
    }
    
    // Pour s'assurer d'avoir tous les sommets connectés entre eux quitte à avoir un lien en plus

    for (int j = 0; j < (taille - 1); j ++)
    {
        matrice[taille - 1][j] = 1;
    }
    return matrice;
}

void afficher_Matrice(int taille, int **matrice)
{
    printf("  |");
    for (int i = 0; i<taille; i++)
    {
        printf("%d    ",i);
    }
    printf("\n___");
    for (int i = 0; i<taille; i++)
    {
        printf("_____");
    }

	for (int i=0;i<taille;i++)
	{
        printf("\n%d |",i);
		for (int j=0;j<taille;j++)
        {
            if ((0 <= matrice[i][j]) && (matrice[i][j] <= 9))
			    printf("%d    ",matrice[i][j]);
            if ((10 <= matrice[i][j]) && (matrice[i][j] <= 99))
			    printf("%d   ",matrice[i][j]);
            if ((100 <= matrice[i][j]) && (matrice[i][j] <= 999))
			    printf("%d  ",matrice[i][j]);
            if ((1000 <= matrice[i][j]) && (matrice[i][j] <= 9999))
			    printf("%d ",matrice[i][j]);
        }
        printf("|");
	}

    printf("\n___");
    for (int i = 0; i<taille; i++)
    {
        printf("_____");
    }
	printf("\n\n");
}

void liberer_Matrice(int **matrice)
{
    free(matrice);
}

int** Matrice_Des_Poids(int taille, int **matrice, sommet_t *sommet)
{
    int** matriceP = (int**)malloc(sizeof(int*) * taille);

    for (int i = 0; i < taille; i ++)
    {
        matriceP[i] = (int*)malloc(sizeof(int) * taille);
    }

    for (int i = 0; i < taille; i ++)
    {
        for (int j = 0; j < taille; j ++)
        {
          matriceP[i][j] = 0;
        }
    }

    for (int i = 0; i < taille; i ++)
    {
        for (int j = 0; j <= i; j ++)
        {
            if(matrice[i][j] == 1)
            {
                matriceP[i][j] = (int)pow(pow(sommet[i].x - sommet[j].x,2) + pow(sommet[i].y - sommet[j].y,2),0.5);
                matriceP[j][i] = matriceP[i][j];
            }
        }
    }

    return matriceP;
}


int** Creation_Matrice_V2(int taille, int proba_ajout_liaison)
{
    int** matrice = (int**)malloc(sizeof(int*) * taille);

    int tab[taille];
    for (int i = 0; i < taille; i ++)
        tab[i] = i;

    for (int i = 0; i < taille; i ++)
    {
        matrice[i] = (int*)malloc(sizeof(int) * taille);
    }

    //Creation de l'arbre binaire

    for (int i = 0; i < taille; i ++)
    {
        for (int j = 0; j <= i; j ++)
        {
            matrice[i][j] = 0;
            matrice[j][i] = 0;
        }
    }

    int indice_des_feuilles = 1;
    int indice_des_parents = 0;

    if (taille % 2 != 0)
    {
        while (indice_des_feuilles < taille)
        {
            for (int i = 0; i < 2; i++)
            {
                matrice[indice_des_parents][indice_des_feuilles] = 1;
                matrice[indice_des_feuilles][indice_des_parents] = 1;
                indice_des_feuilles += 1;
            }
            indice_des_parents += 1;
        }
    }
    else
    {
        while (indice_des_feuilles < taille-1)
        {
            for (int i = 0; i < 2; i++)
            {
                matrice[tab[indice_des_parents]][tab[indice_des_feuilles]] = 1;
                matrice[tab[indice_des_feuilles]][tab[indice_des_parents]] = 1;
                indice_des_feuilles += 1;
            }
            indice_des_parents += 1;
        }
        matrice[indice_des_parents][indice_des_feuilles] = 1;
        matrice[indice_des_feuilles][indice_des_parents] = 1;
    }

    // Proba d'ajouter une liaison entre 2 sommets

    srand(time(NULL));
    for (int i = 0 ; i < taille ; i ++)
    {
        for (int j = 0 ; j < taille ; j ++)
        {
            if (matrice[i][j] == 0)
            {
                int nb_aleat = rand()%101;

                if( nb_aleat <= proba_ajout_liaison)
                {
                    int nb_aleat2 = rand()%2;
                    matrice[i][j] = nb_aleat2;
                    matrice[j][i] = nb_aleat2;
                }
            }
        }
    }

    for (int i = 0; i < taille ; i ++)
        matrice[i][i] = 0;
    return matrice;
}


int** Matrice_Des_Proba(int taille, int **matrice)
{
    int** matriceProba = (int**)malloc(sizeof(int*) * taille);

    //allocation de la matrice

    for (int i = 0; i < taille; i ++)
    {
        matriceProba[i] = (int*)malloc(sizeof(int) * taille);
    }

    //initialisation de la matrice pour eviter les problemes

    for (int i = 0; i < taille; i ++)
    {
        for (int j = 0; j < taille; j ++)
        {
          matriceProba[i][j] = 0;
        }
    }

    // ajout des probas


    for (int j = 0; j < taille; j ++)
    {
        int somme = 0;
        for (int i = 0; i < taille; i ++)
        {
            somme += matrice[j][i];
        }
        if (somme != 0)
        {
            for (int i = 0; i < taille; i ++)
            {
                if (matrice[j][i] == 1)
                    matriceProba[j][i] = (int)(50/somme); //on met un peu de pheromone de base
            }
        }
        somme = 0;
    }
    return matriceProba;
}

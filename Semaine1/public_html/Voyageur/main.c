#include "Creation_de_matrice.h"
#include "voyageur_Corentin.h"
#include "voyageur_Yvan.h"


#define TAILLE_MATRICE 5


int main()
{
	
	int **matrice = Creation_Matrice_V2(TAILLE_MATRICE, 50);
	printf("Matrice de base: \n");
    afficher_Matrice(TAILLE_MATRICE, matrice);


	//int **matrice = Creation_Matrice(TAILLE_MATRICE);

	//afficher_Matrice(TAILLE_MATRICE, matrice);
	

	printf("coucou\n");
	int nSommet = TAILLE_MATRICE;
	sommet_t *listeSommet = getListeSommet(nSommet, 1000, 800);

	
	int **matricePoids = Matrice_Des_Poids(TAILLE_MATRICE, matrice, listeSommet);
	printf("Matrice de pondération : \n");
	afficher_Matrice(TAILLE_MATRICE, matricePoids);

	
	printf("Voyageur Groupe 36\nPREVOST EMPRIN LANGLOIS\n");
	sdlMedia_t *media = initSDL(1);

	int strPos[] = {20,10};


	loadString("GROUPE 36",strPos, media, 0);
	loadSommetLabel(media, listeSommet, nSommet);
	
	

	usrSommet_t *racine = (usrSommet_t*) malloc(sizeof(usrSommet_t));
	racine->sommetId = 0;
	racine->fils = NULL;  

	loop(media, listeSommet, nSommet, matrice, racine, matricePoids);
	endSommet(listeSommet);
	//free(matrice);	
	freeUsrSommet(racine);
	
	endSDL(media);
	
	

	printf("\nTest plus court chemin\n\n");
	
	// Exemple de matrice
	int ** matri;
	matri = (int**)malloc(TAILLE_MATRICE*sizeof(int*));
	int ma1[] = {0, 100, 0};
	int ma2[] = {100, 0, 0};
	int ma3[] = {0, 0, 0};

	matri[0] = ma1;
	matri[1] = ma2;
	matri[2] = ma3;
	
	int ** matrice_complete = Matrice_complete(matricePoids, TAILLE_MATRICE);
	afficher_Matrice(TAILLE_MATRICE, matrice_complete);
	LibererMatrice(matrice_complete, TAILLE_MATRICE);

	srand(time(NULL));

	chemin_t meilleur_chemin = initialisation_chemin(TAILLE_MATRICE+1);

	printf("\nrecuit_simule donne : \n");
	recuit_simule(matricePoids, TAILLE_MATRICE, 0, &meilleur_chemin);
	afficher_chemin(meilleur_chemin);

	printf("\nmethode gloutonne donne : \n");
	methode_gloutonne(matricePoids, TAILLE_MATRICE, 0, &meilleur_chemin);
	afficher_chemin(meilleur_chemin);

	printf("\nrecuit sur methode gloutonne donne : \n");
	recuit_simule_sur_methode_gloutonne(matricePoids, TAILLE_MATRICE, 0, &meilleur_chemin);
	afficher_chemin(meilleur_chemin);


	return 0;
}

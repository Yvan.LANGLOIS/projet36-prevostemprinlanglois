#ifndef __VOYAGEUR_YVAN_H__
#define __VOYAGEUR_YVAN_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>


typedef struct sdlMedia{
	SDL_Window *window;
	SDL_Renderer *renderer;
	TTF_Font *font;
	int nString;
	SDL_Texture **stringListe;
	SDL_Rect *stringPos;
	int nSommet;
	SDL_Texture **labelListe;
	SDL_Rect *labelPos;
} sdlMedia_t;

typedef struct sommet{
	int x, y;
	int visite;
} sommet_t;

typedef struct usrSommet{
	int sommetId;
	struct usrSommet *fils;
} usrSommet_t;



sdlMedia_t* initSDL(int nString);

void loadColoredString(char *str, SDL_Color color, int *pos, sdlMedia_t *media, int strId);
void loadString(char *str,int *pos, sdlMedia_t *media, int strId);
void loadSommetLabel(sdlMedia_t *media, sommet_t *listeSommet, int nSommet);
void endSDL(sdlMedia_t *media);
void loop(sdlMedia_t *media, sommet_t *listeSommet, int nSommet, int **matriceConnection, usrSommet_t *racine, int **matriceProba);

void draw(sdlMedia_t *media);

sommet_t* getListeSommet(int nSommet, int witdh, int height);
void endSommet(sommet_t *listeSommet);

int** getMatriceConnection(int nSommet);
void endMatriceConnection(int **matriceConnection);

usrSommet_t* update(usrSommet_t *benjamin, int sommetId, sommet_t *listeSommet, int nSommet, usrSommet_t *racine, int *code);
int calcLongueurVoyage(usrSommet_t *racine, sommet_t *listeSommet);
void freeUsrSommet(usrSommet_t *racine);

#endif

#ifndef __VOYAGEUR_BAPTISTE_H__
#define __VOYAGEUR_BAPTISTE_H__

#include "voyageur_Yvan.h"
#include "stdio.h"
#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>



int** Creation_Matrice(int taille);
void liberer_Matrice(int **matrice);
int** Matrice_Des_Poids(int taille, int **matrice, sommet_t *sommet);
int** Creation_Matrice_V2(int taille, int proba_ajout_liaison);
#endif

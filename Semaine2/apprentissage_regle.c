#include "apprentissage_regle.h"

void amelioration_regles(int nombre_iteration, int nombre_perception, int nombre_de_monstre)
{
	int tailleBatch = 10;
	int probaAcceptationQuandMeme = 0;		//en pourcentage

	int iterationMax = 200;
	simulation_t *simu = initSimu(iterationMax, 15, 15, 0.01, nombre_perception, nombre_de_monstre);
	
	//printf("simulation initialisee\n");
	int nRegle = simu->joueur->nRegle-8;

    int taille = nRegle*(nombre_perception+2);
    int listeParamMelangee[taille];
    randomizer(taille, listeParamMelangee); 
    
    regle_t *RRegle = simu->joueur->listeRegle[0];
    printf("regle avant\n");
    for(int w = 0;w<nombre_perception;w++)
    {
        printf("%d ",*(RRegle->perception)[w]);
    }
    printf("\n");

	
	int k;
	int bestScore = 1000;
	for(k = 0; k<nombre_iteration; k++){
        
        if (k>taille) randomizer(taille, listeParamMelangee);

        int indice = listeParamMelangee[k % taille];

        int regleChangee = indice / (nombre_perception+2);
        int paramChangee = indice - (regleChangee*(nombre_perception+2));        
        /*
		int regleChangee = rand()%nRegle;
		
		int paramChangee = rand()%(nombre_perception+2);
		printf("regle changee %d / param changee %d / ", regleChangee, paramChangee);
		*/
	
		simulation_t *simuTmp = copieSimu(simu);
		//printf("\nsimulation copiee\n");
		regle_t *curRegle = simuTmp->joueur->listeRegle[regleChangee];
		/*
		if(paramChangee<nombre_perception) printf("val prec %d / ",*curRegle->perception[paramChangee]);
		else if(paramChangee == nombre_perception) printf("val prec %d / ",*curRegle->action);
		else printf("val prec %d / ",*curRegle->priorite);
		*/
		
		int nPossibilite = 0;
		if(paramChangee<nombre_perception) nPossibilite = 10;
		else if(paramChangee == nombre_perception) nPossibilite = 4;
		else nPossibilite = 3;
		
		int *scoreListe = (int*) malloc(sizeof(int)*nPossibilite);
		
		int i, j;
		for(i = 0; i<nPossibilite; i++){
			int scoreMoyen = 0;
			if(paramChangee<nombre_perception) *curRegle->perception[paramChangee] = (i>=4)?9:i;
			else if(paramChangee == nombre_perception) *curRegle->action = i+1;
			else *curRegle->priorite = i+1;
			for(j = 0; j<tailleBatch; j++){
				simulation_t *simuBatch = copieSimu(simuTmp);
				//printf("\n%d|",j);
				scoreMoyen+=execution(simuBatch);
			}
			scoreListe[i] = scoreMoyen/tailleBatch;
			//printf("/ score(%d) %d / ",i,scoreListe[i]);
		}
		
		int bonneVal = 7, val = scoreListe[0];
		for(i = 0; i<nPossibilite; i++){
			if(/*scoreListe[i]<200 &&*/ scoreListe[i]<val){
				val = scoreListe[i];
				//printf("val %d\n",val);
				bestScore = (val<bestScore)?val:bestScore;
				bonneVal = i;
				//printf("regle %d param %d bonne valeur %d / %d\n",regleChangee, paramChangee, bonneVal, bestScore);
			}else{
				if(rand()%100<probaAcceptationQuandMeme){
				val = scoreListe[i];
				bonneVal = i;
				}
			}
		}
		//printf("%d bonne valeur %d /",paramChangee, bonneVal);

        
		if(paramChangee<nombre_perception)*curRegle->perception[paramChangee] = (bonneVal>=4)?9:bonneVal;
		else if(paramChangee == nombre_perception) *curRegle->action = (bonneVal>=5)?1:bonneVal+1;
		else *curRegle->priorite = (bonneVal>=5)?1:bonneVal+1;


		/*
		if(paramChangee<nombre_perception) printf("val nouv %d / ",*curRegle->perception[paramChangee]);
		else if(paramChangee == nombre_perception) printf("val nouv %d / ",*curRegle->action);
		else printf("val nouv %d / ",*curRegle->priorite);
		printf("\n\n");
		*/
	}
	/*
	for(k = 0; k<nRegle; k++){
		regle_t *curRegle = simu->joueur->listeRegle[k];
		int t;
		for(t = 0; t<nombre_perception; t++){
			printf("%d ",(*curRegle->perception[t]));
		}
		printf("%d %d\n",*curRegle->action, *curRegle->priorite);
	}*/
	FileFromRegle("regle.txt", simu->joueur, 12);
	printf("meilleur score : %d\n",bestScore);
	
	RRegle = simu->joueur->listeRegle[0];
    printf("regle apres\n");
    for(int w = 0;w<nombre_perception;w++)
    {
        printf("%d ",*(RRegle->perception)[w]);
    }
    printf("\n");
}

void randomizer (int taille, int * liste_random)
{
    
    int liste_sommet[taille];

    for (int i = 0; i<taille; i++)
    {
        liste_sommet[i] = i;
    }


    for (int i = 0; i<taille; i++)
    {
        int c = rand()%(taille-i);
        liste_random[i] = liste_sommet[c];

        decale_gauche(liste_sommet, taille, c);
    }
}

void decale_gauche(int * list, int taille, int indice)
{
    for (int k = indice;k<taille-1;k++)
    {
        list[k] = list[k+1];
    }
}


/*
{
    for (int i = 0; i < nombre_iteration; i++)
    {
    
    	printf("ameliorztion regle (debut) iteration %d\n",i);
        //generation carte
        carte_t *map = creationCarte(15,15, 0.01);

        //generation de la simulation
        simulation_t *simul = initSimulationFromCarte(10, map, nombre_perception, nombre_de_monstre);      

        joueur_t *J = simul->joueur;
        int N = J->nRegle * (nombre_perception + 1 + 1);
        int *vecteur_regle = (int *)malloc(N * sizeof(int));
        int indice_vecteur_regle = 0;
        for (int j = 0; j < J->nRegle ; j++)
        {
            for (int q = 0; q < nombre_perception; q++)
            {
                vecteur_regle[indice_vecteur_regle] = *J->listeRegle[j]->perception[q];
                indice_vecteur_regle ++;
            }
            vecteur_regle[indice_vecteur_regle] = *J->listeRegle[j]->action;
            vecteur_regle[indice_vecteur_regle + 1] = *J->listeRegle[j]->priorite;
            indice_vecteur_regle += 2;
        }
        

        // On prend 4 nombres aleatoires
        int *p= (int *)malloc(4 * sizeof(int));
        p[0] = rand()%N;
        p[1] = rand()%N;
        p[2] = rand()%N;
        p[3] = rand()%N;
        printf("p0 : %d, p1 : %d, p2 : %d, p3 : %d\n", p[0], p[1], p[2], p[3]);//ok

        //creation d'un tableau prenant en compte le nombre d'iteration et l'etat du heros
        int *p_suivi = (int *)malloc(4 * 3 * 2 *sizeof(int)); //4 regles, 3 para à changer parmis 3 ou 4, et 2 pour score et valeur de la modif
        //score, valeur regle modif * 12

        printf("thread\n");
        int designation = 0; // 0 si perception modif, 1 si action modif, 2 si prio modif
        for (int j = 0; j < 4; j ++)// boucle des 4 regles à modif
        {
            simulation_t *simul_1 = simul;
            simulation_t *simul_2 = simul;
            simulation_t *simul_3 = simul;
            int position_regle = (int)p[j] / (nombre_perception + 2);
            int position_modif = p[j] % (nombre_perception + 2);
            printf("position_regle : %d, position_modif : %d\n", position_regle, position_modif);
            if ((position_modif >= 0) && (position_modif < nombre_perception)) //modif perception
            {
                designation = 0;
                int nb = *simul_1->joueur->listeRegle[position_regle]->perception[position_modif];
                *simul_1->joueur->listeRegle[position_regle]->perception[position_modif] = (nb + 1)%4;
                *simul_2->joueur->listeRegle[position_regle]->perception[position_modif] = (nb + 2)%4;
                *simul_3->joueur->listeRegle[position_regle]->perception[position_modif] = (nb + 3)%4;
                p_suivi[6 * j + 1] = *simul_1->joueur->listeRegle[position_regle]->perception[position_modif];
                p_suivi[6 * j + 3] = *simul_2->joueur->listeRegle[position_regle]->perception[position_modif];
                p_suivi[6 * j + 5] = *simul_3->joueur->listeRegle[position_regle]->perception[position_modif];
            }
            else if (position_modif == nombre_perception) //modif action, il y en a une qu'elle ne fera pas
            {
                designation = 1;
                int nb = *simul_1->joueur->listeRegle[position_regle]->action;
                *simul_1->joueur->listeRegle[position_regle]->action = (nb+ 1) % 5;
                *simul_2->joueur->listeRegle[position_regle]->action = (nb + 2) % 5;
                *simul_3->joueur->listeRegle[position_regle]->action = (nb + 3) % 5;
                p_suivi[6 * j + 1] = *simul_1->joueur->listeRegle[position_regle]->action;
                p_suivi[6 * j + 3] = *simul_2->joueur->listeRegle[position_regle]->action;
                p_suivi[6 * j + 5] = *simul_3->joueur->listeRegle[position_regle]->action;
            }
            else // modif de  la prio
            {
                designation = 2;
                printf("modif de base : %d\n",*simul_1->joueur->listeRegle[position_regle]->priorite);
                int nb= *simul_1->joueur->listeRegle[position_regle]->priorite;
                printf("nb : %d\n",((nb + 1) % 4) + 1);
                simul_1->joueur->listeRegle[position_regle]->priorite[0] = ((nb + 1) % 4) + 1;
                simul_2->joueur->listeRegle[position_regle]->priorite[0] = ((nb + 2) % 4) + 1;
                simul_3->joueur->listeRegle[position_regle]->priorite[0] = ((nb + 3) % 4) + 1;
                p_suivi[6 * j + 1] = *simul_1->joueur->listeRegle[position_regle]->priorite;
                p_suivi[6 * j + 3] = *simul_2->joueur->listeRegle[position_regle]->priorite;
                p_suivi[6 * j + 5] = *simul_3->joueur->listeRegle[position_regle]->priorite;

                printf("modif : %d %d %d\n",p_suivi[6 * j + 1], p_suivi[6 * j + 3], p_suivi[6 * j + 5]);
            }
        printf("insane %d\n", j);
            for (int v = 0; v < 50 ; v++)
            {
                int a1 = execution(simul_1);
                int a2 = execution(simul_2);
                int a3 = execution(simul_3);
                printf("a1 : %d, a2 : %d, a3 : %d\n",a1,a2,a3);
            }
            //gestion_thread(simul_1, simul_2, simul_3, p_suivi, j);
            
            
        }
        //On recup la meilleur modif
        int meilleure_regle = 0;
        int score_meilleure_regle = 200000;
        int valeur_de_la_modif = -1;

        for (int k = 0; k < 4 ; k++)
        {
            for (int l = 0; l < 3; l++)//pour les 3 modifs
            {
                if (p_suivi[2 * k + 2 * l] < score_meilleure_regle) // on recherche le plus petit score
                {
                    if ((0 <= k) && (k < 6))
                        meilleure_regle = p[0];                    
                    else if ((6 <= k) && (k < 12))
                        meilleure_regle = p[1];
                    else if ((12 <= k) && (k < 18))
                        meilleure_regle = p[2];
                    else
                        meilleure_regle = p[3];
                    score_meilleure_regle = p_suivi[2 * k + 2 * l];
                    valeur_de_la_modif = p_suivi[2 * k + 2 * l + 1];
                }
            }
        }
        modif_regles(J, meilleure_regle, valeur_de_la_modif, designation, nombre_perception);
    }
}

void modif_regles(joueur_t *joueur, int meilleure_regle, int valeur_de_la_modif, int designation, int nombre_perception)
{
    if (designation == 0)// perception de modif
        *joueur->listeRegle[(int)(meilleure_regle / (nombre_perception + 2))]->perception[meilleure_regle % (nombre_perception + 2)] = valeur_de_la_modif;
    else if (designation == 1)
        *joueur->listeRegle[(int)(meilleure_regle / (nombre_perception + 2))]->action = valeur_de_la_modif;
    else
        *joueur->listeRegle[(int)(meilleure_regle / (nombre_perception + 2))]->action = valeur_de_la_modif;
}



void* execution2(void* arg) {
    thread_args_t *thread_args = (thread_args_t*)arg;
    simulation_t* simu = thread_args->simu;

    while (simu->iteration < simu->iterationMax && simu->fini == 0) {
        iter(simu);
    }

    *thread_args->result = calcScore(simu);
    return NULL;
}

void gestion_thread(simulation_t *simul_1, simulation_t *simul_2, simulation_t *simul_3, int p_suivi[], int j)
{
    //for (int i = 0; i < 150; i++){
        thread_args_t thread_args1 = {simul_1, 0};
        thread_args_t thread_args2 = {simul_2, 0};
        thread_args_t thread_args3 = {simul_3, 0};

        pthread_t thread1, thread2, thread3;

        pthread_create(&thread1, NULL, execution2, (void*)thread_args1);
        pthread_create(&thread2, NULL, execution2, (void*)thread_args2);
        pthread_create(&thread3, NULL, execution2, (void*)thread_args3);

        pthread_join(thread1, NULL);
        pthread_join(thread2, NULL);
        pthread_join(thread3, NULL);

        p_suivi[6 * j] += *thread_args1.result;
        p_suivi[6 * j + 2] += *thread_args2.result;
        p_suivi[6 * j + 4] += *thread_args3.result;
        
    }
}
*/
int ** fait_des_groupe(int * liste_random, int taille_groupe, int taille_liste)
{
    int nb_de_groupe = (taille_liste/taille_groupe)+1;
    int k = 0;
    int ** les_groupes = (int**)malloc(nb_de_groupe*sizeof(int*)); 

    for (int i=0;i<nb_de_groupe;i++)
    {
        les_groupes[i] = (int*)malloc(taille_groupe*sizeof(int));

        for (int j=0;j<taille_groupe;j++)
        {
            if(k<taille_liste){
                les_groupes[i][j] = liste_random[k];
                k++;
            }
            else{
                les_groupes[i][j] = rand()%taille_liste;
            }

        }
    }
    return les_groupes;
}

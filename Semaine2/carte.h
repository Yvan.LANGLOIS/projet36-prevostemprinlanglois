#ifndef __CARTE_H__
#define __CARTE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct carte{
	int width, height;
	int **carte;
	int **spriteCarte;
	int **liaison;
	
} carte_t;

void afficher_Matrice(carte_t *map);
void afficher_Liaison(carte_t *map);
void genere_carte(carte_t *map);// matrice de 0 avec 1 autour
void mur_carte(carte_t *map, float pourcentage_mur_ajoute); // ajout aleat de mur dans la carte
void construction_mur(int mur_a_ajouter, carte_t *map); //fonction auxiliaire pour mur_carte
void position_aleatoire_dans_matrice(carte_t *map, int *x, int *y);
int max(int a, int b);
void fileFromCarte(char * fileName, carte_t *carte);

carte_t* carteFromFilename(char *fileName);
void getLiaisonFromCarte(carte_t *carteT);

carte_t* carteFromFilename(char *fileName);

#endif

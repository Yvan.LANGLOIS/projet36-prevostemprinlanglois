#include "graphisme.h"


sprite_t* loadSprite(int *textId, int nTexture, int x, int y, int w, int h){


	sprite_t *sprite = (sprite_t*) malloc(sizeof(sprite_t));
	sprite->textId = textId;
	sprite->nTexture = nTexture;
	sprite->spriteState = 0;
	sprite->x = x;
	sprite->y = y;
	sprite->w = w;
	sprite->h = h;
	
	return sprite;

}


SDL_Texture* loadTextureFromFile(char *fileName, sdlMedia_t *media){
	SDL_Surface *surface = NULL;
	SDL_Texture *texture = NULL;
	
	surface = IMG_Load(fileName);					// Charge l'image dans un objet Surface
		
	texture = SDL_CreateTextureFromSurface(media->renderer, surface);	// on transforme la surface en texture
	SDL_FreeSurface(surface);					// On libère la surface
	
	return texture;
}

sdlMedia_t * initSdl(int winW, int winH, int xOffset, int yOffset, int nTexture, char **fileNameListe, int nSprite, int *nTextureSprite, int **spriteTextureListeId, int *x, int *y, int *w, int *h, int animSpeed, int tileSize){
	
	sdlMedia_t *media = (sdlMedia_t*) malloc(sizeof(sdlMedia_t));
	SDL_Window *win = SDL_CreateWindow("Fenetre", 0, 0, winW, winH, SDL_WINDOW_RESIZABLE);
	
	media->window = win;
	media->renderer = SDL_CreateRenderer(media->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	/* Chargement des textures */
	media->textureListe = (SDL_Texture **) malloc(sizeof(SDL_Texture*)*nTexture);
	int i;
	for(i = 0; i<nTexture; i++){
		media->textureListe[i] = loadTextureFromFile(fileNameListe[i], media);
	}
	
	
	/* Chargement des sprites */
	/*loadSprite(int *textId, int nTexture, int x, int y, int w, int h)*/
	media->nSprite = nSprite;
	media->spriteListe = (sprite_t**) malloc(nSprite*sizeof(sprite_t*));
	for(i = 0; i<nSprite; i++) media->spriteListe[i] = loadSprite(spriteTextureListeId[i], nTextureSprite[i], x[i], y[i], w[i], h[i]);
	
	
	media->xOffset = xOffset;
	media->yOffset = yOffset;
	media->animSpeed = animSpeed;
	media->tileSize = tileSize;
	media->winW = winW;
	media->winH = winH;
	media->state = 0;
	
	return media;


}

void renderMap(sdlMedia_t *media, carte_t *carte){
	int carteW = carte->width, carteH = carte->height, tileSize = media->tileSize;
	int i, j;
	for(i = 0; i<carteW; i++){
		for(j = 0; j<carteH; j++){
			SDL_Rect dest = {media->xOffset+i*tileSize, media->yOffset+j*tileSize, tileSize, tileSize};
			int tileSpriteId = carte->spriteCarte[i][j];
			sprite_t *sprite = media->spriteListe[tileSpriteId];
			//SDL_RenderCopyEx(media->renderer, media->textureListe[media->spriteListe[carte->carte[i][j]]->spriteState], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
			SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
		}
	}
	
}

void renderJoueur(sdlMedia_t *media, joueur_t *joueur){
	int /*carteW = carte->width, carteH = carte->height, */tileSize = media->tileSize;
	sprite_t *sprite = media->spriteListe[0];
	SDL_Rect dest = {media->xOffset+joueur->x*tileSize, media->yOffset+joueur->y*tileSize, tileSize, tileSize};
	SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
}

void renderSortie(sdlMedia_t *media, sortie_t *sortie){
	int /*carteW = carte->width, carteH = carte->height, */tileSize = media->tileSize;
	sprite_t *sprite = media->spriteListe[1];
	SDL_Rect dest = {media->xOffset+sortie->x*tileSize, media->yOffset+sortie->y*tileSize, tileSize, tileSize};
	SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);

}

void renderMonstre(sdlMedia_t *media, monstre_cell_t *monstreListe){

	monstre_cell_t *cour = monstreListe;
	int /*carteW = carte->width, carteH = carte->height, */tileSize = media->tileSize;
	while(cour){
	sprite_t*sprite;

	if(cour->monstre->Mstate == 0)
	{
		sprite = media->spriteListe[2];
	}
	
	if(cour->monstre->Mstate == 1) sprite = media->spriteListe[10];

	SDL_Rect dest = {media->xOffset+cour->monstre->Mx*tileSize, media->yOffset+cour->monstre->My*tileSize, tileSize, tileSize};
	SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
	cour = cour->suiv;
	}
}

void renderCloud(sdlMedia_t *media,carte_t *carte, joueur_t *joueur, int active){
	if(active){
		int w = carte->width, h = carte->height;
		int tileSize = media->tileSize;
		int x = joueur->x, y = joueur->y;
		int i, j;
		for(i = 0; i<w; i++){
			for(j = 0; j<h; j++){
				if( (i!=x || j!=y) && (i!=x || j!=y+1) && (i!=x || j!=y-1) && (i!=x || j!=y+2) && (i!=x || j!=y-2) && (i!=x-1 || j!=y-1) && (i!=x-1 || j!=y+1) && (i!=x+1 || j!=y-1) && (i!=x+1 || j!=y+1) && (i!=x+1 || j!=y) && (i!=x-1 || j!=y) && (i!=x+2 || j!=y) && (i!=x-2 || j!=y) )
				{
					SDL_Rect dest = {media->xOffset+i*tileSize, media->yOffset+j*tileSize, tileSize, tileSize};
					//SDL_RenderCopyEx(media->renderer, media->textureListe[media->spriteListe[carte->carte[i][j]]->spriteState], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
					SDL_RenderCopyEx(media->renderer, media->textureListe[17], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
				}
			}
		}
	}
}

void finManche(simulation_t *simu, SDL_bool *program_on, int *valReturn){
	int score = calcScore(simu);
	printf("fini %d, score %d\n",simu->fini, score);
	if(simu->fini!=0) {
		*valReturn = 1;//(simu->fini==2);
		*program_on = SDL_FALSE;
	}
}

int loop(sdlMedia_t *media, simulation_t *simu){
	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	
	//int mx, my;
	int compteurAnim = 0;
	int valReturn = 0;
	int brouillard = 1;
	int avecMonstre = 1;
	
	
	while(program_on){
	
		if(SDL_PollEvent(&event)){
			switch(event.type){
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
					case SDLK_z:
						if(new_place(simu->joueur, simu->carte, 4)) modif_position_joueur(simu->joueur, 4);
						if(avecMonstre) {
							Update_carte2(simu->carte, simu->monstreListe, *(simu->joueur), *(simu->sortie));
							if(estSurMonstre(simu->joueur, simu->carte, 0)) {
								simu->fini = 1;
								finManche(simu, &program_on, &valReturn);
							} 
						}
						break;
					case SDLK_d:
						if(new_place(simu->joueur, simu->carte, 3)) modif_position_joueur(simu->joueur, 3);
						if(avecMonstre) {
							Update_carte2(simu->carte, simu->monstreListe, *(simu->joueur), *(simu->sortie));
							if(estSurMonstre(simu->joueur, simu->carte, 0)) {
								simu->fini = 1;
								finManche(simu, &program_on, &valReturn);
							}
						
						}
						break;						
					case SDLK_s:
						if(new_place(simu->joueur, simu->carte, 2)) modif_position_joueur(simu->joueur, 2);
						if(avecMonstre) {
							Update_carte2(simu->carte, simu->monstreListe, *(simu->joueur), *(simu->sortie));
							if(estSurMonstre(simu->joueur, simu->carte, 0)) {
								simu->fini = 1;
								finManche(simu, &program_on, &valReturn);
							}
						
						}
						break;
					case SDLK_q:
						if(new_place(simu->joueur, simu->carte, 1)) modif_position_joueur(simu->joueur, 1);
						if(avecMonstre) {
							Update_carte2(simu->carte, simu->monstreListe, *(simu->joueur), *(simu->sortie));
							if(estSurMonstre(simu->joueur, simu->carte, 0)) {
								simu->fini = 1;
								finManche(simu, &program_on, &valReturn);
							}
						
						}
						break;
					case SDLK_SPACE:
						iter(simu);
						//printf("iteration %d / %d\n",simu->iteration, simu->iterationMax);
						if(simu->iteration==simu->iterationMax || simu->fini!=0) {
							finManche(simu, &program_on, &valReturn);
						}
						//deplacement_un_pas_heros(joueur, carte, 12);
						//Update_carte2 (carte, monstreListe, *joueur, sortie);
						//afficher_Matrice(carte);
						break;
					case SDLK_l:
						program_on = SDL_FALSE;
						valReturn = 2;
						break;
					case SDLK_m:
						avecMonstre = (avecMonstre==1)?0:1;
						break;
					case SDLK_o:
						brouillard = (brouillard==1)?0:1;
						break;
					case SDLK_ESCAPE:
						program_on = SDL_FALSE;
						break;
				}
				break;
			case SDL_QUIT:
				program_on = SDL_FALSE;
				break;
			case SDL_MOUSEMOTION:
				/*
				mx = event.motion.x;
				my = event.motion.y;
				*/
				break;
			}
		}
		
		SDL_SetRenderDrawColor(media->renderer, 27, 27, 28, 255);
		SDL_RenderClear(media->renderer);
		renderMap(media, simu->carte);
		renderJoueur(media, simu->joueur);
		renderSortie(media, simu->sortie);
		renderMonstre(media, simu->monstreListe);
		renderCloud(media, simu->carte, simu->joueur, brouillard);
		/*int i;
		for(i = 0; i<media->nSprite; i++){
			
			sprite_t *sprite = media->spriteListe[i];
			SDL_Rect dest = {sprite->x, sprite->y, sprite->w, sprite->h};
			SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
		}
		*/
		SDL_RenderPresent(media->renderer);
		
		SDL_Delay(5);
		compteurAnim = (compteurAnim==media->animSpeed)?0:compteurAnim+1;
		if(compteurAnim==0){
			//avancement de l'animation
			int i;
			for(i = 0; i<media->nSprite; i++){
				sprite_t *sprite = media->spriteListe[i];
				sprite->spriteState = (sprite->spriteState == sprite->nTexture)?0:sprite->spriteState+1;
				
						//program_on = SDL_FALSE;
			}
		}
	}
	return valReturn;
}

void renderSelected(sdlMedia_t *media, carte_t *carte, int mx, int my){
	int tileSize = media->tileSize;
	int carteW = carte->width, carteH = carte->height;
	int xOffset = media->xOffset, yOffset = media->yOffset;
	if(mx<=xOffset+carteW*tileSize && my<=yOffset+carteH*tileSize && mx>=xOffset && my>=yOffset){
		SDL_Rect dest = {(mx/tileSize)*tileSize, (my/tileSize)*tileSize, tileSize, tileSize};
		//SDL_RenderCopyEx(media->renderer, media->textureListe[media->spriteListe[carte->carte[i][j]]->spriteState], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
		SDL_RenderCopyEx(media->renderer, media->textureListe[18], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
	}
}

void changeTile(sdlMedia_t *media, carte_t *carte, int mx, int my){
	int tileSize = media->tileSize;
	int i = (mx-media->xOffset)/tileSize, j = (my-media->yOffset)/tileSize;
	carte->carte[i][j] = (carte->carte[i][j]==0)?1:0;
	
	int tile = carte->carte[i][j];
	int val = 0;
	if(tile==0) val = 3+rand()%6;
	else val = 9;
	carte->spriteCarte[i][j] = val;
	//printf("%d\n",carte->carte[i][j]);
}


void reChargerSpriteCarte(carte_t *carte){
	int i, j;
	for(i = 0; i<carte->width; i++){
		carte->spriteCarte[i] = (int*) malloc(sizeof(int)*carte->height);
		for(j = 0; j<carte->height; j++){
			int tile = carte->carte[i][j];
			int val = 0;
			if(tile==0) {
				val = 3+rand()%10;
				val = (val>8)?3:val;
			}
			else {
				if(i>0 && j>0 && i<carte->width-1 && j<carte->height){
					int tn = carte->carte[i][j-1];	//Nord
					int to = carte->carte[i-1][j];	//Ouest
					int ts = carte->carte[i][j+1];	//Sud
					int te = carte->carte[i+1][j];
					
					if(tn==0 && to==1 && te==1 && ts==1) val = 12;		//LAVE_N
					else if(tn==1 && to==1 && te==1 && ts==0) val=13;		//LAVE_S
					else if(tn==1 && to==1 && te==0 && ts==1) val=14;		//LAVE_E
					else if(tn==1 && to==0 && te==1 && ts==1) val=15;		//LAVE_O
					else if(tn==0 && to==0 && te==1 && ts==1) val=16;		//LAVE_NO
					else if(tn==0 && to==1 && te==0 && ts==1) val=17;		//LAVE_NE
					else if(tn==1 && to==0 && te==1 && ts==0) val=18;		//LAVE_SO
					else if(tn==1 && to==1 && te==0 && ts==0) val=19;		//LAVE_SE
					
					else if(tn==0 && to==1 && te==1 && ts==0) val=20;		//LAVE_NS
					else if(tn==1 && to==0 && te==0 && ts==1) val=21;		//LAVE_OE
					else if(tn==0 && to==0 && te==0 && ts==1) val=22;		//LAVE_NOE
					else if(tn==0 && to==1 && te==0 && ts==0) val=23;		//LAVE_NSE
					else if(tn==0 && to==0 && te==1 && ts==0) val=24;		//LAVE_NSO
					else if(tn==1 && to==0 && te==0 && ts==0) val=25;		//LAVE_SOE
					
					else if(tn==0 && to==0 && te==0 && ts==0) val=26;		//LAVE_C
					
					else val = 9;
				}
				else val = 9;
			}
			
			carte->spriteCarte[i][j] = val;
		}
	}

}

void chargerSpriteCarte(carte_t *carte){
	carte->spriteCarte = (int**) malloc(sizeof(int*)*carte->width);
	int i;
	for(i = 0; i<carte->width; i++) carte->spriteCarte[i] = (int*) malloc(sizeof(int)*carte->height);
	reChargerSpriteCarte(carte);
}



int levelLoop(sdlMedia_t *media, carte_t *carte){
	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	
	int mx = 0, my = 0;
	//int compteurAnim = 0;
	int valReturn = 0;
	
	while(program_on){
	if(SDL_PollEvent(&event)){
			switch(event.type){
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
					
					case SDLK_SPACE:
						fileFromCarte("cartePerso.txt", carte);
						break;
					case SDLK_ESCAPE:
						program_on = SDL_FALSE;
						break;
					case SDLK_l:
						valReturn = 3;
						program_on = SDL_FALSE;
						break;
				}
				break;
			case SDL_QUIT:
				program_on = SDL_FALSE;
				break;
			case SDL_MOUSEBUTTONUP:
				mx = event.motion.x;
				my = event.motion.y;
				changeTile(media, carte, mx, my);
				reChargerSpriteCarte(carte);
				break;
			case SDL_MOUSEMOTION:
				
				mx = event.motion.x;
				my = event.motion.y;
				
				break;
			}
		}
		
		SDL_SetRenderDrawColor(media->renderer, 27, 27, 28, 255);
		SDL_RenderClear(media->renderer);

		renderMap(media, carte);
		renderSelected(media, carte, mx, my);
		SDL_RenderPresent(media->renderer);
		
		
	
	}
	return valReturn;
}

void launch(int nMonstre){
	
   	//srand(time(NULL));
	//srand(1688645152);

	int width = 900, height = 700, xOffset = 0, yOffset = 0, animSpeed = 20, tileSize = 36;

	//int nMonstre = 3;
	int iterationMax = 30;
	
	
	int nTexture = 34;
	char **textureListe = (char**) malloc(sizeof(char*)*nTexture);
	textureListe[0] = "./res/sol3.png";
	textureListe[2] = "./res/sol1.png";
	textureListe[1] = "./res/sol2.png";
	textureListe[3] = "./res/sol4.png";
	textureListe[4] = "./res/lave1.png";
	textureListe[5] = "./res/lave2.png";
	textureListe[6] = "./res/heros1.png";
	textureListe[7] = "./res/heros2.png";
	textureListe[8] = "./res/heros3.png";
	textureListe[9] = "./res/heros4.png";
	textureListe[10] = "./res/dodo1.png";
	textureListe[11] = "./res/dodo2.png";
	textureListe[12] = "./res/dodo3.png";
	textureListe[13] = "./res/dodo4.png";
	textureListe[14] = "./res/dodo5.png";
	textureListe[15] = "./res/dodo6.png";
	textureListe[16] = "./res/sortie.png";
	textureListe[17] = "./res/brouillard.png";
	textureListe[18] = "./res/selected.png";
	textureListe[19] = "./res/LAVE_N.png";
	textureListe[20] = "./res/LAVE_S.png";
	textureListe[21] = "./res/LAVE_E.png";
	textureListe[22] = "./res/LAVE_O.png";
	textureListe[23] = "./res/LAVE_NO.png";
	textureListe[24] = "./res/LAVE_NE.png";
	textureListe[25] = "./res/LAVE_SO.png";
	textureListe[26] = "./res/LAVE_SE.png";
	
	textureListe[27] = "./res/LAVE_NS.png";
	textureListe[28] = "./res/LAVE_OE.png";
	
	textureListe[29] = "./res/LAVE_NOE.png";
	textureListe[30] = "./res/LAVE_NSE.png";
	textureListe[31] = "./res/LAVE_NSO.png";
	textureListe[32] = "./res/LAVE_SOE.png";
	
	textureListe[33] = "./res/LAVE_C.png";
	
		/*
	int nSprite = 10;
	int *nTextureSprite = (int*) malloc(sizeof(int)*nSprite);
	nTextureSprite[0] = 5;		// Heros
	nTextureSprite[1] = 1;		// Sortie
	nTextureSprite[2] = 7;		// Ennemi
	nTextureSprite[3] = 1; 
	nTextureSprite[4] = 1; 
	nTextureSprite[5] = 1; 
	nTextureSprite[6] = 1; 
	nTextureSprite[7] = 1; 
	nTextureSprite[8] = 1; 
	nTextureSprite[9] = 2;
		*/
	
	int nSprite = 27;
	int *nTextureSprite = (int*) malloc(sizeof(int)*nSprite);
	nTextureSprite[0] = 5;		// Heros
	nTextureSprite[1] = 1;		// Sortie
	nTextureSprite[2] = 5;		// Ennemi endormi
	nTextureSprite[3] = 1; 
	nTextureSprite[4] = 1; 
	nTextureSprite[5] = 1; 
	nTextureSprite[6] = 1; 
	nTextureSprite[7] = 1; 
	nTextureSprite[8] = 1; 
	nTextureSprite[9] = 2;
	nTextureSprite[10] = 1; 	//ennemi en chasse
	nTextureSprite[11] = 3;	//reveil
	
	nTextureSprite[12] = 1; 
	nTextureSprite[13] = 1; 
	nTextureSprite[14] = 1; 
	nTextureSprite[15] = 1; 
	nTextureSprite[16] = 1; 
	nTextureSprite[17] = 1; 
	nTextureSprite[18] = 1; 
	nTextureSprite[20] = 1; 
	nTextureSprite[21] = 1; 
	nTextureSprite[22] = 1; 
	nTextureSprite[23] = 1; 
	nTextureSprite[24] = 1; 
	nTextureSprite[25] = 1; 
	nTextureSprite[26] = 1; 
	
	
	int **textureListeId = (int**) malloc(sizeof(int*)*nSprite);
	int i;
	for(i = 0; i<nSprite; i++){
		textureListeId[i] = (int*) malloc(sizeof(int)*nTextureSprite[i]);
	}
	
	textureListeId[0][0] = 6;
	textureListeId[0][1] = 7;
	textureListeId[0][2] = 8;
	textureListeId[0][3] = 9;
	textureListeId[0][4] = 8;
	textureListeId[0][5] = 7;

	textureListeId[1][0] = 16;
	textureListeId[1][1] = 16;
	
	textureListeId[2][0] = 10;
	textureListeId[2][1] = 11;
	textureListeId[2][2] = 11;
	textureListeId[2][3] = 11;
	textureListeId[2][4] = 10;
	textureListeId[2][5] = 10;

	/*
	textureListeId[2][2] = 12;
	textureListeId[2][3] = 13;
	textureListeId[2][4] = 14;
	textureListeId[2][5] = 15;
	textureListeId[2][6] = 14;
	textureListeId[2][7] = 13;
	*/

	textureListeId[3][0] = 0;
	textureListeId[3][1] = 0;
	
	textureListeId[4][0] = 1;
	textureListeId[4][1] = 1;
	
	textureListeId[5][0] = 2;
	textureListeId[5][1] = 2;
	
	textureListeId[6][0] = 3;
	textureListeId[6][1] = 3;
	
	textureListeId[7][0] = 1;
	textureListeId[7][1] = 1;
	
	textureListeId[8][0] = 2;
	textureListeId[8][1] = 2;
	
	textureListeId[9][0] = 4;
	textureListeId[9][1] = 5;
	textureListeId[9][2] = 4;

	textureListeId[10][0] = 14;
	textureListeId[10][1] = 15;

	textureListeId[11][0] = 12;
	textureListeId[11][1] = 12;
	textureListeId[11][2] = 13;
	textureListeId[11][3] = 13;
	
	textureListeId[12][0] = 19; 
	textureListeId[12][1] = 19; 

	textureListeId[13][0] = 20; 
	textureListeId[13][1] = 20; 

	textureListeId[14][0] = 21; 
	textureListeId[14][1] = 21; 

	textureListeId[15][0] = 22; 
	textureListeId[15][1] = 22; 

	textureListeId[16][0] = 23; 
	textureListeId[16][1] = 23; 

	textureListeId[17][0] = 24; 
	textureListeId[17][1] = 24; 

	textureListeId[18][0] = 25; 
	textureListeId[18][1] = 25;
	
	textureListeId[19][0] = 26; 
	textureListeId[19][1] = 26; 

	textureListeId[20][0] = 27; 
	textureListeId[20][1] = 27;
	
	textureListeId[21][0] = 28; 
	textureListeId[21][1] = 28;
	textureListeId[22][0] = 29; 
	textureListeId[22][1] = 29;
	textureListeId[23][0] = 30; 
	textureListeId[23][1] = 30;

	textureListeId[24][0] = 31; 
	textureListeId[24][1] = 31;

	textureListeId[25][0] = 32; 
	textureListeId[25][1] = 32;

	textureListeId[26][0] = 33; 
	textureListeId[26][1] = 33;

	
	int x[] = {0}, y[] = {0}, w[] = {30}, h[] = {40};
	
	// sdlMedia_t * initSdl(int winW, int winH, int xOffset, int yOffset, int nTexture, char **fileNameListe, int nSprite, int *nTextureSprite, int **spriteTextureListeId, int *x, int *y, int *w, int *h, int animSpeed, int tileSize)
	sdlMedia_t *media = initSdl(width, height, xOffset, yOffset, nTexture, textureListe, nSprite, nTextureSprite, textureListeId, x, y, w, h, animSpeed, tileSize);
	carte_t *carte = carteFromFilename("carte2.txt");
	/*
	carte_t *carte = (carte_t*) malloc(sizeof(carte_t));
	carte->width = 15; carte->height = 15;
	genere_carte(carte);
	mur_carte(carte, 0.01);
	*/
	chargerSpriteCarte(carte);
	printf("fin du chargement\n");
	
	/*
	joueur_t *joueur = initJoueur2(12);
	joueur->x = 3,joueur->y = 3;
	
	sortie_t sortie;
	sortie.x = 6; sortie.y = 8;
	
	//void Rajoute_des_monstres(carte_t carte, int nb_monstre, monstre_cell_t ** les_monstres, joueur_t joueur, sortie_t sortie)
	monstre_cell_t *monstreListe = Rajoute_des_monstres(*carte, nMonstre, *joueur, sortie);
	//void Update_carte (carte_t * carte, monstre_cell_t * headp, joueur_t joueur)
	Update_carte(carte, monstreListe, *joueur);
	
	//monstreListe->monstre->Mstate = 1;
	for(i = 0; i<carte->width; i++){
		for(j = 0; j<carte->height; j++){
			printf("%d ", carte->carte[i][j]);
		}
		printf("");
	}
	
	*/
	
	simulation_t *simu = initSimulationFromCarte(iterationMax, carte, 12, nMonstre);
	afficher_Matrice(carte);
	int running = 1;
	while(running){
		running = loop(media, simu);
		if(running==1){
			carte_t *carte = (carte_t*) malloc(sizeof(carte_t));
			carte->width = 15; carte->height = 15;
			genere_carte(carte);
			mur_carte(carte, 0.01);
			
			carte->spriteCarte = (int**) malloc(sizeof(int*)*carte->width);
			chargerSpriteCarte(carte);
			
			simu = initSimulationFromCarte(iterationMax, carte, 12, nMonstre);


		}
		else if(running==2){
			carte_t *carte = (carte_t*) malloc(sizeof(carte_t));
			carte->width = 20; carte->height = 20;
			genere_carte(carte);
			
			
			chargerSpriteCarte(carte);
			running = levelLoop(media,carte);
			if(running==3){
			
				carte_t *carte = carteFromFilename("cartePerso.txt");
				/*
				carte_t *carte = (carte_t*) malloc(sizeof(carte_t));
				carte->width = 15; carte->height = 15;
				genere_carte(carte);
				mur_carte(carte, 0.01);
				*/
				
				chargerSpriteCarte(carte);
				simu = initSimulationFromCarte(iterationMax, carte, 12, nMonstre);

			}
		}
	}
}



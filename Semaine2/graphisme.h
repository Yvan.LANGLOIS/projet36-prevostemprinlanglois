#ifndef __GRAPHISME_H__
#define __GRAPHISME_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>
#include "carte.h"
#include "joueur.h"
//#include "pluscourtchemin.h"
#include "monstre.h"
#include "simulation.h"

typedef struct sprite{
	int nTexture;
	int *textId;
	int spriteState;
	int x, y, w, h;
} sprite_t;

typedef struct sdlMedia{
	SDL_Window *window;
	SDL_Renderer *renderer;
	int nTexture;
	SDL_Texture **textureListe;
	int nSprite;
	sprite_t **spriteListe;
	
	int winW, winH;
	int xOffset, yOffset;
	int tileSize;
	int animSpeed;
	int state;
	
} sdlMedia_t;


void launch();
#endif

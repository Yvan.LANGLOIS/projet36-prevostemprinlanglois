#include "joueur.h"

/*
joueur_t* initJoueur(char *regleFileName)
{	
	char *fileName = "regle.txt";
	
	joueur_t *joueur = (joueur_t*) malloc(sizeof(joueur_t));
	
	int nRegle = 0;	
		
	FILE *f;
	char str[2] = "\0";
	f = fopen(fileName, "r");
	char s[20] = "\0";
	int initialized = 0;
	
	while((str[0] = fgetc(f)) != EOF){
		if(!initialized){
			if(str[0]=='\n'){
				nRegle = atoi(s);
				strcpy(s, "");
				initialized = 1;
				//printf("nRegle %d\n",nRegle);
            		}
			else{
				strcat(s, str);
            		}
        	}

            }
	fclose(f);
	initialized = 0;
	joueur->nRegle = nRegle;
	regle_t *listeRegle = malloc(nRegle*sizeof(regle_t));
	
	int i = 0, j = 0;
	f = fopen(fileName, "r");
	int *perception = (int*) malloc(12*sizeof(int));
	int action = 0, priorite = 0;
	
	while((str[0] = fgetc(f)) != EOF){

		if(str[0]=='\n'){
			if (initialized){
	    			priorite = atoi(s);
	    			printf("j %d p1 %d\n",j, perception[1]);
				listeRegle[j]->perception = &perception;
				listeRegle[j]->action = &action;
				listeRegle[j]->priorite = &priorite;
				strcpy(s, "");
				perception = (int*) malloc(12*sizeof(int));
				i = 0;
				j++;
			}
			else {
			initialized = 1;
			i = 0;
			}
    		}
    		else if(str[0]==' '){
    			if(i<13) perception[i-1] = atoi(s);
    			else if(i==13) action = atoi(s);
    			strcpy(s, "");
    		}
		else{
			strcat(s, str);
			i++;
    		}
	}
	fclose(f);
	joueur->listeRegle = listeRegle;
	
	return joueur;	
	
}*/

joueur_t* initJoueur2(int nombre_perception) {
    char *fileName = "regle.txt";

    joueur_t *joueur = (joueur_t*) malloc(sizeof(joueur_t));

    FILE *file;
    file = fopen(fileName, "r");

    if (file == NULL) {
        printf("Erreur lors de l'ouverture du fichier !\n");
        return joueur;
    }

	fscanf(file,"%d", &joueur->nRegle);
	fscanf(file, "\n");
	//printf("nb regles : %d\n", joueur->nRegle);
	joueur->listeRegle = (regle_t**)malloc(joueur->nRegle * sizeof(regle_t*));
	int num_regle = 0;
	

    do {
        joueur->listeRegle[num_regle] = (regle_t*) malloc(sizeof(regle_t));
        joueur->listeRegle[num_regle]->perception = (int**) malloc(nombre_perception * sizeof(int*));

        for (int i = 0; i < nombre_perception; i++) {
            joueur->listeRegle[num_regle]->perception[i] = (int*) malloc(sizeof(int));
            fscanf(file, "%d", joueur->listeRegle[num_regle]->perception[i]);
        }

        joueur->listeRegle[num_regle]->action = (int*) malloc(sizeof(int));
        fscanf(file, "%d", joueur->listeRegle[num_regle]->action);

        joueur->listeRegle[num_regle]->priorite = (int*) malloc(sizeof(int));
        fscanf(file, "%d", joueur->listeRegle[num_regle]->priorite);

        fscanf(file, "\n");
        num_regle += 1;

    } while (!feof(file) && num_regle < joueur->nRegle);

    fclose(file);

    return joueur;
}

void FileFromRegle(char * fileName, joueur_t * joueur, int nombre_perception)
{
	FILE *file;

	file = fopen(fileName, "w");

	if (file == NULL) // on verifie que l'ouverture du fichier s'est bien passé                                                       
    {
        printf("Erreur lors de l'ouverture du fichier !\n");
        //return 0;
    }
	
	fprintf(file,"%d", joueur->nRegle);
	fprintf(file, "\n");
	//printf("nb regles : %d\n", joueur->nRegle);
	int num_regle = 0;

	do
	{
		for (int i = 0; i < nombre_perception; i++)
		{
			fprintf(file, "%d ",*joueur->listeRegle[num_regle]->perception[i]);
		//	printf(" %d", *joueur->listeRegle[num_regle]->perception[i]);
		}
		//printf("\n");
		fprintf(file, "%d ", *joueur->listeRegle[num_regle]->action);
		fprintf(file, "%d ", *joueur->listeRegle[num_regle]->priorite);
		fprintf(file, "\n");
		//printf("regle num : %d, action : %d\n",num_regle, joueur->nRegle);
		num_regle +=1;

	}while(num_regle < joueur->nRegle);
	

	fclose(file);
}

void deplacement_un_pas_heros(joueur_t *joueur, carte_t *map, int nombre_perception)
{
	int direction_de_deplacement = -1;
	regle_t *alentour_joueur = (regle_t*) malloc(sizeof(regle_t*));
	alentour_joueur->perception = (int**)malloc(sizeof(int*) * nombre_perception);
	detection_perception(joueur, map, alentour_joueur);
	
	//phase de selection de regle qui va permettre une action par le joueur
	int tableau_ressemblance[joueur->nRegle];
	comparaison_avec_regles(joueur, alentour_joueur, nombre_perception, tableau_ressemblance);
	int iteration = 0;
	do
	{
		//cas particulier où aucune des perception est identique à plus de 10 perceptions
		int nb_ressemblance = 0;
		for (int i = 0; i < joueur->nRegle; i++)
		{
			if ( tableau_ressemblance[i] == 1)
				nb_ressemblance += 1;
		}
		if (nb_ressemblance == 0)// si on a pas de ressemblance on choissi une regle aleatoirement
		{
			direction_de_deplacement = 0;
			//direction_de_deplacement = rand()%joueur->nRegle;
		}
		else
		{
			direction_de_deplacement = choix_de_la_regle2(joueur, tableau_ressemblance);
		}
		iteration++;
	}while(!new_place(joueur, map, direction_de_deplacement) && iteration<20);
	if(!new_place(joueur, map, direction_de_deplacement)) direction_de_deplacement = 0;
	modif_position_joueur(joueur, direction_de_deplacement);
}

void detection_perception(joueur_t *joueur, carte_t *map, regle_t *alentour_joueur)
{
	/*
	alentour_joueur->perception[0] = map->carte[joueur->x][joueur->y - 2];
	alentour_joueur->perception[1] = map->carte[joueur->x][joueur->y - 1];
	alentour_joueur->perception[6] = map->carte[joueur->x][joueur->y + 2];
	alentour_joueur->perception[7] = map->carte[joueur->x][joueur->y + 1];

	alentour_joueur->perception[2] = map->carte[joueur->x - 1][joueur->y - 1];
	alentour_joueur->perception[3] = map->carte[joueur->x - 1][joueur->y];
	alentour_joueur->perception[5] = map->carte[joueur->x - 1][joueur->y + 1];
	alentour_joueur->perception[4] = map->carte[joueur->x - 2][joueur->y];

	alentour_joueur->perception[11] = map->carte[joueur->x + 1][joueur->y - 1];
	alentour_joueur->perception[10] = map->carte[joueur->x + 1][joueur->y];
	alentour_joueur->perception[8] = map->carte[joueur->x + 1][joueur->y + 1];
	alentour_joueur->perception[9] = map->carte[joueur->x + 2][joueur->y];
	*/
	alentour_joueur->perception[0] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[0][0] = map->carte[joueur->x-2][joueur->y];
	alentour_joueur->perception[1] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[1][0] = map->carte[joueur->x-1][joueur->y];
	alentour_joueur->perception[2] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[2][0] = map->carte[joueur->x - 1][joueur->y - 1];
	alentour_joueur->perception[3] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[3][0] = map->carte[joueur->x][joueur->y-1];
	
	alentour_joueur->perception[4] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[4][0] = map->carte[joueur->x][joueur->y-2];
	alentour_joueur->perception[5] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[5][0] = map->carte[joueur->x+1][joueur->y-1];
	alentour_joueur->perception[6] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[6][0] = map->carte[joueur->x+2][joueur->y];
	alentour_joueur->perception[7] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[7][0] = map->carte[joueur->x+1][joueur->y];


	alentour_joueur->perception[8] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[8][0] = map->carte[joueur->x + 1][joueur->y + 1];
	alentour_joueur->perception[9] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[9][0] = map->carte[joueur->x][joueur->y+2];
	alentour_joueur->perception[10] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[10][0] = map->carte[joueur->x][joueur->y+1];
	alentour_joueur->perception[11] = (int*) malloc(sizeof(int));
	alentour_joueur->perception[11][0] = map->carte[joueur->x-1][joueur->y+1];
	
}

//renvoie 1 dans le tableau si 10 perceptions identiques entre une regle et un joueur 0 sinon
void comparaison_avec_regles(joueur_t *joueur, regle_t *alentour_joueur, int nombre_perception, int *tableau_ressemblance)
{
	for (int i = 0; i < joueur->nRegle ; i ++)
		tableau_ressemblance[i] = 0;

	for (int j = 0; j < joueur->nRegle ; j++)
	{
		int comparaison  = 0;
		for (int i = 0; i < nombre_perception ; i ++)
		{
			if ((*joueur->listeRegle[j]->perception[i] == *alentour_joueur->perception[i]) || (*joueur->listeRegle[j]->perception[i] == 9))
				comparaison += 1;
		}
		//printf("comparaison : %d\n", comparaison);
		if (comparaison >= 12)
			tableau_ressemblance[j] = 1;
	}
}
void triInsertion(int tableau_valeur[], int tableau_num_regle[], int taille) 
{
    int i, j, key, key_num;

    for (i = 1; i < taille; i++) {
        key = tableau_valeur[i];
        key_num = tableau_num_regle[i];
        j = i - 1;

        // Déplacer les éléments de la liste triée vers la droite jusqu'à ce que la bonne position pour "key" soit trouvée
        while (j >= 0 && tableau_valeur[j] < key) {
            tableau_valeur[j + 1] = tableau_valeur[j];
            tableau_num_regle[j + 1] = tableau_num_regle[j];
            j = j - 1;
        }
        
        // Insérer "key" dans la position correcte
        tableau_valeur[j + 1] = key;
        tableau_num_regle[j + 1] = key_num;
    }
}

//la fonction renvoie l'action à réaliser
int choix_de_la_regle(joueur_t *joueur, int tableau_ressemblance[])
{
	double s = 0;
	int tab[joueur->nRegle];
	int indice_tab = 0;

	for (int i = 0; i < joueur->nRegle; i++)
	{
		if(tableau_ressemblance[i] == 1)
		{
			s += pow(*joueur->listeRegle[i]->priorite, s);
			tab[indice_tab] = i;
			indice_tab ++;
		}
	}

	indice_tab = 0;
	double alpha = (float)rand() / ((float)RAND_MAX + 1);
	double cummul = pow(*joueur->listeRegle[tab[indice_tab]]->priorite, 0.0);

	while(alpha > cummul / s)
	{
		indice_tab ++;
		cummul += pow(*joueur->listeRegle[tab[indice_tab]]->priorite, cummul);
	}
	return *joueur->listeRegle[tab[indice_tab]]->action;
}

int choix_de_la_regle2(joueur_t *joueur, int tableau_ressemblance[])
{
	
	int nRegleValide = 0;
	int sommePriorite = 0;
	int i;

	for(i = 0; i<joueur->nRegle; i++){
	//	printf("%d : %d / %d\n", i, tableau_ressemblance[i], joueur->listeRegle[i].priorite);
		if(tableau_ressemblance[i]==1) {
			nRegleValide++;
			sommePriorite += *joueur->listeRegle[i]->priorite;
			if(*joueur->listeRegle[i]->priorite==4) return *joueur->listeRegle[i]->action;
	//		printf("%d priorite %d\n",i, *joueur->listeRegle[i]->priorite);
		}
	}
	//printf("\nsomme priorite %d, nRegleValide %d\n",sommePriorite, nRegleValide);
	int *listeRegleValide = (int*) malloc(sommePriorite*sizeof(int));
	int k = 0, j = 0;
	for(i = 0; i<joueur->nRegle; i++){
		if(tableau_ressemblance[i]==1) {
			for(j = 0; j<*joueur->listeRegle[i]->priorite; j++){
				//printf("valid %d %d\n",k, i);
				listeRegleValide[k] = i;
			k++;
			}
		}
	}
	/*for(i = 0; i<sommePriorite; i++){
		printf("%d: %d\n",i,listeRegleValide[i]); 
	}*/
	int listeRegleChoisie = rand()%sommePriorite;
	int regleChoisie = listeRegleValide[listeRegleChoisie];
	//printf("regle choisie %d %d\n",regleChoisie, listeRegleChoisie);
	int action = *joueur->listeRegle[regleChoisie]->action;
	//printf("deplacement pas heros\n"); 
	//printf("regleChoisie parmi prio %d | regle choisie : %d | action %d\n\n",listeRegleChoisie, regleChoisie, action);
	return action;
	
	/*
	double s = 0;
	int *tab = (int*) malloc(sizeof(int)*joueur->nRegle);
	int indice_tab = 0;
	for (int i = 0; i < joueur->nRegle; i++)
	{
	printf("(%d) %d | %d\n",i, tableau_ressemblance[i], joueur->listeRegle[i].priorite);
		if(tableau_ressemblance[i] == 1)
		{
			s += pow(joueur->listeRegle[i].priorite, s);
			tab[indice_tab] = i;
			indice_tab ++;
		}
	}

	
	double alpha = (double)rand() / ((double)RAND_MAX + 1);
	double somme = 0;
	for (int i = 0; i < indice_tab ; i++)
		somme += pow(joueur->listeRegle[tab[i]].priorite, s);
	indice_tab = 0;
	double cummul = pow(joueur->listeRegle[tab[indice_tab]].priorite, s);

	while(alpha > cummul / somme)
	{
		indice_tab ++;
		cummul += pow(joueur->listeRegle[tab[indice_tab]].priorite, s);
	}
	
	printf("regle choisie %d\n\n",indice_tab);
	return joueur->listeRegle[tab[indice_tab]].action;
	*/
}

int estSurMonstre(joueur_t *joueur, carte_t *carte, int direction_de_deplacement){
	int i = 0, j = 0;
	if (direction_de_deplacement==1) i = -1;
	else if (direction_de_deplacement==2) j = 1;
	else if (direction_de_deplacement==3) i = 1;
	else if (direction_de_deplacement==4) j = -1;
	return (carte->carte[joueur->x + i][joueur->y+j]==3);
}

int estSurSortie(joueur_t *joueur, sortie_t *sortie){
	return ((joueur->x == sortie->x) && (joueur->y == sortie->y));
}

int new_place(joueur_t *joueur, carte_t *map, int direction_de_deplacement)
{
	int i = 0, j = 0;
	if (direction_de_deplacement == 1)
	{
		i = -1;
	}
	else if (direction_de_deplacement == 2)
	{
		j = 1;
	}
	else if (direction_de_deplacement == 3)
	{
		i = 1;
	}
	else if (direction_de_deplacement == 4)
	{
		j = -1;
	}
	else
	{
		i = 0;
		j = 0;
	}
	/*
	int u, v;
	printf("%d %d : %d / %d %d -> %d\n",joueur->x,joueur->y,map->carte[joueur->x][joueur->y], i, j,map->carte[joueur->x+i][joueur->y+j]); 
	for(u = 0; u<map->width; u++){
		for(v = 0; v<map->height; v++){
			printf("%d ", map->carte[v][u]);
		}printf("\n");
	}
	*/
	if (map->carte[joueur->x+ i][joueur->y + j] == 1) return 0;
	
	return 1;
}

void modif_position_joueur(joueur_t *joueur, int direction_de_deplacement)
{
	int x = 0, y = 0;
	if (direction_de_deplacement == 1)
		x = -1;
	else if (direction_de_deplacement == 2)
		y = 1;
	else if (direction_de_deplacement == 3)
		x = 1;
	else if (direction_de_deplacement == 4)
		y = -1;
	joueur->x =joueur->x + x;
	joueur->y = joueur->y + y;
}


void afficher_liste(int liste[], int taille)
{
	for (int i = 0; i < taille; i++)
	{
		printf("%d\t",liste[i]);
	}
	printf("\n");
}

int deplacement_heros_jusque_sortie(joueur_t *joueur, carte_t *map, int nombre_perception, sortie_t *sortie, int *compteur)
{
	while ( ((joueur->x != sortie->x) && (joueur->y != sortie->y)) && (joueur->etat ==0) )
	{
		printf("1\n");
		deplacement_un_pas_heros(joueur, map, nombre_perception);
		printf("2\n");
		compteur += 1;
		printf("3\n");
	}
	if (joueur->etat ==0)
		return 1;
	return 0;
}



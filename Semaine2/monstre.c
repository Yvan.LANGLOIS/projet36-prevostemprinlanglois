#include "monstre.h"

monstre_t* init_monstre(int num, carte_t carte, joueur_t joueur, sortie_t sortie)
{
    monstre_t *mon = (monstre_t*) malloc(sizeof(monstre_t));
    mon->Mnum = num;
    mon->Mstate = 0;
    //mon.Mtype = ? ;
    //mon.Mmotiv = ? ;

    spawn(mon,carte,joueur,sortie);

    return mon;
}

int num_som_from_pos(carte_t carte, int x, int y)
{
    return x*(carte.width) + y;
}

int posx_from_numsomm(carte_t carte, int sommet)
{
    return sommet / carte.width;
}

int posy_from_numsomm(carte_t carte, int sommet)
{
    return (sommet % carte.width);
}

int loin_de_joueur(joueur_t joueur, int x, int y)
{
    int code = 1;
    int eloignement = 1;

    if(x<=joueur.x + eloignement && x >= joueur.x-eloignement)
    {
        if(y<=joueur.y + eloignement && y >= joueur.y-eloignement)
        {
            code = 0;
        }
    }

    return code;
}

int loin_de_sortie(sortie_t sortie, int x, int y)
{
    int code = 1;
    int eloignement = 1;

    if(x<=sortie.x + eloignement && x >= sortie.x-eloignement)
    {
        if(y<=sortie.y + eloignement && y >= sortie.y-eloignement)
        {
            code = 0;
        }
    }

    return code;
}

void spawn(monstre_t * monstre, carte_t carte, joueur_t joueur, sortie_t sortie)
{
    int x = rand()%(carte.width);
    int y = rand()%(carte.height);

    while (loin_de_joueur(joueur, x, y) == 0 || carte.carte[x][y] != 0 || loin_de_sortie(sortie, x, y)==0)
    {
        x = 2 + rand()%(carte.width-2);
        y = 2 + rand()%(carte.height-2);
    }
    carte.carte[x][y] = 3;
    monstre-> Mx = x;
    monstre-> My = y;
}

//monstre_t init_monstre(int num, carte_t carte, joueur_t joueur, sortie_t sortie)
monstre_cell_t* Rajoute_des_monstres2(carte_t carte, int nb_monstre)
{
	//monstre_cell_t *cour = les_monstres;
	monstre_cell_t *cour = (monstre_cell_t*) malloc(sizeof(monstre_cell_t));
	monstre_cell_t *les_monstres = cour;

    joueur_t jou;
    jou.x = 0;
    jou.y = 0;

    sortie_t sor;
    sor.x = 0;
    sor.y = 0;
	
	int i;
	for(i = 0; i<nb_monstre; i++){
		cour->monstre = init_monstre(i, carte, jou, sor);
		cour->suiv = (monstre_cell_t*) malloc(sizeof(monstre_cell_t));
		cour = cour->suiv;
	}
	cour->monstre = init_monstre(i, carte, jou, sor);
	cour->suiv = NULL;
	return les_monstres;
	/*
    monstre_cell_t ** prec = (monstre_cell_t**)malloc(sizeof(monstre_cell_t*));
    *prec = NULL;
    les_monstres = prec;

    for (int i=0; i<nb_monstre; i++)
    {
        monstre_cell_t * cour = (monstre_cell_t*)malloc(sizeof(monstre_cell_t));
        cour->monstre = init_monstre(i,carte,joueur,sortie);
        cour->suiv = *prec;
        *prec = cour;
        
        prec = &(cour->suiv);
    }
    */
}

monstre_cell_t* Rajoute_des_monstres(carte_t carte, int nb_monstre, joueur_t joueur, sortie_t sortie)
{
	//monstre_cell_t *cour = les_monstres;
	monstre_cell_t *cour = (monstre_cell_t*) malloc(sizeof(monstre_cell_t));
	monstre_cell_t *les_monstres = cour;
	
	int i;
	for(i = 0; i<nb_monstre; i++){
		cour->monstre = init_monstre(i, carte, joueur, sortie);
		cour->suiv = (monstre_cell_t*) malloc(sizeof(monstre_cell_t));
		cour = cour->suiv;
	}
	cour->monstre = init_monstre(i, carte, joueur, sortie);
	cour->suiv = NULL;
	return les_monstres;
	/*
    monstre_cell_t ** prec = (monstre_cell_t**)malloc(sizeof(monstre_cell_t*));
    *prec = NULL;
    les_monstres = prec;

    for (int i=0; i<nb_monstre; i++)
    {
        monstre_cell_t * cour = (monstre_cell_t*)malloc(sizeof(monstre_cell_t));
        cour->monstre = init_monstre(i,carte,joueur,sortie);
        cour->suiv = *prec;
        *prec = cour;
        
        prec = &(cour->suiv);
    }
    */
}
/*
void supprimer_un_monstre(monstre_cell_t ** les_monstres, int num)
{
    monstre_cell_t * cour = *les_monstres;
    while (cour && (cour->monstre).Mnum != num)
    {
        cour = cour->suiv;
    }
}
*/

void liberer_liste_monstre(monstre_cell_t ** les_monstres)
{
    monstre_cell_t * cour = *les_monstres;
    while(cour)
    {
        monstre_cell_t * temp = cour->suiv;
        free(cour);
        cour = temp;
    }
    *les_monstres = NULL;
}

void deplacement_monstre(monstre_t * monstre, carte_t * carte, joueur_t joueur, int* code)
{
    int Msom = num_som_from_pos(*carte, monstre->Mx, monstre->My);
    int Jsom = num_som_from_pos(*carte, joueur.y, joueur.x);
    getLiaisonFromCarte(carte);
    *code = 0;

    int som_suiv = Prochaine_etape(carte->liaison,carte->height*carte->width,Msom,Jsom);

    if (som_suiv == -1) * code = 1;
    else{
    monstre ->Mx = posx_from_numsomm(*carte,som_suiv);
    monstre ->My = posy_from_numsomm(*carte,som_suiv);
    }
}

void Update_carte (carte_t * carte, monstre_cell_t * headp, joueur_t joueur)
{

    monstre_cell_t * cour = headp;
    int i = 0;
    int code_err = 0;
    while(cour)
    {
    	i++;
        monstre_t *mons = cour->monstre;

        if(mons->Mstate == 1)
        {

            carte->carte[mons->Mx][mons->My] = 0;
            deplacement_monstre(mons, carte, joueur, &code_err);
            if (code_err == 1) break;
        }
        else if(mons->Mstate == 0){
        	mons->Mstate = estJoueurDetecte(mons, &joueur);
        }

        carte->carte[mons->Mx][mons->My] = 3;
    
        cour = cour->suiv;
    }
    if (code_err == 1) printf("Vous êtes mort!");
}

int estJoueurDetecte(monstre_t *monstre, joueur_t *joueur){
	int mx = monstre->My, my = monstre->Mx;
	int jx = joueur->y, jy = joueur->x;
	
	/*
	*	(mx==jx && my==jy+1) : case sud
	*	(mx==jx && my==jy-1) : case nord
	*	(mx==(jx-1) && my==jy+1) : case sud ouest
	*	(mx==(jx-1) && my==jy-1) : case nord ouest
	*	(mx==(jx+1) && my==jy+1) : case sud est
	*	(mx==(jx+1) && my==jy-1) : case nord est
	*	(mx==(jx+1) && my==jy) : case est
	*	(mx==(jx-1) && my==jy) : case ouest	
	*/
	
//	return ( (mx==jx && my==jy+1) || (mx==jx && my==jy-1) || (mx==(jx-1) && my==jy+1) || (mx==(jx-1) && my==jy-1) || (mx==(jx+1) && my==jy+1) || (mx==(jx+1) && my==jy-1) || (mx==(jx+1) && my==jy) || (mx==(jx-1) && my==jy) );

	return ( (mx==jx && my==jy+1) || (mx==jx && my==jy-1) || (mx==(jx+1) && my==jy) || (mx==(jx-1) && my==jy) );
	// Renvoie 1 si le joueur est proche, 0 sinon
}

int distance_carre(int Jx, int Jy, int Mox, int Moy)
{
    return (Jx - Mox)*(Jx - Mox) + (Jy - Moy)*(Jy - Moy);
}

void Prochaine_etape2(joueur_t * joueur, monstre_t * monstre, carte_t carte) //entourer la sortie avant ?
{
    int mx = monstre->Mx, my = monstre->My;
	int jx = joueur->x, jy = joueur->y;

    int dist = distance_carre(jx,jy,mx,my);

    //printf("%d : grosse distance\n", dist);
    int nouv_mx = mx;
    int nouv_my = my;

    //printf("ancien my : %d\n", my);
    //printf("ancien mx : %d\n", mx);

    if (carte.carte[mx][my-1]==0 && dist > distance_carre(jx,jy,mx,my-1))
    {
        
        nouv_my = my-1;
        nouv_mx = mx;
        
        /*
        nouv_my = my;
        nouv_mx = mx+1;
        */
        dist = distance_carre(jx,jy,mx,my-1);
        //printf("%d : mieux distance ", dist);
        //printf("avec nouv my-1 %d\n", nouv_my);
    }

    if (carte.carte[mx][my+1]==0 && dist > distance_carre(jx,jy,mx,my+1))
    {
        
        nouv_my = my+1;
        nouv_mx = mx;
        
        /*   
        nouv_my = my;
        nouv_mx = mx-1;
        */
        dist = distance_carre(jx,jy,mx,my+1);
        //printf("%d : mieux distance ", dist);
        //printf("avec nouv my+1 %d\n", nouv_my);
    }

    if (carte.carte[mx+1][my]==0 && dist > distance_carre(jx,jy,mx+1,my))
    {
        
        nouv_mx = mx+1;
        nouv_my = my;
        
        /*
        nouv_my = my+1;
        nouv_mx = mx;
        */
        dist = distance_carre(jx,jy,mx+1,my);
        //printf("%d : mieux distance ", dist);
        //printf("avec nouv mx+1 %d\n", nouv_mx);
    }

    if (carte.carte[mx-1][my]==0 && dist > distance_carre(jx,jy,mx-1,my))
    {
        
        nouv_mx = mx-1;
        nouv_my = my;
        
        /*
        nouv_my = my-1;
        nouv_mx = mx;
        */
        dist = distance_carre(jx,jy,mx-1,my);
        //printf("%d : mieux distance ", dist);
        //printf("avec nouv mx-1 %d\n", nouv_mx);
    }

    //printf("%d : select distance\n", dist);

    monstre->Mx = nouv_mx;
    monstre->My = nouv_my;
}

void proteg_sortie(carte_t * carte, sortie_t sortie)
{
    if(carte->carte[sortie.x-1][sortie.y-1] !=1) carte->carte[sortie.x-1][sortie.y-1] = 8;  //mur que le joueur peut traverser mais pas les monstres
    if(carte->carte[sortie.x][sortie.y-1] !=1) carte->carte[sortie.x][sortie.y-1] = 8;
    if(carte->carte[sortie.x-1][sortie.y] !=1) carte->carte[sortie.x-1][sortie.y] = 8;
    if(carte->carte[sortie.x-1][sortie.y+1] !=1) carte->carte[sortie.x-1][sortie.y+1] = 8;
    if(carte->carte[sortie.x+1][sortie.y-1] !=1) carte->carte[sortie.x+1][sortie.y-1] = 8;
    if(carte->carte[sortie.x+1][sortie.y+1] !=1) carte->carte[sortie.x+1][sortie.y+1] = 8;
    if(carte->carte[sortie.x][sortie.y+1] !=1) carte->carte[sortie.x][sortie.y+1] = 8;
    if(carte->carte[sortie.x+1][sortie.y] !=1) carte->carte[sortie.x+1][sortie.y] = 8;
}

void deproteg_sortie(carte_t * carte, sortie_t sortie)
{
    if(carte->carte[sortie.x-1][sortie.y-1] == 8) carte->carte[sortie.x-1][sortie.y-1] = 0;  //mur que le joueur peut traverser mais pas les monstres
    if(carte->carte[sortie.x][sortie.y-1] == 8) carte->carte[sortie.x][sortie.y-1] = 0;
    if(carte->carte[sortie.x-1][sortie.y] == 8) carte->carte[sortie.x-1][sortie.y] = 0;
    if(carte->carte[sortie.x-1][sortie.y+1] == 8) carte->carte[sortie.x-1][sortie.y+1] = 0;
    if(carte->carte[sortie.x+1][sortie.y-1] == 8) carte->carte[sortie.x+1][sortie.y-1] = 0;
    if(carte->carte[sortie.x+1][sortie.y+1] == 8) carte->carte[sortie.x+1][sortie.y+1] = 0;
    if(carte->carte[sortie.x][sortie.y+1] == 8) carte->carte[sortie.x][sortie.y+1] = 0;
    if(carte->carte[sortie.x+1][sortie.y] == 8) carte->carte[sortie.x+1][sortie.y] = 0;
}

void Update_carte2(carte_t * carte, monstre_cell_t * headp, joueur_t joueur, sortie_t sortie)
{

    monstre_cell_t * cour = headp;
    int i = 0;
    while(cour)
    {
    	i++;
        monstre_t *mons = cour->monstre;

        if(mons->Mstate == 1)
        {
            carte->carte[mons->Mx][mons->My] = 0;
            proteg_sortie(carte, sortie);
            Prochaine_etape2(&joueur, mons, *carte);
            deproteg_sortie(carte, sortie);
        }
        else if(mons->Mstate == 0){
        	mons->Mstate = estJoueurDetecte(mons, &joueur);
        }

        carte->carte[mons->Mx][mons->My] = 3;
    
        cour = cour->suiv;
    }
    
}

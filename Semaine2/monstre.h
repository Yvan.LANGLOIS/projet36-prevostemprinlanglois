#ifndef __MONSTRE_H__
#define __MONSTRE_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>
#include <stdlib.h>

#include "pluscourtchemin.h"
#include "carte.h"
#include "joueur.h"

typedef struct monstre{
    int Mtype;
    int Mstate; // 0 endormi, 1 en chasse, -1 mort
    int Mmotiv; // de 0 a n : nombre de tour ou il poursuit le joueur
    int Mx,My;
    int Mnum;
} monstre_t;

typedef struct monstre_cell{
    monstre_t *monstre;
    struct monstre_cell * suiv;
} monstre_cell_t;

monstre_t* init_monstre(int num, carte_t carte, joueur_t joueur, sortie_t sortie);

int num_som_from_pos(carte_t carte, int x, int y);

int posx_from_numsomm(carte_t carte, int sommet);

int posy_from_numsomm(carte_t carte, int sommet);

int loin_de_joueur(joueur_t joueur, int x, int y);

void spawn(monstre_t *monstre, carte_t carte, joueur_t joueur, sortie_t sortie);

void deplacement_monstre(monstre_t * monstre, carte_t * carte, joueur_t joueur, int* code);

void Update_carte (carte_t * carte, monstre_cell_t * headp, joueur_t joueur);

monstre_cell_t* Rajoute_des_monstres(carte_t carte, int nb_monstre, joueur_t joueur, sortie_t sortie);

void supprimer_un_monstre(monstre_cell_t ** les_monstres, int num);

void liberer_liste_monstre(monstre_cell_t ** les_monstres);

int estJoueurDetecte(monstre_t *monstre, joueur_t *joueur);


//optimisation du temps de calcul :
monstre_cell_t* Rajoute_des_monstres2(carte_t carte, int nb_monstre);

void Prochaine_etape2(joueur_t * joueur, monstre_t * monstre, carte_t carte);

int distance_carre(int Jx, int Jy, int Mox, int Moy);

void proteg_sortie(carte_t * carte, sortie_t sortie);

void deproteg_sortie(carte_t * carte, sortie_t sortie);

void Update_carte2(carte_t * carte, monstre_cell_t * headp, joueur_t joueur, sortie_t sortie);
#endif

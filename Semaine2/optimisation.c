#include "optimisation.h"

void amelioration_regles2(int nombre_iteration, int nombre_perception, int nombre_de_monstre)
{
	int tailleBatch = 150;
	int probaAcceptationQuandMeme = 0;		//en pourcentage

	int iterationMax = 200;
	simulation_t *simu = initSimu(iterationMax, 15, 15, 0.01, nombre_perception, nombre_de_monstre);
	
	//printf("simulation initialisee\n");
	int nRegle = simu->joueur->nRegle;

    int taille = nRegle*(nombre_perception+2);
    int listeParamMelangee[taille];
    randomizer(taille, listeParamMelangee); 
    
    int nb_de_regle_changee = 0;

	
	int k;
	int bestScore = 100000;
	for(k = 0; k<nombre_iteration; k++){
        
        if (k>taille) randomizer(taille, listeParamMelangee);

        int indice = listeParamMelangee[k % taille];

        int regleChangee = indice / (nombre_perception+2);
        int paramChangee = indice - (regleChangee*(nombre_perception+2));        
        /*
		int regleChangee = rand()%nRegle;
		
		int paramChangee = rand()%(nombre_perception+2);
		printf("regle changee %d / param changee %d / ", regleChangee, paramChangee);
		*/
	
		simulation_t *simuTmp = copieSimu(simu);
		//printf("\nsimulation copiee\n");
		regle_t *curRegle = simuTmp->joueur->listeRegle[regleChangee];
		/*
		if(paramChangee<nombre_perception) printf("val prec %d / ",*curRegle->perception[paramChangee]);
		else if(paramChangee == nombre_perception) printf("val prec %d / ",*curRegle->action);
		else printf("val prec %d / ",*curRegle->priorite);
		*/
		int valeurdebase;
        if(paramChangee<nombre_perception) valeurdebase = *curRegle->perception[paramChangee];
			else if(paramChangee == nombre_perception) valeurdebase = *curRegle->action ;
			else valeurdebase = *curRegle->priorite;
		
        int nPossibilite = 0;
		if(paramChangee<nombre_perception) nPossibilite = 4;
		else if(paramChangee == nombre_perception) nPossibilite = 4;
		else nPossibilite = 4;
		
		int *scoreListe = (int*) malloc(sizeof(int)*nPossibilite);
		//////////////////////////////

		pthread_t threads[4];
    	thread_args_t thread_args[4];

		for (int i = 0; i < 4; i++) {
			thread_args[i].tailleBatch = tailleBatch;
			thread_args[i].i = i;
			thread_args[i].valeur = (int *)malloc(sizeof(int));
			*thread_args[i].valeur = scoreListe[i];
			thread_args[i].paraCh = paramChangee;
			thread_args[i].nb_percep = nombre_perception;
			thread_args[i].simuTmp = copieSimu(simuTmp);
			thread_args[i].curRegle = curRegle;

			//pthread_create(&threads[i], NULL, thread_func, &thread_args[i]);
		}

		

		for (int i = 0; i < 4; i++) {
			pthread_join(threads[i], NULL);
		}
		/*
		scoreListe[0] = *thread_args[0].valeur;
		scoreListe[1] = *thread_args[1].valeur;
		scoreListe[2] = *thread_args[2].valeur;
		scoreListe[3] = *thread_args[3].valeur;*/
		printf("valeur : %d\n", scoreListe[0]);
		
		int bonneVal = 7, val = scoreListe[0];
		for(int i = 0; i<nPossibilite; i++){
			if(/*scoreListe[i]<200 &&*/ scoreListe[i]<val){
				val = scoreListe[i];
				//printf("val %d\n",val);
				bestScore = (val<bestScore)?val:bestScore;
				bonneVal = i;
				//printf("regle %d param %d bonne valeur %d / %d\n",regleChangee, paramChangee, bonneVal, bestScore);
			}else{
				if(rand()%100<probaAcceptationQuandMeme){
				val = scoreListe[i];
				bonneVal = i;
				}
			}
		}
		printf("regle %d param %d bonne valeur %d / %d\n",regleChangee, paramChangee, bonneVal, bestScore);

        if(bonneVal!=7)
        {
            if(paramChangee<nombre_perception)
            {
                *curRegle->perception[paramChangee] = bonneVal;
            }
            else if(paramChangee == nombre_perception) 
                *curRegle->action = bonneVal;
            else
                *curRegle->priorite = bonneVal;

            nb_de_regle_changee++;
        }
		else
        {
            if(paramChangee<nombre_perception)
            {
                *curRegle->perception[paramChangee] = valeurdebase;
            }
            else if(paramChangee == nombre_perception) 
                *curRegle->action = valeurdebase;
            else
                *curRegle->priorite = valeurdebase;
        }

	}

	FileFromRegle("regle.txt", simu->joueur, 12);
	printf("meilleur score : %d avec %d regles changees\n",bestScore, nb_de_regle_changee);

}

int main()
{
 	printf("\nVoulez vous lancer un apprentissage ? (y/n)");
 	char res = ' ';
 	scanf("%c", &res);
    
 	if(res!='n'){
    for (int i = 0; i<150; i++){
 	//int tt = time(NULL);
	int tt = 1688585270;
    srand(tt);
	printf("seed %d \n", tt);
    amelioration_regles2(36*14,12, 3);
    }
 	}
 	else launch( 4);
    return 0;

}

void fonction_inter(int tailleBatch, int i, int paraCh, int nb_percep, int *valeur, simulation_t *simuTmp, regle_t *curRegle)
{
	int scoreMoyen = 0;
	if(paraCh<nb_percep) *curRegle->perception[paraCh] = (i>=4)?9:i;
	else if(paraCh == nb_percep) *curRegle->action = i+1;
	else *curRegle->priorite = i+1;

	for(int j = 0; j<tailleBatch; j++){
		simulation_t *simuBatch = copieSimu(simuTmp);
		scoreMoyen+=execution(simuBatch);
	}
	*valeur = scoreMoyen / tailleBatch;
	//printf("valeur : %d\n", *valeur);
}

void* thread_func(void* arg) {
    thread_args_t* thread_args = (thread_args_t*)arg;
	thread_args->valeur = (int *)malloc(sizeof(int));
    int tailleBatch = thread_args->tailleBatch;
    int i = thread_args->i;
    int valeur = *thread_args->valeur;
	int paraCh = thread_args->paraCh;
	int nb_percep = thread_args->nb_percep;
	simulation_t *simuTmp = copieSimu(thread_args->simuTmp);
	regle_t *curRegle = thread_args->curRegle;

    fonction_inter(tailleBatch, i, paraCh, nb_percep, &valeur, simuTmp, curRegle);
	printf("valeur init : %d\n", valeur);
    return NULL;
}


#ifndef __OPTIMISATION_H__
#define __OPTIMISATION_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "apprentissage_regle.h"
#include "graphisme.h"

typedef struct {
    int tailleBatch;
    int i;
    int *valeur;
    int paraCh;
    int nb_percep;
    simulation_t *simuTmp;
    regle_t *curRegle;
} thread_args_t;


void fonction_inter(int tailleBatch, int i, int paraCh, int nb_percep, int *valeur, simulation_t *simuTmp, regle_t *curRegle);
void* thread_func(void* arg);

#endif
#include "pluscourtchemin.h"


/*////////////////////////////////*/
/*/RECHERCHE DU PLUS COURT CHEMIN/*/
/*////////////////////////////////*/

void init_list_voisins(int * list, int taille)  //Principe : liste du sommet k, list [i] =
                                                    // -1 : i n'est pas voisin (de k), 0 : i voisin deja visité, 1 : i voisin pas visité
{
    int i;
    for (i = 0; i<taille; i++)
        list[i] = -1;
}

void liste_voisin_explicite(int A, int N_sommet, int ** MATRICE, int * vois_explicite, int * nb_vois)
{
    int i;
    int k = 0;

    for (i=0;i<N_sommet;i++)
    {
        if (MATRICE[A][i]!=0)
        {
            vois_explicite[k] = i;
            k = k+1;
            *nb_vois = *nb_vois + 1;
        }

    }
}

int Recherche_sommet(int A, int * list, int taille)
{
    int presence = 0;
    for (int i = 0;i<taille;i++)
    {
        if(list[i]==A)
        {
            presence = 1;
            break;
        }
    }

    return presence;
}

chemin_t initialisation_chemin(int taille)
{
    chemin_t ch;
    int * liste = (int*)malloc(taille*sizeof(int));
    init_list_voisins(liste,taille);
    ch.nb_sommet = 0;
    ch.distance = 0; 
    ch.suite = liste;

    return ch;
}


void copier_chemin(chemin_t * source, chemin_t * dest)
{
    //chemin_t ch = initialisation_chemin(taille);
    for (int i =0; i<source->nb_sommet;i++)
    {
        dest->suite[i] = source->suite[i];
    }
    dest->distance = source->distance;
    dest->nb_sommet = source->nb_sommet;

}



void Ajouter_sommet(int A, int num_chemin, chemin_t * ch, int ** MATRICE)
{
    int z = ch[num_chemin].nb_sommet;
    
    ch[num_chemin].suite[z] = A;
    ch[num_chemin].nb_sommet = z + 1;

    //printf("distance avant: %d\n",ch[num_chemin].distance);
    ch[num_chemin].distance = ch[num_chemin].distance + MATRICE[A][ch[num_chemin].suite[z-1]];
    //printf("distance entre %d et %d =",A,ch[num_chemin].suite[z-1]);

    //printf(" %d\n",MATRICE[A][ch[num_chemin].suite[z-1]]);
    //printf("distance totale: %d\n",ch[num_chemin].distance);
}

void creer_chemin_par_voisin(int ** MATRICE, chemin_t * liste_chemin, chemin_t * chemin_suivant, int num_chemin, int taille, int * nb_chemin)
{
    int * voisin_expli = (int*)malloc(sizeof(int)*taille);
    int nb_voisin = 0;
    int sommet = liste_chemin[num_chemin].suite[liste_chemin[num_chemin].nb_sommet-1];
    //printf("sommet concerné = %d\n",sommet);

    init_list_voisins(voisin_expli, taille);
    liste_voisin_explicite(sommet,taille,MATRICE,voisin_expli,&nb_voisin);

    for (int k=0; k<nb_voisin; k++)
    {
        {
            copier_chemin(&(liste_chemin[num_chemin]), &(chemin_suivant[*nb_chemin + k]));
            Ajouter_sommet(voisin_expli[k],*nb_chemin + k,chemin_suivant,MATRICE);

            //afficher_chemin(chemin_suivant[*nb_chemin + k]);
        }
    }
    
    *nb_chemin = *nb_chemin + nb_voisin;

    free(voisin_expli);
}

void liberer_chemin(chemin_t * ch, int taille)
{
    for(int i=0;i<taille;i++)
    {
        free(ch[i].suite);
    }
    free(ch);
}


void Plus_court_chemin(int ** MATRICE, int N_sommet, int A, int B, chemin_t * chemin_le_plus_court, int * code_err)
{
    
    //contient les proposition de chemin
    
    chemin_t * liste_chemin = (chemin_t*)malloc(sizeof(chemin_t)*N_sommet);
    for (int i =0; i<N_sommet;i++)
        liste_chemin[i] = initialisation_chemin(N_sommet);
    

    int compte_chemin = 1; 
    int code = -1; 

    //liste_chemin[0] = initialisation_chemin(N_sommet);
    liste_chemin[0].distance = 0;
    liste_chemin[0].nb_sommet = 1;
    liste_chemin[0].suite[0] = A;

    //liste de sommet deja visite
    int deja_visite[N_sommet];
    init_list_voisins(deja_visite, N_sommet);
    deja_visite[0] = A;

    while(code == -1)
    {
    
        int max_nouv_chemin = 10000;
        chemin_t * chemin_suivant = (chemin_t*)malloc(sizeof(chemin_t)*max_nouv_chemin);

        int compte_voisin = 0;
        
        for (int i =0; i<max_nouv_chemin;i++)
        {
            chemin_suivant[i] = initialisation_chemin(N_sommet);
        }

        /*Creation d'une liste contenant toutes les listes avec les voisins suivant*/
        for (int k=0;k<compte_chemin;k++)
        {
                creer_chemin_par_voisin(MATRICE, liste_chemin, chemin_suivant, k, N_sommet, &compte_voisin);
        }

        int dist_min = 100000000;
        int chemin_retenue = -1;

        for (int k=0;k<compte_voisin;k++)
        {

            int dernier_voisin = chemin_suivant[k].suite[chemin_suivant[k].nb_sommet-1];
            //printf("voisin recherché = %d\n",dernier_voisin);
            if (chemin_suivant[k].distance < dist_min && Recherche_sommet(dernier_voisin,deja_visite,N_sommet)==0)
            {
                dist_min = chemin_suivant[k].distance;
                chemin_retenue = k;
            }
        }

        if(chemin_retenue!=-1)
        {
            copier_chemin(&(chemin_suivant[chemin_retenue]), &(liste_chemin[compte_chemin]));
            //printf("chemin choisi pour être rajouté:\n");
            //afficher_chemin(liste_chemin[compte_chemin]);

            int K = liste_chemin[compte_chemin].suite[liste_chemin[compte_chemin].nb_sommet-1];
            deja_visite[compte_chemin] = K;
            //printf("                                    deja vu %d\n",K);
            
            compte_chemin++;

            //printf("nombre de chemin = %d\n", compte_chemin);

            liberer_chemin(chemin_suivant,compte_voisin);

            for (int k=0;k<compte_chemin;k++)
            {
                int G = liste_chemin[k].suite[liste_chemin[k].nb_sommet-1];
                //printf("dernier sommet d'un chemin: %d\n",G);
                if(G == B)
                {
                    code = k;
                }
            }
            //printf("code = %d\n\n\n", code);
        }
        else
        {
            *code_err = 1;
            printf("L'arrivée et le départ ne sont pas joignables\n");
            break;
        }

    }


    if (*code_err != 1)
    {
        *chemin_le_plus_court = initialisation_chemin(N_sommet);
        
        copier_chemin(&(liste_chemin[code]),chemin_le_plus_court);
    }
    

    liberer_chemin(liste_chemin, compte_chemin);

}

void afficher_chemin(chemin_t PCC)
{
   	printf("nb sommet: %d et distance %d\n chemin = ", PCC.nb_sommet, PCC.distance);
    int i =0;
    for (i=0;i<PCC.nb_sommet;i++)
		printf("%d ",PCC.suite[i]);
        
	
	printf("\n\n"); 
}

void LibererMatrice (int ** MATRICE, int taille)
{
    for (int i = 0; i<taille; i++)
    {
        free(MATRICE[i]);
    }
    free(MATRICE);
}

int Prochaine_etape(int ** MATRICE, int taille, int pos_ar, int pos_pap)
                    //renvoie le sommet suivant (deplacement monstre) en fonction de la position du joueur et des autres monstres
                    // renvoie -1 si il n'y a pas de chemin possible
{
    chemin_t chemin_a_suivre;
    int sommet_suivant = -1;

	chemin_a_suivre = initialisation_chemin(taille);
	int code = 0;

	Plus_court_chemin(MATRICE,taille,pos_ar,pos_pap,&chemin_a_suivre, &code);

	if (code != 1) sommet_suivant = chemin_a_suivre.suite[1];

    //liberer_chemin(&chemin_a_suivre,taille);
    return sommet_suivant;
}





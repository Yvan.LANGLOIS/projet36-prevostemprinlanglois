#include "apprentissage_regle.h"

void amelioration_regles(int nombre_iteration, int nombre_perception, int nombre_de_monstre)
{
    carte_t *map = creationCarte(15,15, 0.1);
    simulation_t *simul = initSimulationFromCarte(100, map, nombre_perception, nombre_de_monstre); 

    joueur_t *J = (joueur_t *)malloc(sizeof(joueur_t));
        memcpy(J, simul->joueur, sizeof(joueur_t));

        J->listeRegle = (regle_t *)malloc(simul->joueur->nRegle * sizeof(regle_t));
        memcpy(J->listeRegle, simul->joueur->listeRegle, simul->joueur->nRegle * sizeof(regle_t));
    for (int i = 0; i < nombre_iteration; i++)
    {
        //printf("iter %d; ", i);
        //generation carte
        

        //generation de la simulation
        

        
        FileFromRegle("regle_modif.txt", J, nombre_perception);


        int N = J->nRegle * (nombre_perception + 1 + 1);
        int *vecteur_regle = (int *)malloc(N * sizeof(int));
        int indice_vecteur_regle = 0;

        for (int j = 0; j < J->nRegle ; j++)
        {
            for (int q = 0; q < nombre_perception; q++)
            {
                vecteur_regle[indice_vecteur_regle] = J->listeRegle[j].perception[q];
                indice_vecteur_regle ++;
            }
            vecteur_regle[indice_vecteur_regle] = J->listeRegle[j].action;
            vecteur_regle[indice_vecteur_regle + 1] = J->listeRegle[j].priorite;
            indice_vecteur_regle += 2;
        }
        

        // On prend 4 nombres aleatoires
        int *p= (int *)malloc(4 * sizeof(int));
        p[0] = rand()%N;
        p[1] = rand()%N;
        p[2] = rand()%N;
        p[3] = rand()%N;

        //creation d'un tableau prenant en compte le nombre d'iteration et l'etat du heros
        int p_suivi[4 * 4 * 5]; //4 regles, 4 para à changer parmis 3 ou 4, et 2 pour score et valeur de la modif
        //score, valeur regle modif * 12
        p_suivi[0] = 0;//pour evieterles erreurs de seg fault

        
        // 0 si perception modif, 1 si action modif, 2 si prio modif
        for (int j = 0; j < 4; j ++)// boucle des 4 regles à modif
        {
            simulation_t *simul_1 = (simulation_t *)malloc(sizeof(simulation_t));
            simulation_t *simul_2 = (simulation_t *)malloc(sizeof(simulation_t));
            simulation_t *simul_3 = (simulation_t *)malloc(sizeof(simulation_t));
            simulation_t *simul_4 = (simulation_t *)malloc(sizeof(simulation_t));

            memcpy(simul_1, simul, sizeof(simulation_t));
            memcpy(simul_2, simul, sizeof(simulation_t));
            memcpy(simul_3, simul, sizeof(simulation_t));
            memcpy(simul_4, simul, sizeof(simulation_t));

            simul_1->joueur = (joueur_t *)malloc(sizeof(joueur_t));
            memcpy(simul_1->joueur, simul->joueur, sizeof(joueur_t));

            simul_2->joueur = (joueur_t *)malloc(sizeof(joueur_t));
            memcpy(simul_2->joueur, simul->joueur, sizeof(joueur_t));

            simul_3->joueur = (joueur_t *)malloc(sizeof(joueur_t));
            memcpy(simul_3->joueur, simul->joueur, sizeof(joueur_t));

            simul_4->joueur = (joueur_t *)malloc(sizeof(joueur_t));
            memcpy(simul_4->joueur, simul->joueur, sizeof(joueur_t));

            simul_1->joueur->listeRegle = (regle_t *)malloc(simul->joueur->nRegle * sizeof(regle_t));
            memcpy(simul_1->joueur->listeRegle, simul->joueur->listeRegle, simul->joueur->nRegle * sizeof(regle_t));

            simul_2->joueur->listeRegle = (regle_t *)malloc(simul->joueur->nRegle * sizeof(regle_t));
            memcpy(simul_2->joueur->listeRegle, simul->joueur->listeRegle, simul->joueur->nRegle * sizeof(regle_t));

            simul_3->joueur->listeRegle = (regle_t *)malloc(simul->joueur->nRegle * sizeof(regle_t));
            memcpy(simul_3->joueur->listeRegle, simul->joueur->listeRegle, simul->joueur->nRegle * sizeof(regle_t));

            simul_4->joueur->listeRegle = (regle_t *)malloc(simul->joueur->nRegle * sizeof(regle_t));
            memcpy(simul_4->joueur->listeRegle, simul->joueur->listeRegle, simul->joueur->nRegle * sizeof(regle_t));

            int position_regle = p[j] / (nombre_perception + 2);
            int position_modif = p[j] % (nombre_perception + 2);
            if ((position_modif >= 0) && (position_modif < nombre_perception)) //modif perception
            {
                int nb = simul_1->joueur->listeRegle[position_regle].perception[position_modif];
                //printf("nb desi : %d\n",nb);
                simul_1->joueur->listeRegle[position_regle].perception = malloc(nombre_perception * sizeof(int));
                simul_2->joueur->listeRegle[position_regle].perception = malloc(nombre_perception * sizeof(int));
                simul_3->joueur->listeRegle[position_regle].perception = malloc(nombre_perception * sizeof(int));
                simul_4->joueur->listeRegle[position_regle].perception = malloc(nombre_perception * sizeof(int));
                if (nb == 0)
                {
                    simul_1->joueur->listeRegle[position_regle].perception[position_modif] = 9;
                    simul_2->joueur->listeRegle[position_regle].perception[position_modif] = 3;
                    simul_3->joueur->listeRegle[position_regle].perception[position_modif] = 2;
                    simul_4->joueur->listeRegle[position_regle].perception[position_modif] = 1;
                }
                else if (nb == 2)
                {
                    simul_1->joueur->listeRegle[position_regle].perception[position_modif] = 0;
                    simul_2->joueur->listeRegle[position_regle].perception[position_modif] = 3;
                    simul_3->joueur->listeRegle[position_regle].perception[position_modif] = 9;
                    simul_4->joueur->listeRegle[position_regle].perception[position_modif] = 1;
                }
                else if (nb == 3)
                {
                    simul_1->joueur->listeRegle[position_regle].perception[position_modif] = 0;
                    simul_2->joueur->listeRegle[position_regle].perception[position_modif] = 2;
                    simul_3->joueur->listeRegle[position_regle].perception[position_modif] = 9;
                    simul_4->joueur->listeRegle[position_regle].perception[position_modif] = 1;
                }
                else if (nb == 1)
                {
                    simul_1->joueur->listeRegle[position_regle].perception[position_modif] = 0;
                    simul_2->joueur->listeRegle[position_regle].perception[position_modif] = 3;
                    simul_3->joueur->listeRegle[position_regle].perception[position_modif] = 2;
                    simul_4->joueur->listeRegle[position_regle].perception[position_modif] = 9;
                }
                else 
                {
                    simul_1->joueur->listeRegle[position_regle].perception[position_modif] = 0;
                    simul_2->joueur->listeRegle[position_regle].perception[position_modif] = 3;
                    simul_3->joueur->listeRegle[position_regle].perception[position_modif] = 2;
                    simul_4->joueur->listeRegle[position_regle].perception[position_modif] = 1;
                }
                p_suivi[8 * j + 1] = simul_1->joueur->listeRegle[position_regle].perception[position_modif];
                p_suivi[8 * j + 3] = simul_2->joueur->listeRegle[position_regle].perception[position_modif];
                p_suivi[8 * j + 5] = simul_3->joueur->listeRegle[position_regle].perception[position_modif];
                p_suivi[8 * j + 7] = simul_4->joueur->listeRegle[position_regle].perception[position_modif];
                //printf("prio modif percep : %d, %d, %d\n",p_suivi[6 * j + 1],p_suivi[6 * j + 3],p_suivi[6 * j + 5]);
            }
            else if (position_modif == nombre_perception) //modif action, il y en a une qu'elle ne fera pas
            {
                int nb = simul_1->joueur->listeRegle[position_regle].action;
                simul_1->joueur->listeRegle[position_regle].action = (nb + 1) % 5;
                simul_2->joueur->listeRegle[position_regle].action = (nb + 2) % 5;
                simul_3->joueur->listeRegle[position_regle].action = (nb + 3) % 5;
                simul_4->joueur->listeRegle[position_regle].action = (nb + 4) % 5;
                p_suivi[8 * j + 1] = simul_1->joueur->listeRegle[position_regle].action;
                p_suivi[8 * j + 3] = simul_2->joueur->listeRegle[position_regle].action;
                p_suivi[8 * j + 5] = simul_3->joueur->listeRegle[position_regle].action;
                p_suivi[8 * j + 7] = simul_3->joueur->listeRegle[position_regle].action;
                //printf("prio modif action : %d, %d, %d, %d\n",p_suivi[6 * j + 1],p_suivi[6 * j + 3],p_suivi[6 * j + 5], p_suivi[6 * j + 7]);
            }
            else // modif de  la prio
            {
                int nb = simul_1->joueur->listeRegle[position_regle].priorite;
                //printf("prio : %d\n", nb);
                //printf("position regle : %d\n",position_regle);
                simul_1->joueur->listeRegle[position_regle].priorite = ((nb + 1) % 4) + 1;// + 1 car il n'y a pas de prio 0
                simul_2->joueur->listeRegle[position_regle].priorite = ((nb + 2) % 4) + 1;
                simul_3->joueur->listeRegle[position_regle].priorite = ((nb + 3) % 4) + 1;
                p_suivi[8 * j + 1] = simul_1->joueur->listeRegle[position_regle].priorite;
                p_suivi[8 * j + 3] = simul_2->joueur->listeRegle[position_regle].priorite;
                p_suivi[8 * j + 5] = simul_3->joueur->listeRegle[position_regle].priorite;
                p_suivi[8 * j + 7] = simul_4->joueur->listeRegle[position_regle].priorite;
                //printf("prio modif prio : %d, %d, %d, %d\n",p_suivi[6 * j + 1],p_suivi[6 * j + 3],p_suivi[6 * j + 5], p_suivi[6 * j + 5]);
            }
            for (int v = 0; v < 3 ; v++)
            {
                printf("v : %d\n", v);
                int a1 = execution(simul_1);
                int a2 = execution(simul_2);
                int a3 = execution(simul_3);
                int a4 = execution(simul_4);
                p_suivi[8 * j] += a1;
                p_suivi[8 * j + 2] += a2;
                p_suivi[8 * j + 4] += a3;
                p_suivi[8 * j + 6] += a4;
                //printf("a1 : %d, a2 : %d, a3 : %d\n", a1,a2,a3);
            }
            
            //gestion_thread(simul_1, simul_2, simul_3, p_suivi, j);    
        }
        ////printf(" designation : %d; ",designation);

        //On recup la meilleur modif
        int meilleure_regle = 0;
        int score_meilleure_regle = 200000;
        int valeur_de_la_modif = -1;
        //afficher_liste(p_suivi, 4 * 3 * 5);
        for (int k = 0; k < 4 ; k++)
        {
            for (int l = 0; l < 4; l++)//pour les 4 modifs
            {
                //printf("nb : %d\n", 6 * k + 2 * l);
                if (p_suivi[8 * k + 2 * l] < score_meilleure_regle) // on recherche le plus petit score
                {
                    if (k == 0)
                        meilleure_regle = p[0];                    
                    else if (k == 1)
                        meilleure_regle = p[1];
                    else if (k == 2)
                        meilleure_regle = p[2];
                    else
                        meilleure_regle = p[3];
                    score_meilleure_regle = p_suivi[8 * k + 2 * l];
                    valeur_de_la_modif = p_suivi[8 * k + 2 * l + 1];
                }
            }
        }
        //printf("meilleure regle : %d, valeur modif : %d\n",meilleure_regle, valeur_de_la_modif);
        modif_regles(J, meilleure_regle, valeur_de_la_modif, nombre_perception);
    }
}

void modif_regles(joueur_t *joueur, int meilleure_regle, int valeur_de_la_modif, int nombre_perception)
{
    int position_regle = meilleure_regle / (nombre_perception + 2);
    int position_modif = meilleure_regle % (nombre_perception + 2);
    
    if (position_modif == nombre_perception)// perception de modif
    {
        //printf("regle : %d\n",meilleure_regle);
        //printf("action : %d;  ",joueur->listeRegle[(int)(meilleure_regle / (nombre_perception + 2))].action);
        joueur->listeRegle[position_regle].action = valeur_de_la_modif;
        //printf("%d\n",joueur->listeRegle[position_regle].action);
    }
    else if (position_modif == nombre_perception + 1)
    {
        joueur->listeRegle[position_regle].priorite = valeur_de_la_modif;
    }
    else
         joueur->listeRegle[position_regle].perception[position_modif] = valeur_de_la_modif;
}



void* execution2(void* arg) {
    thread_args_t* thread_args = (thread_args_t*)arg;
    simulation_t* simu = thread_args->simu;
    int* result = thread_args->result;

    while (simu->iteration < simu->iterationMax && simu->fini == 0) {
        iter(simu);
    }

    *result = calcScore(simu);
    return NULL;
}

void gestion_thread(simulation_t *simul_1, simulation_t *simul_2, simulation_t *simul_3, int p_suivi[], int j)
{
    for (int i = 0; i < 150; i++)
    {
        thread_args_t thread_args1 = {simul_1, 0};
        thread_args_t thread_args2 = {simul_2, 0};
        thread_args_t thread_args3 = {simul_3, 0};

        pthread_t thread1, thread2, thread3;

        pthread_create(&thread1, NULL, execution2, (void*)&thread_args1);
        pthread_create(&thread2, NULL, execution2, (void*)&thread_args2);
        pthread_create(&thread3, NULL, execution2, (void*)&thread_args3);

        pthread_join(thread1, NULL);
        pthread_join(thread2, NULL);
        pthread_join(thread3, NULL);

        p_suivi[6 * j] += *thread_args1.result;
        p_suivi[6 * j + 2] += *thread_args2.result;
        p_suivi[6 * j + 4] += *thread_args3.result;
    }
}

void randomizer (int taille, int * liste_random)
{
    
    int liste_sommet[taille];

    for (int i = 0; i<taille; i++)
    {
        liste_sommet[i] = i;
    }


    for (int i = 0; i<taille; i++)
    {
        int c = rand()%(taille-i);
        liste_random[i] = liste_sommet[c];

        decale_gauche(liste_sommet, taille, c);
    }
}

void decale_gauche(int * list, int taille, int indice)
{
    for (int k = indice;k<taille-1;k++)
    {
        list[k] = list[k+1];
    }
}

int ** fait_des_groupe(int * liste_random, int taille_groupe, int taille_liste)
{
    int nb_de_groupe = (taille_liste/taille_groupe)+1;
    int k = 0;
    int ** les_groupes = (int**)malloc(nb_de_groupe*sizeof(int*)); 

    for (int i=0;i<nb_de_groupe;i++)
    {
        les_groupes[i] = (int*)malloc(taille_groupe*sizeof(int));

        for (int j=0;j<taille_groupe;j++)
        {
            if(k<taille_liste){
                les_groupes[i][j] = liste_random[k];
                k++;
            }
            else{
                les_groupes[i][j] = rand()%taille_liste;
            }

        }
    }
    return les_groupes;
}
#ifndef __APPRENTISSAGE_H__
#define __APPRENTISSAGE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

//#include "carte.h"
#include "joueur.h"
#include "simulation.h"

typedef struct {
    simulation_t* simu;
    int *result;
} thread_args_t;

void amelioration_regles(int nombre_iteration, int nombre_perception, int nombre_de_monstre);
void modif_regles(joueur_t *joueur, int meilleure_regle, int valeur_de_la_modif, int nombre_perception);
void* execution2(void* arg);
void gestion_thread(simulation_t *simul_1, simulation_t *simul_2, simulation_t *simul_3, int p_suivi[], int j);

void decale_gauche(int * list, int taille, int indice);
void randomizer (int taille, int * liste_random);
int ** fait_des_groupe(int * liste_random, int taille_groupe, int taille_liste);

#endif

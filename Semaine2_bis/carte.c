#include "carte.h"
//#include "monstre.h"

void afficher_Matrice(carte_t *map)
{
    printf("    |");
    for (int i = 0; i< map->height; i++)
    {
        if ((0 <= i) && (i <= 9))
			printf("%d    ",i);
        if ((10 <= i) && (i <= 99))
			printf("%d   ",i);
        if ((100 <= i) && (i <= 999))
			printf("%d  ",i);
        if ((1000 <= i) && (i <= 9999))
			printf("%d ",i);
    }
    printf("\n_____");
    for (int i = 0; i< map->height; i++)
    {
        printf("_____");
    }

	for (int i=0;i<map-> width;i++)
	{
        if ((0 <= i) && (i <= 9))
			printf("\n%d   |",i);
        if ((10 <= i) && (i <= 99))
			printf("\n%d  |",i);
        if ((100 <= i) && (i <= 999))
			printf("\n%d |",i);
        if ((1000 <= i) && (i <= 9999))
			printf("\n%d|",i);

		for (int j=0;j< map->height;j++)
        {
            if ((0 <= map->carte[i][j]) && (map->carte[i][j] <= 9))
			    printf("%d    ",map->carte[i][j]);
            if ((10 <= map->carte[i][j]) && (map->carte[i][j] <= 99))
			    printf("%d   ",map->carte[i][j]);
            if ((100 <= map->carte[i][j]) && (map->carte[i][j] <= 999))
			    printf("%d  ",map->carte[i][j]);
            if ((1000 <= map->carte[i][j]) && (map->carte[i][j] <= 9999))
			    printf("%d ",map->carte[i][j]);
        }
        printf("|");
	}

    printf("\n___");
    for (int i = 0; i< map->height; i++)
    {
        printf("_____");
    }
    printf("__");
	printf("\n\n");
}

void genere_carte(carte_t *map)
{
    map->carte = (int**)malloc(sizeof(int*) * map->width);

    for (int x = 0; x < map->width; x ++)
    {
        map->carte[x] = (int*)malloc(sizeof(int) * map->height);
    }

    for (int x = 0; x < map->width; x ++)
    {
        for (int y = 0; y < map->height; y ++)
        {
            if ((x == 0) || (y == 0))
                map->carte[x][y] = 1;
            else if((x == map->width - 1) || (y == map->height - 1))
                map->carte[x][y] = 1;
            else 
                map->carte[x][y] = 0;
        }
    }
    // ajout de la deuxieme ligne de 1
    for (int x = 0; x < map->width; x ++)
    {
        map->carte[x][1] = 1;
        map->carte[x][map->height - 2] = 1;
    }
    for (int y = 0; y < map->height; y ++)
    {
        map->carte[1][y] = 1;
        map->carte[map->width - 2][y] = 1;
    }
}

void mur_carte(carte_t *map, float pourcentage_mur_ajoute)
{
    int nombre_de_mur = (int)(map->width * map->height * pourcentage_mur_ajoute); // On construit 20% de mur sur la map
    //printf("nombre_de_mur = %d\n", nombre_de_mur);
    int nombre_de_mur_construit = 0; // Constante pour voir le nombre de murs que l'on a déjà construit

    while ( nombre_de_mur_construit <= nombre_de_mur)
    {
        //printf("nombre_de_mur_construit = %d\n", nombre_de_mur_construit);
        int mur_a_ajouter = 1 + (int)( max(map->height, map->width) * (double)rand() / RAND_MAX);
        //printf(" mur_a_ajouter = %d\n",mur_a_ajouter);
        
        construction_mur(mur_a_ajouter, map);
        nombre_de_mur_construit += mur_a_ajouter;
    }
}

void construction_mur(int mur_a_ajouter, carte_t *map)
{
	int x = 0, y = 0;
	position_aleatoire_dans_matrice(map, &x, &y);

    for (int i = 0; i < mur_a_ajouter; i++)
    {
        int direction = rand()%4; //choix de la direction

        if (direction == 0)// On va vers le Nord
        {
            if (map->carte[x - 1][y] == 0) // Il n'y a pas de mur on peut construire
            {
                x -= 1;
                map->carte[x][y] = 1;
            }
            else if ((x - 1) == 1) // On est sur la bordure de la map donc on recule de 1 et on recommence
            {
                //x += 1;
                i -= 1;
            }
			else //On est au milieu de la carte mais il y a déjà un mur, dans ce cas on passe au dessus du mur et on recommence
			{
				x -= 1;
				i -= 1;
			}
        }

		if (direction == 1)// On va vers l'Est
        {
            if (map->carte[x][y + 1] == 0) // Il n'y a pas de mur on peut construire
            {
                y += 1;
                map->carte[x][y] = 1;
            }
            else if ((y + 1) == (map->height) - 2) // On est sur la bordure de la map donc on recule de 1 et on recommence
            {
                //y -= 1;
                i -= 1;
            }
			else //On est au milieu de la carte mais il y a déjà un mur, dans ce cas on passe au dessus du mur et on recommence
			{
				y += 1;
				i -= 1;
			}
        }

		if (direction == 2)// On va vers le Sud
        {
            if (map->carte[x + 1][y] == 0) // Il n'y a pas de mur on peut construire
            {
                x += 1;
                map->carte[x][y] = 1;
            }
            else if (x + 1 == map->width - 2) // On est sur la bordure de la map donc on recule de 1 et on recommence
            {
                //x -= 1;
                i -= 1;
            }
			else //On est au milieu de la carte mais il y a déjà un mur, dans ce cas on passe au dessus du mur et on recommence
			{
				x += 1;
				i -= 1;
			}
        }

		if (direction == 3)// On va vers l'Ouest
        {
            if (map->carte[x][y - 1] == 0) // Il n'y a pas de mur on peut construire
            {
                y -= 1;
                map->carte[x][y] = 1;
            }
            else if ((y - 1) == 1) // On est sur la bordure de la map donc on recule de 1 et on recommence
            {
                //y += 1;
                i -= 1;
            }
			else //On est au milieu de la carte mais il y a déjà un mur, dans ce cas on passe au dessus du mur et on recommence
			{
				y -= 1;
				i -= 1;
			}
        }
    }
}

void position_aleatoire_dans_matrice(carte_t *map, int *x, int *y)
{
    *x = rand()%map->width;
    //printf("x = %d\t", *x);
    *y = rand()%map->height;
    //printf("y = %d\n", *y);

    while (map->carte[*x][*y] == 1)
    {
        //printf("1\n");
        *x = rand()%map->width;
        //printf("x = %d\t", *x);
        *y = rand()%map->height;
        //printf("y = %d\n", *y);
    }
}

int max(int a, int b)
{
	if (a >= b)
		return a;
	return b;
}



carte_t* carteFromFilename(char *fileName){

	int **carte;

	FILE *f;
	char str[2] = "\0";
	f = fopen(fileName, "r");
	char s[20] = "\0";
	int initialized = 0;
	
	int width = 0, height = 0;
	int i = 0;
	int j = 0;
	while((str[0] = fgetc(f)) != EOF){
		if(!initialized){
			if(str[0]==' '){
				width = atoi(s);
				strcpy(s, "");
        		}
			else if(str[0]=='\n'){
				height = atoi(s);
				strcpy(s, "");
				initialized = 1;
				printf("taille %d*%d\n",width,height);
            		}
            		
			else{
				strcat(s, str);
            		}
        	}

            }
	fclose(f);
	initialized = 0;
	
	carte = (int**) malloc(height*sizeof(int*));
	f = fopen(fileName, "r");
	while((str[0] = fgetc(f)) != EOF){
		if(!initialized){
			if(str[0]=='\n'){
				initialized = 1;
				
            }
        }
		else if(str[0]!=' ' && str[0]!='\n') {
			if(j==width) {
				j=0;
				i++;
			}
			if(j==0){
				carte[i] = (int*) malloc(width*sizeof(int));
			}
			carte[i][j] = atoi(str);
			j++;
		}
	}
	fclose(f);
	printf("\n\n\n");
        /*
	for(i = 0; i<height; i++){
		for(j=0; j<width; j++){
			printf("%d ",carte[i][j]);
		}
		printf("\n");
	}
	*/
	carte_t *carteT = (carte_t*) malloc(sizeof(carte_t));
	carteT->carte = carte;
	carteT->width = width;
	carteT->height = height;
	carteT->liaison = NULL;
	
	getLiaisonFromCarte(carteT);
	return carteT;
    }
    
    
void getLiaisonFromCarte(carte_t *carteT)
{
	
	int **carte = carteT->carte;
	int width = carteT->width;
	int height = carteT->height;
	
	int n = width*height;
	
	int **liaison = (int**) malloc(n*sizeof(int*));	
	int i, j;

	/*
	for(i = 0; i<n; i++){
		liaison[i] = (int*) malloc(n*sizeof(int));
		for(j = 0; j<=i; j++){
			liaison[i][j] = 0;
			liaison[j][i] = 0;
		}		
	}
	*/
	for(i = 0; i<n; i++){
		liaison[i] = (int*) malloc(n*sizeof(int));
		for(j = 0; j<n; j++){
			liaison[i][j] = 0;

			int sX = i%width, sY = (i-(i%width))/height;
			int dX = j%width, dY = (j-(j%width))/height;
			
			int src = carte[sY][sX];
			int dst = carte[dY][dX];
			
			if(((sX==dX) && (sY==(dY+1))) || ((sX==dX) && (sY==(dY-1)))){
				if(src == 0 && dst == 0) liaison[i][j] = 1;
			} 
			else if(((sX==(dX+1)) && (sY==dY)) ||  ((sX==(dX-1)) && (sY==dY))){
				if(src == 0 && dst == 0) liaison[i][j] = 1;
			}
		}
	}
	
	/*
	for (i = 0; i<height;i++)
	{
		for (j=0;j<width;j++)
		{
			if(carte[i][j] == 0)
			{
				if(i<height-1 && carte[i+1][j] == 0)
				{
					int som1 = num_som_from_pos(*carteT, i, j);
					int som2 = num_som_from_pos(*carteT, i+1, j);

					liaison[som1][som2] = 1;
					liaison[som2][som1] = 1;
				}

				if(j<width-1 && carte[i][j+1] == 0)
				{
					int som1 = num_som_from_pos(*carteT, i, j+1);
					int som2 = num_som_from_pos(*carteT, i, j);

					liaison[som1][som2] = 1;
					liaison[som2][som1] = 1;
				}
			}
		}
	}
	
	*/
	/*
	printf("\n\n");
	
	for(i = 0; i<width; i++){
		for(j=0; j<height; j++){
			printf("%d ",(j+i*height));
			if(i+j*height <10) printf(" ");
		}
		printf("\n");
	}
	
	printf("\n\n   |");
	for(i = 0; i<n; i++) printf("%d ",i);
	
	printf("\n================================\n");
	
	for(i = 0; i<n; i++){
		for(j = 0; j<n; j++){
			if(j==0){
				if(i<10)printf("%d  |",i);
				else printf("%d |",i);
			}
			printf("%d ",liaison[i][j]);
		}
		printf("\n");
	}*/
	/*if(carteT->liaison != NULL){
		for(i=0; i<n; i++){
			free(carteT->liaison[i]);
		}
		free(carteT->liaison);
	}*/
	carteT->liaison = liaison;
}

void afficher_Liaison(carte_t *map)
{
    printf("    |");
    for (int i = 0; i< (map->height)*(map->width); i++)
    {
        if ((0 <= i) && (i <= 9))
			printf("%d    ",i);
        if ((10 <= i) && (i <= 99))
			printf("%d   ",i);
        if ((100 <= i) && (i <= 999))
			printf("%d  ",i);
        if ((1000 <= i) && (i <= 9999))
			printf("%d ",i);
    }
    printf("\n_____");
    for (int i = 0; i< (map->height)*(map->width); i++)
    {
        printf("_____");
    }

	for (int i=0;i<(map-> width)*(map->height);i++)
	{
        if ((0 <= i) && (i <= 9))
			printf("\n%d   |",i);
        if ((10 <= i) && (i <= 99))
			printf("\n%d  |",i);
        if ((100 <= i) && (i <= 999))
			printf("\n%d |",i);
        if ((1000 <= i) && (i <= 9999))
			printf("\n%d|",i);

		for (int j=0;j< (map->height)*(map->width);j++)
        {
            if ((0 <= map->liaison[i][j]) && (map->liaison[i][j] <= 9))
			    printf("%d    ",map->liaison[i][j]);
            if ((10 <= map->liaison[i][j]) && (map->liaison[i][j] <= 99))
			    printf("%d   ",map->liaison[i][j]);
            if ((100 <= map->liaison[i][j]) && (map->liaison[i][j] <= 999))
			    printf("%d  ",map->liaison[i][j]);
            if ((1000 <= map->liaison[i][j]) && (map->liaison[i][j] <= 9999))
			    printf("%d ",map->liaison[i][j]);
        }
        printf("|");
	}
}
   


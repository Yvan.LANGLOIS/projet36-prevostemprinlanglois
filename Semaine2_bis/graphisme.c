#include "graphisme.h"


sprite_t* loadSprite(int *textId, int nTexture, int x, int y, int w, int h){


	sprite_t *sprite = (sprite_t*) malloc(sizeof(sprite_t));
	sprite->textId = textId;
	sprite->nTexture = nTexture;
	sprite->spriteState = 0;
	sprite->x = x;
	sprite->y = y;
	sprite->w = w;
	sprite->h = h;
	
	return sprite;

}


SDL_Texture* loadTextureFromFile(char *fileName, sdlMedia_t *media){
	SDL_Surface *surface = NULL;
	SDL_Texture *texture = NULL;
	
	surface = IMG_Load(fileName);					// Charge l'image dans un objet Surface
		
	texture = SDL_CreateTextureFromSurface(media->renderer, surface);	// on transforme la surface en texture
	SDL_FreeSurface(surface);					// On libère la surface
	
	return texture;
}

sdlMedia_t * initSdl(int winW, int winH, int xOffset, int yOffset, int nTexture, char **fileNameListe, int nSprite, int *nTextureSprite, int **spriteTextureListeId, int *x, int *y, int *w, int *h, int animSpeed, int tileSize){
	
	sdlMedia_t *media = (sdlMedia_t*) malloc(sizeof(sdlMedia_t));
	SDL_Window *win = SDL_CreateWindow("Fenetre", 0, 0, winW, winH, SDL_WINDOW_RESIZABLE);
	
	media->window = win;
	media->renderer = SDL_CreateRenderer(media->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	/* Chargement des textures */
	media->textureListe = (SDL_Texture **) malloc(sizeof(SDL_Texture*)*nTexture);
	int i;
	for(i = 0; i<nTexture; i++){
		media->textureListe[i] = loadTextureFromFile(fileNameListe[i], media);
	}
	
	
	/* Chargement des sprites */
	/*loadSprite(int *textId, int nTexture, int x, int y, int w, int h)*/
	media->nSprite = nSprite;
	media->spriteListe = (sprite_t**) malloc(nSprite*sizeof(sprite_t*));
	for(i = 0; i<nSprite; i++) media->spriteListe[i] = loadSprite(spriteTextureListeId[i], nTextureSprite[i], x[i], y[i], w[i], h[i]);
	
	
	media->xOffset = xOffset;
	media->yOffset = yOffset;
	media->animSpeed = animSpeed;
	media->tileSize = tileSize;
	media->winW = winW;
	media->winH = winH;
	media->state = 0;
	
	return media;


}

void renderMap(sdlMedia_t *media, carte_t *carte){
	int carteW = carte->width, carteH = carte->height, tileSize = media->tileSize;
	int i, j;
	for(i = 0; i<carteW; i++){
		for(j = 0; j<carteH; j++){
			SDL_Rect dest = {media->xOffset+i*tileSize, media->yOffset+j*tileSize, tileSize, tileSize};
			
			int tileSpriteId = carte->spriteCarte[j][i];
			sprite_t *sprite = media->spriteListe[tileSpriteId];

			//SDL_RenderCopyEx(media->renderer, media->textureListe[media->spriteListe[carte->carte[i][j]]->spriteState], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
			SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
		}
	}
	
}

void renderJoueur(sdlMedia_t *media, joueur_t *joueur){
	int /*carteW = carte->width, carteH = carte->height, */tileSize = media->tileSize;
	sprite_t *sprite = media->spriteListe[0];
	SDL_Rect dest = {media->xOffset+joueur->x*tileSize, media->yOffset+joueur->y*tileSize, tileSize, tileSize};
	SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
}

void renderSortie(sdlMedia_t *media, sortie_t sortie){
	int /*carteW = carte->width, carteH = carte->height, */tileSize = media->tileSize;
	sprite_t *sprite = media->spriteListe[1];
	SDL_Rect dest = {media->xOffset+sortie.x*tileSize, media->yOffset+sortie.y*tileSize, tileSize, tileSize};
	SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);

}

void renderMonstre(sdlMedia_t *media, monstre_cell_t *monstreListe){

	monstre_cell_t *cour = monstreListe;
	int /*carteW = carte->width, carteH = carte->height, */tileSize = media->tileSize;
	while(cour){
	sprite_t *sprite = media->spriteListe[2];
	SDL_Rect dest = {media->xOffset+cour->monstre->My*tileSize, media->yOffset+cour->monstre->Mx*tileSize, tileSize, tileSize};
	SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
	cour = cour->suiv;
	}
}

void loop(sdlMedia_t *media, simulation_t *simu){
	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	
	//int mx, my;
	int compteurAnim = 0;
	
	while(program_on){
	
		if(SDL_PollEvent(&event)){
			switch(event.type){
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
					case SDLK_z:
						//if(new_place(joueur, carte, 4)) modif_position_joueur(joueur, 4);
						break;
					case SDLK_d:
						//if(new_place(joueur, carte, 3)) modif_position_joueur(joueur, 3);
						break;						
					case SDLK_s:
						//if(new_place(joueur, carte, 2)) modif_position_joueur(joueur, 2);
						break;
					case SDLK_q:
						//if(new_place(joueur, carte, 1)) modif_position_joueur(joueur, 1);
						break;
					case SDLK_SPACE:
						iter(simu);
						//printf("iteration\n");
						if(simu->iteration==simu->iterationMax || simu->fini!=0) {
							int score = calcScore(simu);
							printf("fini %d, score %d\n",simu->fini, score);
						}
						//deplacement_un_pas_heros(joueur, carte, 12);
						//Update_carte2 (carte, monstreListe, *joueur, sortie);
						//afficher_Matrice(carte);
						break;
					case SDLK_ESCAPE:
						program_on = SDL_FALSE;
						break;
				}
				break;
			case SDL_QUIT:
				program_on = SDL_FALSE;
				break;
			case SDL_MOUSEMOTION:
				/*
				mx = event.motion.x;
				my = event.motion.y;
				*/
				break;
			}
		}
		
		SDL_SetRenderDrawColor(media->renderer, 27, 27, 28, 255);
		SDL_RenderClear(media->renderer);
		renderMap(media, simu->carte);
		renderJoueur(media, simu->joueur);
		//renderSortie(media, simu->sortie);
		renderMonstre(media, simu->monstreListe);
		/*int i;
		for(i = 0; i<media->nSprite; i++){
			
			sprite_t *sprite = media->spriteListe[i];
			SDL_Rect dest = {sprite->x, sprite->y, sprite->w, sprite->h};
			SDL_RenderCopyEx(media->renderer, media->textureListe[sprite->textId[sprite->spriteState]], NULL, &dest, 0, NULL, SDL_FLIP_NONE);
		}
		*/
		SDL_RenderPresent(media->renderer);
		
		SDL_Delay(5);
		compteurAnim = (compteurAnim==media->animSpeed)?0:compteurAnim+1;
		if(compteurAnim==0){
			//avancement de l'animation
			int i;
			for(i = 0; i<media->nSprite; i++){
				sprite_t *sprite = media->spriteListe[i];
				sprite->spriteState = (sprite->spriteState == sprite->nTexture)?0:sprite->spriteState+1;
				
						//program_on = SDL_FALSE;
			}
		}
	}
	
}


void launchh(){
	
   	srand(time(NULL));

	int width = 900, height = 700, xOffset = 100, yOffset = 150, animSpeed = 10, tileSize = 36;
	int nMonstre = 1;
	
	
	int nTexture = 17;
	char **textureListe = (char**) malloc(sizeof(char*)*nTexture);
	textureListe[0] = "./res/sol1.png";
	textureListe[1] = "./res/sol2.png";
	textureListe[2] = "./res/sol3.png";
	textureListe[3] = "./res/sol4.png";
	textureListe[4] = "./res/lave1.png";
	textureListe[5] = "./res/lave2.png";
	textureListe[6] = "./res/heros1.png";
	textureListe[7] = "./res/heros2.png";
	textureListe[8] = "./res/heros3.png";
	textureListe[9] = "./res/heros4.png";
	textureListe[10] = "./res/dodo1.png";
	textureListe[11] = "./res/dodo2.png";
	textureListe[12] = "./res/dodo3.png";
	textureListe[13] = "./res/dodo4.png";
	textureListe[14] = "./res/dodo5.png";
	textureListe[15] = "./res/dodo6.png";
	textureListe[16] = "./res/sortie.png";
	
	int nSprite = 10;
	int *nTextureSprite = (int*) malloc(sizeof(int)*nSprite);
	nTextureSprite[0] = 5;		// Heros
	nTextureSprite[1] = 1;		// Sortie
	nTextureSprite[2] = 7;		// Ennemi
	nTextureSprite[3] = 1; 
	nTextureSprite[4] = 1; 
	nTextureSprite[5] = 1; 
	nTextureSprite[6] = 1; 
	nTextureSprite[7] = 1; 
	nTextureSprite[8] = 1; 
	nTextureSprite[9] = 2;
	
	
	int **textureListeId = (int**) malloc(sizeof(int*)*nSprite);
	int i;
	for(i = 0; i<nSprite; i++){
		textureListeId[i] = (int*) malloc(sizeof(int)*nTextureSprite[i]);
	}
	
	textureListeId[0][0] = 6;
	textureListeId[0][1] = 7;
	textureListeId[0][2] = 8;
	textureListeId[0][3] = 9;
	textureListeId[0][4] = 8;
	textureListeId[0][5] = 7;

	textureListeId[1][0] = 16;
	textureListeId[1][1] = 16;
	
	textureListeId[2][0] = 10;
	textureListeId[2][1] = 11;
	textureListeId[2][2] = 12;
	textureListeId[2][3] = 13;
	textureListeId[2][4] = 14;
	textureListeId[2][5] = 15;
	textureListeId[2][6] = 14;
	textureListeId[2][7] = 13;
	
	textureListeId[3][0] = 0;
	textureListeId[3][1] = 0;
	
	textureListeId[4][0] = 1;
	textureListeId[4][1] = 1;
	
	textureListeId[5][0] = 2;
	textureListeId[5][1] = 2;
	
	textureListeId[6][0] = 3;
	textureListeId[6][1] = 3;
	
	textureListeId[7][0] = 1;
	textureListeId[7][1] = 1;
	
	textureListeId[8][0] = 3;
	textureListeId[8][1] = 3;
	
	textureListeId[9][0] = 4;
	textureListeId[9][1] = 5;
	textureListeId[9][2] = 4;
	
	int x[] = {0}, y[] = {0}, w[] = {30}, h[] = {40};
	
	// sdlMedia_t * initSdl(int winW, int winH, int xOffset, int yOffset, int nTexture, char **fileNameListe, int nSprite, int *nTextureSprite, int **spriteTextureListeId, int *x, int *y, int *w, int *h, int animSpeed, int tileSize)
	sdlMedia_t *media = initSdl(width, height, xOffset, yOffset, nTexture, textureListe, nSprite, nTextureSprite, textureListeId, x, y, w, h, animSpeed, tileSize);
	//carte_t *carte = carteFromFilename("carte2.txt");
	carte_t *carte = (carte_t*) malloc(sizeof(carte_t));
	carte->width = 15; carte->height = 15;
	genere_carte(carte);
	mur_carte(carte, 0.01);
	carte->spriteCarte = (int**) malloc(sizeof(int*)*carte->width);
	int j;
	for(i = 0; i<carte->width; i++){
		carte->spriteCarte[i] = (int*) malloc(sizeof(int)*carte->height);
		for(j = 0; j<carte->height; j++){
			int tile = carte->carte[i][j];
			int val = 0;
			if(tile==0) val = 3+rand()%6;
			else val = 9;
			carte->spriteCarte[i][j] = val;
		}
	}
	
	
	/*
	joueur_t *joueur = initJoueur2(12);
	joueur->x = 3,joueur->y = 3;
	
	sortie_t sortie;
	sortie.x = 6; sortie.y = 8;
	
	//void Rajoute_des_monstres(carte_t carte, int nb_monstre, monstre_cell_t ** les_monstres, joueur_t joueur, sortie_t sortie)
	monstre_cell_t *monstreListe = Rajoute_des_monstres(*carte, nMonstre, *joueur, sortie);
	//void Update_carte (carte_t * carte, monstre_cell_t * headp, joueur_t joueur)
	Update_carte(carte, monstreListe, *joueur);
	
	//monstreListe->monstre->Mstate = 1;
	for(i = 0; i<carte->width; i++){
		for(j = 0; j<carte->height; j++){
			printf("%d ", carte->carte[i][j]);
		}
		printf("\n");
	}
	
	*/
	
	simulation_t *simu = initSimulationFromCarte(10, carte, 12, nMonstre);
	
	loop(media, simu);
}



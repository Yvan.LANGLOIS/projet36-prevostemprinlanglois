#include "joueur.h"

joueur_t* initJoueur1(int nombre_perception)
{	
char *fileName = "regle.txt";
	
	joueur_t *joueur = (joueur_t*) malloc(sizeof(joueur_t));

	FILE *file;

	file = fopen(fileName, "r");

	if (file == NULL) // on verifie que l'ouverture du fichier s'est bien passé                                                       
    {
        printf("Erreur lors de l'ouverture du fichier !\n");
        return joueur;
    }

	fscanf(file,"%d", &joueur->nRegle);
	fscanf(file, "\n");
	//printf("nb regles : %d\n", joueur->nRegle);
	joueur->listeRegle = malloc(joueur->nRegle * sizeof(regle_t));
	int num_regle = 0;

	do
	{
		joueur->listeRegle[num_regle].perception = (int*) malloc(12*sizeof(int));
		for (int i = 0; i < nombre_perception; i++)
		{
			fscanf(file, "%d",&joueur->listeRegle[num_regle].perception[i]);
		}
		fscanf(file, "%d", &joueur->listeRegle[num_regle].action);
		fscanf(file, "%d", &joueur->listeRegle[num_regle].priorite);
		fscanf(file, "\n");
		//printf("regle num : %d, action : %d\n",num_regle, joueur->listeRegle[num_regle].action);
		num_regle +=1;

	}while(!feof(file) && num_regle < joueur->nRegle);
	fclose(file);
	//printf("regle num : %d, action : %d\n",num_regle, joueur->listeRegle[0].action);
	return joueur;	
	
}

joueur_t* initJoueur2(int nombre_perception)
{
	char *fileName = "regle_modif.txt";
	
	joueur_t *joueur = (joueur_t*) malloc(sizeof(joueur_t));

	FILE *file;

	file = fopen(fileName, "r");

	if (file == NULL) // on verifie que l'ouverture du fichier s'est bien passé                                                       
    {
        printf("Erreur lors de l'ouverture du fichier !\n");
        return joueur;
    }

	fscanf(file,"%d", &joueur->nRegle);
	fscanf(file, "\n");
	//printf("nb regles : %d\n", joueur->nRegle);
	joueur->listeRegle = malloc(joueur->nRegle * sizeof(regle_t));
	int num_regle = 0;

	do
	{
		joueur->listeRegle[num_regle].perception = (int*) malloc(12*sizeof(int));
		for (int i = 0; i < nombre_perception; i++)
		{
			fscanf(file, "%d",&joueur->listeRegle[num_regle].perception[i]);
		}
		fscanf(file, "%d", &joueur->listeRegle[num_regle].action);
		fscanf(file, "%d", &joueur->listeRegle[num_regle].priorite);
		fscanf(file, "\n");
		//printf("regle num : %d, action : %d\n",num_regle, joueur->listeRegle[num_regle].action);
		num_regle +=1;

	}while(!feof(file) && num_regle < joueur->nRegle);
	fclose(file);
	//printf("regle num : %d, action : %d\n",num_regle, joueur->listeRegle[0].action);
	return joueur;
}

void deplacement_un_pas_heros(joueur_t *joueur, carte_t *map, int nombre_perception)
{
	int direction_de_deplacement = -1;
	regle_t alentour_joueur;
	alentour_joueur.perception = (int*)malloc(sizeof(int) * nombre_perception);
	detection_perception(joueur, map, &alentour_joueur);
	
	//phase de selection de regle qui va permettre une action par le joueur
	int tableau_ressemblance[joueur->nRegle];
	comparaison_avec_regles(joueur, &alentour_joueur, nombre_perception, tableau_ressemblance);
	int ietrM = 0;
	do
	{
		//cas particulier où aucune des perception est identique à plus de 10 perceptions
		int nb_ressemblance = 0;
		for (int i = 0; i < joueur->nRegle; i++)
		{
			if ( tableau_ressemblance[i] == 1)
				nb_ressemblance += 1;
		}
		if (nb_ressemblance == 0)// si on a pas de ressemblance on choissi une regle aleatoirement
		{
			direction_de_deplacement = rand()%joueur->nRegle;
			//printf("direction dep 0 : %d\n", direction_de_deplacement);
		}
		else
		{
			direction_de_deplacement = choix_de_la_regle2(joueur, tableau_ressemblance);
			//printf("direction dep 1 : %d\n", direction_de_deplacement);
		}
		ietrM ++;
		
	}while(!new_place(joueur, map, direction_de_deplacement) && ietrM < 30);
	if (ietrM == 30)
	{
		while(!new_place(joueur, map, direction_de_deplacement))
		{
			direction_de_deplacement = (direction_de_deplacement + 1) %5 + 1;
		}
	}
	printf("direction : %d\n",direction_de_deplacement);
	modif_position_joueur(joueur, direction_de_deplacement);
}

void detection_perception(joueur_t *joueur, carte_t *map, regle_t *alentour_joueur)
{
	
	alentour_joueur->perception[0] = map->carte[joueur->x][joueur->y - 2];
	alentour_joueur->perception[1] = map->carte[joueur->x][joueur->y - 1];
	alentour_joueur->perception[6] = map->carte[joueur->x][joueur->y + 2];
	alentour_joueur->perception[7] = map->carte[joueur->x][joueur->y + 1];

	alentour_joueur->perception[2] = map->carte[joueur->x - 1][joueur->y - 1];
	alentour_joueur->perception[3] = map->carte[joueur->x - 1][joueur->y];
	alentour_joueur->perception[5] = map->carte[joueur->x - 1][joueur->y + 1];
	alentour_joueur->perception[4] = map->carte[joueur->x - 2][joueur->y];

	alentour_joueur->perception[11] = map->carte[joueur->x + 1][joueur->y - 1];
	alentour_joueur->perception[10] = map->carte[joueur->x + 1][joueur->y];
	alentour_joueur->perception[8] = map->carte[joueur->x + 1][joueur->y + 1];
	alentour_joueur->perception[9] = map->carte[joueur->x + 2][joueur->y];
	/*
	alentour_joueur->perception[0] = map->carte[joueur->y][joueur->x-2];
	alentour_joueur->perception[1] = map->carte[joueur->y][joueur->x-1];
	alentour_joueur->perception[2] = map->carte[joueur->y - 1][joueur->x - 1];
	alentour_joueur->perception[3] = map->carte[joueur->y-1][joueur->x];
	
	alentour_joueur->perception[4] = map->carte[joueur->y-2][joueur->x];
	alentour_joueur->perception[5] = map->carte[joueur->y - 1][joueur->x + 1];
	alentour_joueur->perception[6] = map->carte[joueur->y][joueur->x+2];
	alentour_joueur->perception[7] = map->carte[joueur->y][joueur->x+1];


	alentour_joueur->perception[8] = map->carte[joueur->y + 1][joueur->x + 1];
	alentour_joueur->perception[9] = map->carte[joueur->y+2][joueur->x];
	alentour_joueur->perception[10] = map->carte[joueur->y+1][joueur->x];
	alentour_joueur->perception[11] = map->carte[joueur->y+1][joueur->x - 1];
	*/
}

//renvoie 1 dans le tableau si 10 perceptions identiques entre une regle et un joueur 0 sinon
void comparaison_avec_regles(joueur_t *joueur, regle_t *alentour_joueur, int nombre_perception, int *tableau_ressemblance)
{
	for (int i = 0; i < joueur->nRegle ; i ++)
		tableau_ressemblance[i] = 0;

	for (int j = 0; j < joueur->nRegle ; j++)
	{
		int comparaison  = 0;
		for (int i = 0; i < nombre_perception ; i ++)
		{
			if ((joueur->listeRegle[j].perception[i] == alentour_joueur->perception[i]) || (joueur->listeRegle[j].perception[i] == 9))
				comparaison += 1;
		}
		//printf("comparaison : %d\n", comparaison);
		if (comparaison >= 12)
			tableau_ressemblance[j] = 1;
	}
}

void triInsertion(int tableau_valeur[], int tableau_num_regle[], int taille) 
{
    int i, j, key, key_num;

    for (i = 1; i < taille; i++) {
        key = tableau_valeur[i];
        key_num = tableau_num_regle[i];
        j = i - 1;

        // Déplacer les éléments de la liste triée vers la droite jusqu'à ce que la bonne position pour "key" soit trouvée
        while (j >= 0 && tableau_valeur[j] < key) {
            tableau_valeur[j + 1] = tableau_valeur[j];
            tableau_num_regle[j + 1] = tableau_num_regle[j];
            j = j - 1;
        }
        
        // Insérer "key" dans la position correcte
        tableau_valeur[j + 1] = key;
        tableau_num_regle[j + 1] = key_num;
    }
}

//la fonction renvoie l'action à réaliser
int choix_de_la_regle(joueur_t *joueur, int tableau_ressemblance[])
{
	double s = 0;
	int tab[joueur->nRegle];
	int indice_tab = 0;

	for (int i = 0; i < joueur->nRegle; i++)
	{
		if(tableau_ressemblance[i] == 1)
		{
			s += pow(joueur->listeRegle[i].priorite, s);
			tab[indice_tab] = i;
			indice_tab ++;
		}
	}

	indice_tab = 0;
	double alpha = (float)rand() / ((float)RAND_MAX + 1);
	double cummul = pow(joueur->listeRegle[tab[indice_tab]].priorite, 0.0);

	while(alpha > cummul / s)
	{
		indice_tab ++;
		cummul += pow(joueur->listeRegle[tab[indice_tab]].priorite, cummul);
	}
	return joueur->listeRegle[tab[indice_tab]].action;
}

int choix_de_la_regle2(joueur_t *joueur, int tableau_ressemblance[])
{
	
	int nRegleValide = 0;
	int sommePriorite = 0;
	
	for(int i = 0; i<joueur->nRegle; i++){
	//	printf("%d : %d / %d\n", i, tableau_ressemblance[i], joueur->listeRegle[i].priorite);
		if(tableau_ressemblance[i]==1) {
			nRegleValide++;
			sommePriorite += joueur->listeRegle[i].priorite;
		}
	}
	//printf("somme priorite %d, nRegleValide %d\n",sommePriorite, nRegleValide);
	int listeRegleValide[sommePriorite];
	int k = 0;
	for(int i = 0; i<joueur->nRegle; i++){
		if(tableau_ressemblance[i]==1) {
			for(int j = 0; j<joueur->listeRegle[i].priorite; j++) 
			{
				listeRegleValide[k+j] = i;
			}
			k += joueur->listeRegle[i].priorite;
		}
	}
	int listeRegleChoisie = rand()%sommePriorite;
	int regleChoisie = listeRegleValide[listeRegleChoisie];
	int action = joueur->listeRegle[regleChoisie].action;
	//printf("regleChoisie parmi prio %d | regle choisie : %d | action %d\n\n",listeRegleChoisie, regleChoisie, action);
	return action;
	
	/*
	double s = 0;
	int *tab = (int*) malloc(sizeof(int)*joueur->nRegle);
	int indice_tab = 0;
	for (int i = 0; i < joueur->nRegle; i++)
	{
	printf("(%d) %d | %d\n",i, tableau_ressemblance[i], joueur->listeRegle[i].priorite);
		if(tableau_ressemblance[i] == 1)
		{
			s += pow(joueur->listeRegle[i].priorite, s);
			tab[indice_tab] = i;
			indice_tab ++;
		}
	}

	
	double alpha = (double)rand() / ((double)RAND_MAX + 1);
	double somme = 0;
	for (int i = 0; i < indice_tab ; i++)
		somme += pow(joueur->listeRegle[tab[i]].priorite, s);
	indice_tab = 0;
	double cummul = pow(joueur->listeRegle[tab[indice_tab]].priorite, s);

	while(alpha > cummul / somme)
	{
		indice_tab ++;
		cummul += pow(joueur->listeRegle[tab[indice_tab]].priorite, s);
	}
	
	printf("regle choisie %d\n\n",indice_tab);
	return joueur->listeRegle[tab[indice_tab]].action;
	*/
}

int choix_de_la_regle3(joueur_t *joueur, int tableau_ressemblance[])
{
	double s = 0;
	int tab[joueur->nRegle];
	int indice_tab = 0;

	for (int i = 0; i < joueur->nRegle; i++)
	{
		if(tableau_ressemblance[i] == 1)
		{
			s += pow(joueur->listeRegle[i].priorite, s);
			tab[indice_tab] = i;
			indice_tab ++;
		}
	}

	
	double alpha = (double)rand() / ((double)RAND_MAX + 1);
	double somme = 0;
	for (int i = 0; i < indice_tab ; i++)
		somme += pow(joueur->listeRegle[tab[i]].priorite, s);
	indice_tab = 0;
	double cummul = pow(joueur->listeRegle[tab[indice_tab]].priorite, s) / somme;

	while(alpha > cummul / somme)
	{
		indice_tab ++;
		cummul += pow(joueur->listeRegle[tab[indice_tab]].priorite, s) / somme;
	}
	return joueur->listeRegle[tab[indice_tab]].action;
}


int estSurMonstre(joueur_t *joueur, carte_t *carte, int direction_de_deplacement){
	int i = 0, j = 0;
	if (direction_de_deplacement==1) i = -1;
	else if (direction_de_deplacement==2) j = 1;
	else if (direction_de_deplacement==3) i = 1;
	else if (direction_de_deplacement==4) j = -1;
	return (carte->carte[joueur->y + j][joueur->x+i]==3);
}

int estSurSortie(joueur_t *joueur, sortie_t *sortie){
	return ((joueur->x == sortie->x) && (joueur->y == sortie->y));
}

Bool new_place(joueur_t *joueur, carte_t *map, int direction_de_deplacement)
{
	int i = 0, j = 0;
	if (direction_de_deplacement == 1)
	{
		i = -1;
	}
	else if (direction_de_deplacement == 2)
	{
		j = 1;
	}
	else if (direction_de_deplacement == 3)
	{
		i = 1;
	}
	else if (direction_de_deplacement == 4)
	{
		j = -1;
	}
	else
	{
		i = 0;
		j = 0;
	}
	if (map->carte[joueur->y + j][joueur->x + i] == 1) return true;
	
	return false;
}

void modif_position_joueur(joueur_t *joueur, int direction_de_deplacement)
{
	int x = 0, y = 0;
	if (direction_de_deplacement == 1)
		x = -1;
	else if (direction_de_deplacement == 2)
		y = 1;
	else if (direction_de_deplacement == 3)
		x = 1;
	else if (direction_de_deplacement == 4)
		y = -1;
	joueur->x =joueur->x + x;
	joueur->y = joueur->y + y;
}


void afficher_liste(int liste[], int taille)
{
	for (int i = 0; i < taille; i++)
	{
		printf("%d\t",liste[i]);
	}
	printf("\n");
}

int deplacement_heros_jusque_sortie(joueur_t *joueur, carte_t *map, int nombre_perception, sortie_t *sortie, int *compteur)
{
	while ( ((joueur->x != sortie->x) && (joueur->y != sortie->y)) && (joueur->etat ==0) )
	{
		printf("1\n");
		deplacement_un_pas_heros(joueur, map, nombre_perception);
		printf("2\n");
		compteur += 1;
		printf("3\n");
	}
	if (joueur->etat ==0)
		return 1;
	return 0;
}


void FileFromRegle(char * fileName, joueur_t * joueur, int nombre_perception)
{
	FILE *file;

	file = fopen(fileName, "w");

	if (file == NULL) // on verifie que l'ouverture du fichier s'est bien passé                                                       
    {
        printf("Erreur lors de l'ouverture du fichier !\n");
        //return 0;
    }
	
	fprintf(file,"%d", joueur->nRegle);
	fprintf(file, "\n");
	//printf("nb regles : %d\n", joueur->nRegle);
	int num_regle = 0;

	do
	{
		for (int i = 0; i < nombre_perception; i++)
		{
			fprintf(file, "%d ",joueur->listeRegle[num_regle].perception[i]);
			//printf(" %d\n", joueur->listeRegle[num_regle].perception[i]);
		}
		//printf("\n");
		fprintf(file, "%d ", joueur->listeRegle[num_regle].action);
		fprintf(file, "%d ", joueur->listeRegle[num_regle].priorite);
		fprintf(file, "\n");
		//sprintf("regle num : %d, action : %d\n",num_regle, joueur->nRegle);
		num_regle +=1;

	}while(num_regle < joueur->nRegle);
	

	fclose(file);
}

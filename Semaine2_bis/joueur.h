#ifndef __JOUEUR_H__
#define __JOUEUR_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "carte.h"
//#include "simulation.h"
//#include "monstre.h"
//#include "apprentissage_regle.h"
#include "pluscourtchemin.h"

typedef struct regle{
	int *perception;		//Vecteur perception de l'environnement du joueur
	int action;
	int priorite;
}regle_t;

typedef struct joueur{
	int x,y;				//position du joueur (n° ligne / n° colonne)
	int nRegle;
	regle_t *listeRegle;
	int etat; //1 si en vie, 0 sinon
} joueur_t;

typedef struct sortie{
	int x,y;
} sortie_t;

typedef enum
{
	true,
	false
}Bool;


joueur_t* initJoueur1(int nombre_perception);
joueur_t* initJoueur2(int nombre_perception);
void deplacement_un_pas_heros(joueur_t *joueur, carte_t *map, int nombre_perception);
void detection_perception(joueur_t *joueur, carte_t *map, regle_t *alentour_joueur);//ok
void comparaison_avec_regles(joueur_t *joueur, regle_t *alentour_joueur, int nombre_perception, int tableau_ressemblance[]);
void triInsertion(int tableau_valeur[], int tableau_num_regle[], int taille);//ok
int choix_de_la_regle(joueur_t *joueur, int tableau_ressemblance[]);//ok
int choix_de_la_regle2(joueur_t *joueur, int tableau_ressemblance[]);
int choix_de_la_regle3(joueur_t *joueur, int tableau_ressemblance[]);
Bool new_place(joueur_t *joueur, carte_t *map, int direction_de_deplacement);//ok
void modif_position_joueur(joueur_t *joueur, int direction_de_deplacement);//ok
void afficher_liste(int liste[], int taille); //ok
int estSurMonstre(joueur_t *joueur, carte_t *carte, int direction_de_deplacement);
int estSurSortie(joueur_t *joueur, sortie_t *sortie);

//retourne 1 si le heros arrive à la sortie, 0 sinon
int deplacement_heros_jusque_sortie(joueur_t *joueur, carte_t *map, int nombre_perception, sortie_t *sortie, int *compteur);
void FileFromRegle(char * fileName, joueur_t * joueur, int nombre_perception);
#endif

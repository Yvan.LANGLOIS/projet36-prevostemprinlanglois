//#include "carte.h"
#include "joueur.h"
#include "graphisme.h"
#include "monstre.h"
#include "simulation.h"
#include "apprentissage_regle.h"

int main()
{
    srand(time(NULL));
    carte_t *map = creationCarte(15,15, 0.1);

    //generation de la simulation
    simulation_t *simul = initSimulationFromCarte(100, map, 12, 3);
    int somme = 0;
    for (int i = 0; i < 100; i++)
        somme += execution(simul);
    printf("score 1 : %d\n", somme);
    // printf("nb1 : %d\n",simul->joueur->listeRegle[2].priorite);
    // simulation_t *simul_2 = simul;
    // simul_2->joueur->listeRegle[2].priorite = 2;
    // printf("nb2 : %d\n",simul_2->joueur->listeRegle[2].priorite);
    amelioration_regles(10,12,1);

}
    /*
    //amelioration_regles(100, 12, 3);
    int taille = 15*14;
    int liste[taille];
    int i,j;

    randomizer(taille,liste);

    for (i=0;i<taille;i++)
    {
        printf("%d ", liste[i]);
    }
    printf("\n\n");

    int taille_paquets = 4;
    int nb_de_paquets = taille/taille_paquets +1;
    int ** paquets = fait_des_groupe(liste,taille_paquets,taille);
    printf("nnb_de_paquets : %d\n",nb_de_paquets);


    for(i=0;i<nb_de_paquets;i++)
    {
        for(j=0;j<taille_paquets;j++)
        {
            printf("%d ",paquets[i][j]);
        }
        printf("\n");
    }
}*/
/*
    //génération de la carte
    srand(time(NULL));
    carte_t map;
    map.width = 25;
    map.height = 25;
    genere_carte(&map);
    mur_carte(&map, 0.05);
    afficher_Matrice(&map);
    
    //placement des monstres
    int nb_de_monstre = 10;
    monstre_cell_t * ListeDeMonstres = Rajoute_des_monstres2(map, nb_de_monstre);
    Init_Monstre_carte(&map, ListeDeMonstres);
    afficher_Matrice(&map);

    //placement de la sortie et du joueur
    joueur_t joueur;
    sortie_t sortie;

    Init_Joueur_Sortie2(map,joueur,sortie);
    afficher_Matrice(&map);
    return 1;
}
*/
/*
void launch()
{
    srand(time(NULL));
    carte_t map;
    map.width = 10;
    map.height = 10;
    genere_carte(&map);
    mur_carte(&map, 0.05);
    afficher_Matrice(&map);

    joueur_t *joueur = initJoueur2(12);
    joueur->x = 5;
    joueur->y = 5;
    joueur->etat = 0;

    sortie_t sortie;
    sortie.x = 6;
    sortie.y = 6;

    int compteur = 0;
    int res = -1;

    res = deplacement_heros_jusque_sortie(joueur, &map, 12, &sortie, &compteur);
    printf("res : %d, cpt : %d\n", res, compteur);


    
    printf("\n --Test du joueur et sortie-- \n");

    joueur_t J;
    position_aleatoire_dans_matrice(&map, &(J.x), &(J.y));

    sortie_t S;
    position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));
    while(loin_de_joueur(J,S.x,S.y)==0)
    {
        position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));
        printf("ok");
    }
    printf("\n");

    
    //Verification du trajet :
    getLiaisonFromCarte(&map);
    int code_e = Verif_trajet(J,S,map);
    while (code_e == 1)
    {
        // nouvelle positions:
        position_aleatoire_dans_matrice(&map, &(J.x), &(J.y));
        position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));
        while(loin_de_joueur(J,S.x,S.y)==0)
        {
            position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));
            printf("ok\n");
        }

        //Verification du trajet :
        getLiaisonFromCarte(&map);
        code_e = Verif_trajet(J,S,map);
        printf("ok %d\n", code_e);
    }
    
    //getLiaisonFromCarte(&map);

    //afficher_Matrice(&map);

    //afficher_Liaison(&map);

    printf("%d\n",Verif_trajet(J,S,map));

    printf("\n --Test des monstres-- \n");

    //placement des monstres

    int nb_de_monstre = 10;

    monstre_cell_t * ListeDeMonstres = Rajoute_des_monstres(map, nb_de_monstre, J, S);
    
    Update_carte(&map, ListeDeMonstres, J);

    //Verification du trajet :
    getLiaisonFromCarte(&map);
    code_e = Verif_trajet(J,S,map);
    printf("ok %d\n", code_e);

    while(code_e == 1)
    {
        int nb_de_monstre = 10;

        monstre_cell_t * ListeDeMonstres = Rajoute_des_monstres(map, nb_de_monstre, J, S);
        
        Update_carte(&map, ListeDeMonstres, J);

        getLiaisonFromCarte(&map);
        code_e = Verif_trajet(J,S,map);
        printf("ok %d\n", code_e);
        
    }
    Init_pos_mat_JetS(J,S,&map); //ordre important
    afficher_Matrice(&map);
    */
    

/*
    joueur_t joueur;
    joueur.x = 10;
    joueur.y = 10;
    joueur.nRegle = 4;
    joueur.listeRegle = (int *)malloc(4 * sizeof(regle_t));
    joueur.listeRegle[0].perception = (int *)malloc(12 * sizeof(int));
    joueur.listeRegle[0].perception = {0,0,0,0,0,0,0,0,0,0,0,0};
    joueur.listeRegle[0].action = 0;
    joueur.listeRegle[0].priorite = 0;  */

    //int direction = int choix_de_la_regle(joueur_t *joueur, int tableau_num_regle[], int nb_prio);
  
  /*
    joueur_t *joueur = initJoueur2(12);
    joueur->x = 10;
    joueur->y = 10;

    regle_t *alentour_joueur = malloc(12*sizeof(int));

    detection_perception(joueur, &map, alentour_joueur);

    int tableau_ressemblance[12];

    for (int i = 0; i < 12; i++)
        printf("%d\t", alentour_joueur->perception[i]);
    printf("\n");
    comparaison_avec_regles(joueur, alentour_joueur, 12, tableau_ressemblance);
    afficher_liste(tableau_ressemblance, 12);
    */
//}

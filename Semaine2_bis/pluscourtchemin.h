#ifndef __PLUSCOURTCHEMIN_H__
#define __PLUSCOURTCHEMIN_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>
#include <stdlib.h>

void init_list_voisins(int * list, int taille);

void liste_voisin_explicite(int A, int N_sommet, int ** MATRICE, int * vois_explicite, int * nb_vois);

int Recherche_sommet(int A, int * list, int taille);

typedef struct chemin {
    int nb_sommet;
    int distance;
    int * suite;
} chemin_t;

chemin_t initialisation_chemin(int taille);

void copier_chemin(chemin_t* source, chemin_t* dest);

void Ajouter_sommet(int A, int num_chemin, chemin_t * ch, int ** MATRICE);

void creer_chemin_par_voisin(int ** MATRICE, chemin_t * liste_chemin, chemin_t * chemin_suivant, int num_chemin, int taille, int * nb_chemin);

void liberer_chemin(chemin_t * ch, int taille);

void Plus_court_chemin(int ** MATRICE, int N_sommet, int A, int B, chemin_t * chemin_le_plus_cours, int * code_err);

void afficher_chemin(chemin_t PCC);

void LibererMatrice (int ** MATRICE, int taille);

int Prochaine_etape(int ** MATRICE, int taille, int pos_ar, int pos_pap);

#endif
#include "simulation.h"


void iter(simulation_t *simulation){
		
	//printf("iteration\n");	
	if(estSurMonstre(simulation->joueur, simulation->carte, 0)) simulation->fini = 1; 
	simulation->carte->carte[simulation->joueur->x][simulation->joueur->y] = 0;
	deplacement_un_pas_heros(simulation->joueur, simulation->carte, 12);
	if(estSurMonstre(simulation->joueur, simulation->carte, 0)) simulation->fini = 1; 
	if(estSurSortie(simulation->joueur, simulation->sortie)) simulation->fini = 2; 
	simulation->carte->carte[simulation->joueur->x][simulation->joueur->y] = 7;
	//void Update_carte2(carte_t * carte, monstre_cell_t * headp, joueur_t joueur, sortie_t sortie)
	Update_carte2(simulation->carte, simulation->monstreListe, *(simulation->joueur), *(simulation->sortie));
	simulation->iteration+=1;

}


simulation_t* initSimulationFromCarte(int iterationMax, carte_t *carte, int nPerception, int nMonstre){
	simulation_t *simu = (simulation_t*) malloc(sizeof(simulation_t));
	
	simu->fini = 0;
	simu->iterationMax = iterationMax;
	simu->iteration = 0;
	
	simu->carte = carte;
	
	//simu->monstreListe = Rajoute_des_monstres(*(simu->carte), nMonstre, *(simu->joueur), *(simu->sortie));
	simu->monstreListe = Rajoute_des_monstres2(*(simu->carte), nMonstre);
	Init_Monstre_carte(simu->carte, simu->monstreListe);
    
    
	simu->joueur = initJoueur2(nPerception);
	simu->sortie = (sortie_t*) malloc(sizeof(sortie_t));
	
	/*
	simu->joueur->x = 3;
	simu->joueur->y = 4;
	simu->sortie->x = 11;
	simu->sortie->y = 7;
	*/
	
	
	Init_Joueur_Sortie2(simu->carte,simu->joueur,simu->sortie);
	//printf("pos joueur %d | %d %d\n",simu->carte->carte[simu->joueur->x][simu->joueur->y], simu->joueur->x, simu->joueur->y);
	//printf("pos sortie %d | %d %d\n",simu->carte->carte[simu->sortie->x][simu->sortie->y], simu->sortie->x, simu->sortie->y);
    	
	return simu;
}

carte_t* creationCarte(int carteW, int carteH, float probaMur){
	carte_t *carte = (carte_t*) malloc(sizeof(carte_t));
	carte->width = carteW;
	carte->height = carteH;
	genere_carte(carte);
	mur_carte(carte, probaMur);
	getLiaisonFromCarte(carte);
	
	return carte;
}

simulation_t* initSimu(int iterationMax, int carteW, int carteH, float probaMur, int nPerception, int nMonstre){

	carte_t *carte = creationCarte(carteW, carteH, probaMur);
	simulation_t *simu = initSimulationFromCarte(iterationMax, carte, nPerception, nMonstre);
	
	return simu;
}
/*
simulation_t* copieSimu(simulation_t *oSimu){
	simulation_t *simu = (simulation_t*) malloc(sizeof(simulation_t));
	simu->fini = 0;
	simu->iterationMax = oSimu->iterationMax;
	simu->iteration = 0;
	
	simu->carte = oSimu->carte;
	
    	simu->joueur = initJoueur2(nPerception);
//	simu->sortie = oSimu;
	
	//printf("pos joueur %d | %d %d\n",simu->carte->carte[simu->joueur->x][simu->joueur->y], simu->joueur->x, simu->joueur->y);
	//printf("pos sortie %d | %d %d\n",simu->carte->carte[simu->sortie->x][simu->sortie->y], simu->sortie->x, simu->sortie->y);
    	
	return simu;
}*/

int calcScore(simulation_t *simu){
	//int score = simu->iteration;
	/*
	printf("iteration : %d / fin %d\n",simu->iteration, simu->fini);
	int i, j;
	for(i = 0; i<simu->carte->width; i++){
		for(j = 0; j<simu->carte->height; j++){
			printf("%d ",simu->carte->carte[i][j]);
		}
		printf("\n");
	}
	*/
	if(simu->fini == 2) return 0;			// Le joueur a trouvé l'arrivé
	
	//sortie_t *sortie = simu->sortie;
	//joueur_t *joueur = simu->joueur;
	//int sX = sortie->x, sY = sortie->y;
	//int jX = joueur->y, jY = joueur->x;
	
	//printf("SX %d SY %d | JX %d JY %d\n", sX, sY, jX, jY);
	
	if(simu->fini == 0) {					// Le joueur a fini en vie, mais n'a pas atteint l'arrivée
		
		
		//score=(int) ((sX-jX)*(sX-jX)+(sY-jY)*(sY-jY));
		return 5;
	}
	//score = simu->iterationMax - simu->iteration;
	//score +=(int) ((sX-jX)*(sX-jX)+(sY-jY)*(sY-jY));
	return 10;
}

int execution(simulation_t *simu){
   	while(simu->iteration<simu->iterationMax && simu->fini==0){
	   	iter(simu);
   	}

   	return calcScore(simu);

}


int Verif_trajet(joueur_t joueur, sortie_t sortie, carte_t map)  // renvoie 1 si le trajet n'est pas faisable
{
    //getLiaisonFromCarte(&map);
    int som_J = num_som_from_pos(map, joueur.x, joueur.y);
    int som_S = num_som_from_pos(map, sortie.x, sortie.y);
    int code = 0;

    chemin_t chemin_a_suivre;

	chemin_a_suivre = initialisation_chemin(map.width*map.height);


    Plus_court_chemin(map.liaison, map.width*map.height, som_J, som_S, &chemin_a_suivre, &code);
    //liberer_chemin(&chemin_a_suivre, map.width+map.height);
    //printf("jusque là tout va bien\n");

    return code;
}

void Init_pos_mat_JetS(joueur_t *joueur, sortie_t *sortie, carte_t * carte)
{
    carte->carte[joueur->x][joueur->y] = 7; //provisoire (pour voir ou est le joueur)
    carte->carte[sortie->x][sortie->y] = 2;
}

void Init_Joueur_Sortie(carte_t map, int nb_monstre, joueur_t J, sortie_t S)
{
	position_aleatoire_dans_matrice(&map, &(J.x), &(J.y));
	position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));
    while(loin_de_joueur(J,S.x,S.y)==0)
    {
        position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));
    }

	//Verification du trajet :
    getLiaisonFromCarte(&map);
    int code_e = Verif_trajet(J,S,map);

    while (code_e == 1)
    {
        // nouvelle positions:
        position_aleatoire_dans_matrice(&map, &(J.x), &(J.y));
        position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));

        while(loin_de_joueur(J,S.x,S.y)==0)
        {
            position_aleatoire_dans_matrice(&map, &(S.x), &(S.y));
        }

        //Verification du trajet :
        getLiaisonFromCarte(&map);
        code_e = Verif_trajet(J,S,map);
        //printf("ok %d\n", code_e);
    }

	//placement des monstres

	monstre_cell_t * ListeDeMonstres = Rajoute_des_monstres(map, nb_monstre, J, S);
	
	Update_carte(&map, ListeDeMonstres, J);

	//Verification du trajet :
    getLiaisonFromCarte(&map);
    code_e = Verif_trajet(J,S,map);
    printf("ok %d\n", code_e);

    while(code_e == 1)
    {

        monstre_cell_t * ListeDeMonstres = Rajoute_des_monstres(map, nb_monstre, J, S);
        
        Update_carte(&map, ListeDeMonstres, J);

        getLiaisonFromCarte(&map);
        code_e = Verif_trajet(J,S,map);
        printf("ok %d\n", code_e);
        
    }
    Init_pos_mat_JetS(&J,&S,&map); //ordre important
}

void Init_Joueur_Sortie2(carte_t *map,joueur_t *J, sortie_t *S) //Il faut rajouter les monstres avant cette fonction
{
	position_aleatoire(map, &J->x, &J->y);
	position_aleatoire(map, &S->x, &S->y);
    while(loin_de_joueur(*J,S->x,S->y)==0)
    {
        position_aleatoire_dans_matrice(map, &S->x, &S->y);
    }

	//Verification du trajet :
    getLiaisonFromCarte(map);
    int code_e = Verif_trajet(*J,*S,*map);

    while (code_e == 1)
    {
        // nouvelle positions:
	position_aleatoire(map, &J->x, &J->y);
	position_aleatoire(map, &S->x, &S->y);

        while(loin_de_joueur(*J,S->x,S->y)==0)
        {
	position_aleatoire(map, &S->x, &S->y);
        }

        //Verification du trajet :
        getLiaisonFromCarte(map);
        code_e = Verif_trajet(*J,*S,*map);
        //printf("ok %d\n", code_e);
    }

    Init_pos_mat_JetS(J,S,map); //ordre important
}

void position_aleatoire(carte_t *map, int *x, int *y)
{
    *x = 2 + rand()%(map->width-2);
    //printf("x = %d\t", *x);
    *y = 2 + rand()%(map->height-2);
    //printf("y = %d\n", *y);

    while (map->carte[*x][*y] == 1)
    {
        //printf("1\n");
        *x = 2 + rand()%(map->width-2);
        //printf("x = %d\t", *x);
        *y = 2 + rand()%(map->height-2);
        //printf("y = %d\n", *y);
    }
}

void Init_Monstre_carte (carte_t * carte, monstre_cell_t * headp)
{

    monstre_cell_t * cour = headp;
    int i = 0;
    while(cour)
    {
    	i++;
        monstre_t *mons = cour->monstre;

        carte->carte[mons->Mx][mons->My] = 3;
    
        cour = cour->suiv;
    }
}

//modifie Rajoute des monstre pour qu'on puisse les placer avant joueur et sortie
//modifie Rajoute des monstre pour qu'on puisse les placer avant joueur et sortie







void launch(){

   	srand(time(NULL));
	
	int iterationMax = 10;
	
	int carteW = 15, carteH = 15;
	float probaMur = 0.01;
	
	int nPerception = 12;		// Eviter de le changer
	
	int nMonstre = 2;		// Il y en aura nMonstre+1

   	//simulation_t *simu = initSimu(iterationMax, carteW, carteH, probaMur, nPerception, nMonstre);
   	int nSimu = 1;
   	
   	carte_t *carte = creationCarte(carteW, carteH, probaMur);
   	simulation_t **simulationListe = (simulation_t**) malloc(nSimu*sizeof(simulation_t*));
   	int i;
   	for(i = 0; i<nSimu; i++){
   		//simulation_t* initSimulationFromCarte(int iterationMax, carte_t *carte, int nPerception, int nMonstre)
   		simulationListe[i] = initSimulationFromCarte(iterationMax, carte, nPerception, nMonstre);
   	}
	
   	// Mettre des threads
   	for(i = 0; i<nSimu; i++){
   		printf("resultat%d : %d\n",i, execution(simulationListe[i]));
   	}   	
   	
   	//return 0;
	
}






#ifndef __SIMULATION_H__
#define __SIMULATION_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>

#include "carte.h"
#include "joueur.h"
#include "monstre.h"

typedef struct simulation{
	joueur_t *joueur;
	carte_t *carte;
	sortie_t *sortie;
	monstre_cell_t *monstreListe;
	
	int iterationMax;
	int iteration;
	int fini;			// 0 => pas mort mais sortie non trouvée  |  1 => mort de monstre  |  2 => sortie trouvée
} simulation_t;

simulation_t* initSimulationFromCarte(int iterationMax, carte_t *carte, int nPerception, int nMonstre);
simulation_t* initSimu(int iterationMax, int carteW, int carteH, float probaMur, int nPerception, int nMonstre);

void iter(simulation_t *simulation);

int calcScore(simulation_t *simu);
int execution(simulation_t *simu);
int Verif_trajet(joueur_t joueur, sortie_t sortie, carte_t map);
  // renvoie 1 si le trajet n'est pas faisable

void Init_pos_mat_JetS(joueur_t *joueur, sortie_t *sortie, carte_t *carte);

void Init_Joueur_Sortie(carte_t map, int nb_monstre, joueur_t J, sortie_t S);

void position_aleatoire(carte_t *map, int *x, int *y);

void Init_Joueur_Sortie2(carte_t *map, joueur_t *J, sortie_t *S);

void Init_Monstre_carte (carte_t * carte, monstre_cell_t * headp);

int calcScore(simulation_t *simu);
carte_t* creationCarte(int carteW, int carteH, float probaMur);

#endif
